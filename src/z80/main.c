/*-------------------------------------------------------------------------
  main.c - Z80 specific definitions.

  Michael Hope <michaelh@juju.net.nz> 2001

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 2, or (at your option) any
   later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   In other words, you are welcome to use, share and improve this program.
   You are forbidden to forbid anyone else to use, share and improve
   what you give them.   Help stamp out software-hoarding!
-------------------------------------------------------------------------*/

#include <sys/stat.h>
#include "z80.h"
#include "SDCCsystem.h"
#include "SDCCutil.h"
#include "SDCCargs.h"
#include "dbuf_string.h"

#define OPTION_BO              "-bo"
#define OPTION_BA              "-ba"
#define OPTION_CODE_SEG        "--codeseg"
#define OPTION_CONST_SEG       "--constseg"
#define OPTION_CALLEE_SAVES_BC "--callee-saves-bc"
#define OPTION_PORTMODE        "--portmode="
#define OPTION_ASM             "--asm="
#define OPTION_NO_STD_CRT0     "--no-std-crt0"
#define OPTION_RESERVE_IY      "--reserve-regs-iy"
#define OPTION_OLDRALLOC       "--oldralloc"
#define OPTION_FRAMEPOINTER    "--fno-omit-frame-pointer"

static char _z80_defaultRules[] = {
/* ROSE MODIFICATION (DQ (1/26/2014): Including header for initialization.
   include "peeph.rul"
   include "peeph-z80.rul"
*/
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"\n"
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"replace restart {\n"
"	ld	%1, %1\n"
"} by {\n"
"	; peephole 0 removed redundant load from %1 into %1.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"} by {\n"
"	; peephole 1 removed dead load from %2 into %1.\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	add	ix,sp\n"
"} by {\n"
"	; peephole 1a removed dead frame pointer setup.\n"
"} if notUsed('ix')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 + %3\n"
"} by {\n"
"	; peephole 2 removed dead load from %2 + %3 into %1.\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, (iy)\n"
"} by {\n"
"	ld	%1, 0 (iy)\n"
"	; peephole 3 made 0 offset explicit.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	(iy), %1\n"
"} by {\n"
"	ld	0 (iy), %1\n"
"	; peephole 4 made 0 offset explicit.\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"} by {\n"
"	; peephole 5 removed dead increment of hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	dec	hl\n"
"} by {\n"
"	; peephole 6 removed dead decrement of hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (iy)\n"
"} by {\n"
"	; peephole 7 removed dead load from %2 (iy) into %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (ix)\n"
"} by {\n"
"	; peephole 8 removed dead load from %2 (ix) into %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3, %1\n"
"} by {\n"
"	; peephole 9 loaded %3 from %2 directly instead of going through %1.\n"
"	ld	%3, %2\n"
"} if canAssign(%3 %2), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3, %4\n"
"	ld	%5, %1\n"
"} by {\n"
"	ld	%5, %2\n"
"	; peephole 10 loaded %5 from %2 directly instead of going through %1.\n"
"	ld	%3, %4\n"
"} if canAssign(%5 %2), notVolatile(%1), operandsNotRelated(%1 %4), operandsNotRelated(%1 %3), operandsNotRelated(%4 %5), notUsed(%1), notSame(%3 %4 '(hl)' '(de)' '(bc)'), notVolatile(%5)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %1\n"
"} by {\n"
"	; peephole 11 loaded %2 (%3) into %4 directly instead of going through %1.\n"
"	ld	%4, %2 (%3)\n"
"} if canAssign(%4 %2 %3), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3 (%4), %1\n"
"} by {\n"
"	; peephole 12 loaded %2 into %3 (%4) directly instead of going through %1.\n"
"	ld	%3 (%4), %2\n"
"} if canAssign(%3 %4 %2), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %5 (%6)\n"
"	ld	%7, %1\n"
"} by {\n"
"	ld	%7, %2 (%3)\n"
"	; peephole 13 loaded %2 (%3) into %7 directly instead of going through %1.\n"
"	ld	%4, %5 (%6)\n"
"} if canAssign(%7 %2 %3), notVolatile(%1), notUsed(%1), notSame(%1 %4), notSame(%7 %4)\n"
"\n"
"replace restart {\n"
"	ld	%1, %7\n"
"	ld	%5 (%6), %4\n"
"	ld	%2 (%3), %1\n"
"} by {\n"
"	ld	%5 (%6), %4\n"
"	; peephole 14 loaded %7 into %2 (%3) directly instead of going through %1.\n"
"	ld	%2 (%3), %7\n"
"} if canAssign(%2 %3 %7), notVolatile(%1), notUsed(%1), notSame(%1 %4)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %5\n"
"	ld	%7, %1\n"
"} by {\n"
"	ld	%7, %2 (%3)\n"
"	; peephole 15 loaded %2 (%3) into %7 directly instead of going through %1.\n"
"	ld	%4, %5\n"
"} if canAssign(%7 %2 %3), notVolatile(%1), notUsed(%1), notSame(%1 %5), notSame(%7 %4), notSame(%7 %5), notSame(%4 '(hl)' '(de)' '(bc)'), notSame(%5 '(hl)' '(de)' '(bc)' '(iy)')\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	a,%3 (%1)\n"
"} by {\n"
"	; peephole 16 loaded %2 into a directly instead of going through %1.\n"
"	ld	a,(#%2 + %3)\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	a,(hl)\n"
"} by {\n"
"	ld	a,(#%1)\n"
"	; peephole 17 loaded a from (#%1) directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1 + %2\n"
"	ld	a,(hl)\n"
"} by {\n"
"	; peephole 18 loaded %2 into a directly instead of using hl.\n"
"	ld	a,(#%1 + %2)\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	(#%1),a\n"
"	; peephole 19 loaded (#%1) from a directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	(#%1 + %2),a\n"
"	; peephole 20 loaded (#%1) from a directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	srl	%1\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 21 shifted in a instead of %1.\n"
"	srl	a\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	a, (de)\n"
"	srl	a\n"
"	ld	(de), a\n"
"} by {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	srl	(hl)\n"
"	ld	a, (hl)\n"
"	; peephole 21a shifted in (hl) instead of a.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, %1 (%2)\n"
"	srl	a\n"
"	ld	%1 (%2), a\n"
"} by {\n"
"	srl	%1 (%2)\n"
"	ld	a, %1 (%2)\n"
"	; peephole 21b shifted in (%2) instead of a.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	ld	%2, %3 (iy)\n"
"	srl	%2\n"
"	bit %4, %3 (iy)\n"
"} by {\n"
"	ld	hl, #%1 + %3\n"
"	; peephole 21c used hl instead of iy.\n"
"	ld	%2, (hl)\n"
"	srl %2\n"
"	bit %4, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,(hl)\n"
"	ld	a,%2 (%3)\n"
"	sub	a,%1\n"
"} by {\n"
"	ld	a,%2 (%3)\n"
"	; peephole 22 used (hl) in sub directly instead of going through %1.\n"
"	sub	a,(hl)\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	inc	bc\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	ld	l,c\n"
"	ld	h,b\n"
"	; peephole 23 incremented in hl instead of bc.\n"
"	inc	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	inc	de\n"
"	ld	l,e\n"
"	ld	h,d\n"
"} by {\n"
"	ld	l,e\n"
"	ld	h,d\n"
"	; peephole 24 incremented in hl instead of de.\n"
"	inc	hl\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	a,#%1\n"
"	ld	(bc),a\n"
"} by {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	(hl),#%1\n"
"	; peephole 25 loaded #%1 into (hl) instead of (bc).\n"
"}\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	de\n"
"} by {\n"
"	; peephole 26 pushed hl directly instead of going through de.\n"
"	push	hl\n"
"} if notUsed('de'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,%1\n"
"	ld	h,d\n"
"	push	hl\n"
"} by {\n"
"	; peephole 27 pushed de instead of hl removing a load.\n"
"	ld	e,%1\n"
"	push	de\n"
"} if notUsed('hl'), notUsed('e')\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	bc\n"
"	push	de\n"
"} by {\n"
"	; peephole 28 pushed hl directly instead of going through de.\n"
"	push	bc\n"
"	push	hl\n"
"} if notUsed('de'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,c\n"
"	ld	h,b\n"
"	push	hl\n"
"} by {\n"
"	; peephole 29 pushed bc directly instead of going through hl.\n"
"	push	bc\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,%1\n"
"	ld	h,b\n"
"	push	hl\n"
"} by {\n"
"	; peephole 30 pushed bc instead of hl removing a load.\n"
"	ld	c,%1\n"
"	push	bc\n"
"} if notUsed('hl'), notUsed('c')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	push	%1\n"
"	push	bc\n"
"} by {\n"
"	; peephole 31 pushed hl directly instead of going through bc.\n"
"	push	%1\n"
"	push	hl\n"
"} if notUsed('bc'), notSame(%1 'bc')\n"
"\n"
"replace restart {\n"
"	pop	de\n"
"	ld	l, e\n"
"	ld	h, d\n"
"} by {\n"
"	; peephole 32 popped hl directly instead of going through de.\n"
"	pop	hl\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	pop	bc\n"
"	ld	l, c\n"
"	ld	h, b\n"
"} by {\n"
"	; peephole 33 popped hl directly instead of going through bc.\n"
"	pop	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix), %2\n"
"	ld	%3, %1 (ix)\n"
"} by {\n"
"	; peephole 34 loaded %3 from %2 instead of going through %1 (ix).\n"
"	ld	%1 (ix), %2\n"
"	ld	%3, %2\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix), a\n"
"	push	de\n"
"	ld	%2, %1 (ix)\n"
"} by {\n"
"	ld	%1 (ix), a\n"
"	push	de\n"
"	; peephole 34a loaded %2 from a instead of %1 (ix)\n"
"	ld	%2, a\n"
"}	\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,e\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 35 pushed de instead of pushing a twice.\n"
"	ld	d,a\n"
"	push	de\n"
"} if notUsed('d'), notUsed('a')\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 36 pushed de instead of pushing a twice.\n"
"	ld	d,a\n"
"	ld	e,#%1\n"
"	push	de\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 37 pushed bc instead of pushing a twice.\n"
"	ld	b,a\n"
"	ld	c,#%1\n"
"	push	bc\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	push	bc\n"
"	inc	sp\n"
"	push	de\n"
"	inc	sp\n"
"} by {\n"
"	ld	c, d\n"
"	; peephole 37a combined pushing of b and d.\n"
"	push	bc\n"
"} if notUsed('c')\n"
"\n"
"replace restart {\n"
"	push	bc\n"
"	inc	sp\n"
"	ld	a, c\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	push	bc\n"
"	ld	a, c\n"
"	; peephole 38 simplified pushing bc.\n"
"}\n"
"\n"
"replace restart {\n"
"	push	de\n"
"	inc	sp\n"
"	ld	a, #%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	ld	e, #%1\n"
"	push	de\n"
"	; peephole 39 simplified pushing de.\n"
"} if notUsed('e')\n"
"\n"
"replace restart {\n"
"	ld	a,#%1\n"
"	ld	d,a\n"
"} by {\n"
"	; peephole 40 loaded #%1 into d directly instead of going through a.\n"
"	ld	d,#%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	%2,%1\n"
"} by {\n"
"	; peephole 41 loaded %2 from a directly instead of going through %1.\n"
"	ld	%2,a\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	a,%1 (%3)\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,%2 (%3)\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 42 pushed %1 (%3), %2(%3) through hl instead of af.\n"
"	ld	h,%1 (%3)\n"
"	ld	l,%2 (%3)\n"
"	push	hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	c, l\n"
"	ld	b, h\n"
"	push	bc\n"
"} by {\n"
"	; peephole 43 pushed hl instead of bc.\n"
"	push	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	a, (hl)\n"
"	inc	hl\n"
"	ld	h, (hl)\n"
"	ld	l, a\n"
"	push	hl\n"
"} by {\n"
"	; peephole 43a pushed bc instead of hl.\n"
"	ld	c, (hl)\n"
"	inc	hl\n"
"	ld	b, (hl)\n"
"	push	bc\n"
"} if notUsed('bc'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	%1\n"
"	push	%1\n"
"} by {\n"
"	; peephole 44 eleminated dead pop/push pair.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	bc\n"
"} by {\n"
"	ld	c, l\n"
"	ld	b, h\n"
"	; peephole 44a replaced push/pop pair by loads.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	de\n"
"} by {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	; peephole 44b replaced push/pop pair by loads.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	or	a,%2 (iy)\n"
"} by {\n"
"	; peephole 45 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	or	a,(hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2,%3 (iy)\n"
"} by {\n"
"	; peephole 46 used hl instead of iy.\n"
"	ld	hl,#%1 + %3\n"
"	ld	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	h,%3 (iy)\n"
"} by {\n"
"	; peephole 46a used hl instead of iy.\n"
"	ld	hl,#%1 + %3\n"
"	ld	h, (hl)\n"
"} if notUsed('iy'), notUsed('l')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (iy), %3\n"
"} by {\n"
"	; peephole 46b used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl), %3\n"
"} if notUsed('iy'), notUsed('hl'), notSame(%3 'h' 'l')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2,0 (iy)\n"
"	ld	%3,1 (iy)\n"
"} by {\n"
"	; peephole 47 used hl instead of iy.\n"
"	ld	hl,#%1\n"
"	ld	%2, (hl)\n"
"	inc	hl\n"
"	ld	%3, (hl)\n"
"} if notUsed('iy'), notUsed('hl'), operandsNotRelated(%2 'hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (iy),%3\n"
"	ld	l,%2 (iy)\n"
"} by {\n"
"	; peephole 48 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl),%3\n"
"	ld	l,(hl)\n"
"} if notUsed('iy'), notUsed('h')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (%3), %4\n"
"} by {\n"
"	; peephole 49 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl), %4\n"
"} if notUsed('iy'), notUsed('hl'), operandsNotRelated(%4 'hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	bit	%2,%3 (iy)\n"
"} by {\n"
"	; peephole 49a used hl instead of iy.\n"
"	ld	hl,#%1+%3\n"
"	bit	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	%2, %3 (iy)\n"
"} by {\n"
"	; peephole 49b used hl instead of iy.\n"
"	ld	hl, #%1+%3\n"
"	add	hl, sp\n"
"	ld	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	%2, 0 (iy)\n"
"	ld	%3, 1 (iy)\n"
"} by {\n"
"	; peephole 49c used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	%2, (hl)\n"
"	inc	hl\n"
"	ld	%3, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	l, 0 (iy)\n"
"	ld	h, 1 (iy)\n"
"} by {\n"
"	; peephole 49d used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	a, (hl)\n"
"	inc	hl\n"
"	ld	h, (hl)\n"
"	ld	l, a\n"
"} if notUsed('iy'), notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	0 (iy), #%2\n"
"	ld	1 (iy), #%3\n"
"} by {\n"
"	; peephole 49e used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	(hl), #%2\n"
"	inc	hl\n"
"	ld	(hl), #%3\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1+1\n"
"	ld	a, 1 (iy)\n"
"	or	a, 0 (iy)\n"
"} by {\n"
"	ld	hl, #%1\n"
"	ld	a, (hl)\n"
"	dec	hl\n"
"	or	a, (hl)\n"
"	; peephole 49f used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	a, 1 (iy)\n"
"	or	a, 0 (iy)\n"
"} by {\n"
"	ld	hl, #%1+1\n"
"	add	hl, sp\n"
"	ld	a, (hl)\n"
"	dec	hl\n"
"	or	a, (hl)\n"
"	; peephole 49f' used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	bit	%2, %3 (iy)\n"
"} by {\n"
"	ld	hl, #%1+%3\n"
"	add	hl, sp\n"
"	bit	%2, (hl)\n"
"	; peephole 49g used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	or	a, %2 (iy)\n"
"} by {\n"
"	ld	hl, #%1+%2\n"
"	add	hl, sp\n"
"	or	a, (hl)\n"
"	; peephole 49h used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	inc	bc\n"
"} by {\n"
"	; peephole 51 incremented in hl instead of bc.\n"
"	inc	hl\n"
"	ld	c,l\n"
"	ld	b,h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	inc	%2 (iy)\n"
"} by {\n"
"	; peephole 51a incremented in (hl) instead of %2 (iy).\n"
"	ld	hl, #%1+%2\n"
"	inc	(hl)\n"
"} if notUsed('hl'), notUsed('iy')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	dec	%2 (iy)\n"
"} by {\n"
"	; peephole 51a' decremented in (hl) instead of %2 (iy).\n"
"	ld	hl, #%1+%2\n"
"	dec	(hl)\n"
"} if notUsed('hl'), notUsed('iy')\n"
"\n"
"replace restart {\n"
"	add	hl, hl\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	inc	de\n"
"	inc	de\n"
"} by {\n"
"	inc	hl\n"
"	; peephole 51b incremented once in hl instead of incrementing in de twice.\n"
"	add	hl, hl\n"
"	ld	e, l\n"
"	ld	d, h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	add	hl, hl\n"
"	inc	hl\n"
"	inc	hl\n"
"} by {\n"
"	inc	hl\n"
"	; peephole 51c incremented once in hl instead of incrementing in hl twice.\n"
"	add	hl, hl\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a, a\n"
"	add	a, a\n"
"	add	a, #0x04\n"
"} by {\n"
"	inc	a\n"
"	; peephole 51d incremented a once instead of adding #0x04 to a.\n"
"	add	a, a\n"
"	add	a, a\n"
"}\n"
"\n"
"replace restart {\n"
"	add	hl, hl\n"
"	pop	de\n"
"	inc	hl\n"
"	inc	hl\n"
"} by {\n"
"	inc	hl\n"
"	; peephole 51e incremented once in hl instead of incrementing in hl twice.\n"
"	add	hl, hl\n"
"	pop	de\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,%1 (%2)\n"
"	bit	%3,a\n"
"} by {\n"
"	; peephole 52 tested bit of %1 (%2) directly instead of going through a.\n"
"	bit	%3,%1 (%2)\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	bit	%2,a\n"
"} by {\n"
"	; peephole 53 tested bit %2 of %1 directly instead of going through a.\n"
"	bit	%2,%1\n"
"} if notUsed('a'), canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1\n"
"	set	%2, a\n"
"	ld	%1, a\n"
"} by {\n"
"	; peephole 54 set bit %2 of %1 directly instead of going through a.\n"
"	set	%2, %1\n"
"	ld	a, %1\n"
"} if canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1 (%2)\n"
"	set	%3, a\n"
"	ld	%1 (%2), a\n"
"} by {\n"
"	; peephole 55 set bit %3 of %1 (%2) directly instead of going through a.\n"
"	set	%3, %1 (%2)\n"
"	ld	a, %1 (%2)\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, %1\n"
"	res	%2, a\n"
"	ld	%1, a\n"
"} by {\n"
"	; peephole 56 reset bit %2 of %1 directly instead of going through a.\n"
"	res	%2, %1\n"
"	ld	a, %1\n"
"} if canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1 (%2)\n"
"	res	%3, a\n"
"	ld	%1 (%2), a\n"
"} by {\n"
"	; peephole 57 reset bit %3 of %1 (%2) directly instead of going through a.\n"
"	res	%3, %1 (%2)\n"
"	ld	a, %1 (%2)\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	c, %1 (%2)\n"
"	ld	b, %3 (%4)\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	; peephole 58 stored %1 (%2) %3 (%4) into hl directly instead of going through bc.\n"
"	ld	l, %1 (%2)\n"
"	ld	h, %3 (%4)\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	c, %1\n"
"	ld	b, %2\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	; peephole 59 stored %2%1 into hl directly instead of going through bc.\n"
"	ld	l, %1\n"
"	ld	h, %2\n"
"} if notUsed('bc'), operandsNotRelated(%2 'l')\n"
"\n"
"replace restart {\n"
"	jp	NC,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	C,%2\n"
"	; peephole 60 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	C,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NC,%2\n"
"	; peephole 61 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	NZ,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	Z,%2\n"
"	; peephole 62 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	Z,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NZ,%2\n"
"	; peephole 63 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	jp	%6\n"
"	; peephole 64 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	jp	%1,%6\n"
"	; peephole 65 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1\n"
"%2:\n"
"%1:\n"
"} by {\n"
"   ; peephole 65a eliminated jump.\n"
"%2:\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	ld	a,#0x00\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} by {\n"
"	ld	a,#0x00\n"
"	jp	%3\n"
"	; peephole 65a jumped directly to %3 instead of testing a first.\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} if labelRefCountChange(%3 +1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	jp	%4\n"
"} by {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	; peephole 65b removed unreachable jump to %3.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"%3:\n"
"} by {\n"
"	ld	%1, %2\n"
"%3:\n"
"	; peephole 65c removed redundant jump to %3.\n"
"} if labelRefCountChange(%3 -1)\n"
"\n"
"replace restart {\n"
"	ld	%1, #0x01\n"
"	bit	0, %1\n"
"	jp	Z, %2\n"
"} by {\n"
"	ld	%1, #0x01\n"
"	; peephole 65d removed impossible jump to %2.\n"
"} if labelRefCountChange(%2 -1)\n"
"	\n"
"replace restart {\n"
"	xor	a,a\n"
"	ld	a,#0x00\n"
"} by {\n"
"	xor	a,a\n"
"	; peephole 66 removed redundant load of 0 into a.\n"
"}\n"
"\n"
"replace {\n"
"	ld	e,#0x%1\n"
"	ld	d,#0x%2\n"
"} by {\n"
"	ld	de,#0x%2%1\n"
"	; peephole 67 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	d,#0x%1\n"
"	ld	e,#0x%2\n"
"} by {\n"
"	ld	de,#0x%1%2\n"
"	; peephole 67a combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	l,#0x%1\n"
"	ld	h,#0x%2\n"
"} by {\n"
"	ld	hl,#0x%2%1\n"
"	; peephole 68 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	h,#0x%1\n"
"	ld	l,#0x%2\n"
"} by {\n"
"	ld	hl,#0x%1%2\n"
"	; peephole 68a combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	c,#0x%1\n"
"	ld	b,#0x%2\n"
"} by {\n"
"	ld	bc,#0x%2%1\n"
"	; peephole 69 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	b,#0x%1\n"
"	ld	c,#0x%2\n"
"} by {\n"
"	ld	bc,#0x%1%2\n"
"	; peephole 69a combined constant loads into register pair.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	; peephole 70 removed redundant load from %1 into a.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	ld	%1,a\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 71 removed redundant load from a into %1.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	or	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,%2\n"
"	; peephole 72 removed load by reordering or arguments.\n"
"} if notVolatile(%1), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1\n"
"	; peephole 73 removed redundant or after or.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1 (%2)\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1 (%2)\n"
"	; peephole 74 removed redundant or after or.\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	and	a,%1\n"
"	; peephole 75 removed redundant or after and.\n"
"}\n"
"\n"
"replace restart {\n"
"	xor	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	xor	a,%1\n"
"	; peephole 76 removed redundant or after xor.\n"
"}\n"
"\n"
"replace restart {\n"
"	xor	a,%1 (%2)\n"
"	or	a,a\n"
"} by {\n"
"	xor	a,%1 (%2)\n"
"	; peephole 77 removed redundant or after xor.\n"
"}\n"
"\n"
"replace {\n"
"	ld	%1,%2\n"
"	ld	a,%2\n"
"} by {\n"
"	ld	a,%2\n"
"	ld	%1,a\n"
"	; peephole 78 load value in a first and use it next\n"
"} if notVolatile(%1 %2)\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	ld	%2,%1\n"
"	ld	%4,%3\n"
"} by {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	; peephole 79 removed redundant load from %3%1 into %4%2\n"
"} if notVolatile(%1 %2 %3 %4)\n"
"\n"
"replace restart {\n"
"	push	de\n"
"	inc	sp\n"
"	ld	a,e\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	push	de\n"
"	; peephole 80 pushed de\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	iy,%1\n"
"	add	iy,sp\n"
"	ld	sp,iy\n"
"} by {\n"
"	ld	hl,%1\n"
"	add	hl,sp\n"
"	ld	sp,hl\n"
"	; peephole 81 fixed stack using hl instead of iy.\n"
"} if notUsed('hl'), notUsed('iy')\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	a,%1\n"
"	cp	a,%2\n"
"	jp	%3,%4\n"
"	; peephole 82 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	cp	a,%2\n"
"	jp	%3,%4\n"
"	; peephole 83 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"}\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"} by {\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	Z,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 84 replaced sub a,#0xFF by inc a.\n"
"	jp	Z,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	NZ,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 85 replaced sub a,#0xFF by inc a.\n"
"	jp	NZ,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	rlca\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	rlca\n"
"	and	a,#0x01\n"
"	; peephole 86 replaced zero load, rla by and since rlca writes the same value to carry bit and least significant bit.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	push	%1\n"
"	pop	%4\n"
"	ld	%1,%3\n"
"} by {\n"
"	ld	%4,%2\n"
"	; peephole 87 moved %2 directly into de instead of going through %1.\n"
"	ld	%1,%3\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a,#0x00\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	adc	a,%4\n"
"} by {\n"
"	; peephole 88 removed lower part of multibyte addition.\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	add	a,%4\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, l\n"
"	add	a, #0x%1\n"
"	ld	e, a\n"
"	ld	a, h\n"
"	adc	a, #0x%2\n"
"	ld	d, a\n"
"} by {\n"
"	ld	de, #0x%2%1\n"
"	add	hl, de\n"
"	; peephole 89 used 16-bit addition.\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	a, h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	a, l\n"
"	add	a, #0x%1\n"
"	ld	c, a\n"
"	ld	a, h\n"
"	adc	a, #0x%2\n"
"	ld	b, a\n"
"} by {\n"
"	ld	bc, #0x%2%1\n"
"	add	hl,bc\n"
"	; peephole 90 used 16-bit addition.\n"
"	ld	c, l\n"
"	ld	b, h\n"
"	ld	a, h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"} by {\n"
"	; peephole 91 removed loads by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"} if notVolatile(%1), notUsed(%1), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix),a\n"
"	ld	a,#%2\n"
"	add	a,%1 (ix)\n"
"} by {\n"
"	ld	%1 (ix),a\n"
"	; peephole 92 removed loads by exploiting commutativity of addition.\n"
"	add	a,#%2\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	ld	a,(hl)\n"
"	inc	a\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	inc	(hl)\n"
"	; peephole 93 incremented in (hl) instead of going through a.\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	a,(hl)\n"
"	inc	a\n"
"	ld	(hl),a\n"
"} by {\n"
"	inc	(hl)\n"
"	; peephole 93' incremented in (hl) instead of going through a.\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	inc	%1\n"
"	ld	%2 (%3), %1\n"
"} by {\n"
"	inc	%2 (%3)\n"
"	ld	%1, %2 (%3)\n"
"	; peephole 93a incremented in %2 (%3) instead of going through %1.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	dec	%1\n"
"	ld	%2 (%3), %1\n"
"} by {\n"
"	dec	%2 (%3)\n"
"	ld	%1, %2 (%3)\n"
"	; peephole 93b decremented in %2 (%3) instead of going through %1.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"} by {\n"
"	ld	%1, a\n"
"	; peephole 94 removed load by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"} if notSame(%2 '(bc)' '(de)'), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	hl,#%1\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 95 removed loads by exploiting commutativity of addition.\n"
"	ld	bc,#%1\n"
"	add	hl,bc\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	add	hl,%2\n"
"	ld	bc,#%4\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 96 removed loads by exploiting commutativity of addition.\n"
"	ld	hl,#%1 + %4\n"
"	add	hl,%2\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	c,e\n"
"	ld	b,d\n"
"	ld	hl,#%1\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 97 removed loads by exploiting commutativity of addition.\n"
"	ld	hl,#%1\n"
"	add	hl,de\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 98 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 99 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 100 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	a\n"
"	jp	NZ,%1\n"
"	ld	%2,#0x00\n"
"} by {\n"
"	inc	a\n"
"	jp	NZ,%1\n"
"	ld	%2,a\n"
"	; peephole 101 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,#0x00\n"
"} by {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,a\n"
"	; peephole 102 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 103 shortened or using a (which has just been tested to be #0x00).\n"
"} if canAssign('b' %3)\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 104 shortened or using a (which has just been tested to be #0x00).\n"
"} if canAssign('b' %3)\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,a\n"
"	; peephole 105 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	(hl),#0x00\n"
"	inc	hl\n"
"	ld	(hl),#0x00\n"
"} by {\n"
"	xor	a, a\n"
"	; peephole 106 cached zero in a.\n"
"	ld	(hl), a\n"
"	inc	hl\n"
"	ld	(hl), a\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	add	hl,%2\n"
"	inc	hl\n"
"} by {\n"
"	ld	hl,#%1+1\n"
"	add	hl,%2\n"
"	; peephole 107 moved increment of hl to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"	ld	%1,#%2\n"
"	add	hl,%1\n"
"} by {\n"
"	ld	%1,#%2+1\n"
"	add	hl,%1\n"
"	; peephole 108 moved increment of hl to constant.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	dec	hl\n"
"	ld	%1,#%2\n"
"	add	hl,%1\n"
"} by {\n"
"	ld	%1,#%2-1\n"
"	add	hl,%1\n"
"	; peephole 109 moved decrement of hl to constant.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	inc	iy\n"
"	ld	%1, %2 (iy)\n"
"} by {\n"
"	ld	%1, %2+1 (iy)\n"
"	; peephole 110 moved increment of iy to offset.\n"
"} if notUsed('iy')\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	iy\n"
"	pop	hl\n"
"	inc	iy\n"
"} by {\n"
"	inc	hl\n"
"	push	hl\n"
"	pop	iy\n"
"	pop	hl\n"
"	; peephole 111 incremented in hl instead of iy.\n"
"}\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	iy\n"
"	inc	iy\n"
"} by {\n"
"	inc	hl\n"
"	push	hl\n"
"	pop	iy\n"
"	; peephole 111a incremented in hl instead of iy.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	push	bc\n"
"	pop	iy\n"
"	inc	iy\n"
"} by {\n"
"	inc	bc\n"
"	push	bc\n"
"	pop	iy\n"
"	; peephole 111b incremented in bc instead of iy.\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	push	de\n"
"	pop	iy\n"
"	inc	iy\n"
"} by {\n"
"	inc	de\n"
"	push	de\n"
"	pop	iy\n"
"	; peephole 111c incremented in de instead of iy.\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ld	hl,%1\n"
"	add	hl,%2\n"
"	push	hl\n"
"	pop	iy\n"
"} by {\n"
"	ld	iy,%1\n"
"	add	iy,%2\n"
"	; peephole 111b added in iy instead of hl.\n"
"} if notUsed('hl'), notSame(%2 'hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	sp,%1\n"
"} by {\n"
"	; peephole 112 removed redundant pop af.\n"
"	ld	sp,%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	inc	sp\n"
"	ld	sp,%1\n"
"} by {\n"
"	; peephole 113 removed redundant inc sp.\n"
"	ld	sp,%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	ret\n"
"} by {\n"
"	jp	%1\n"
"	; peephole 114 replaced call at end of function by jump (tail call optimization).\n"
"}\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	pop	ix\n"
"	ret\n"
"} by {\n"
"	pop	ix\n"
"	jp	%1\n"
"	; peephole115 replaced call at end of function by jump moving call beyond pop ix (tail call optimization).\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	%3,%4\n"
"	ld	%1,#%2\n"
"} by {\n"
"	ld	%1,#%2\n"
"	ld	%3,%4\n"
"	; peephole 116 removed load of #%2 into %1 since it's still there.\n"
"} if notVolatile(%1), operandsNotRelated(%3 %1)\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	de,#%1\n"
"} by {\n"
"	; peephole 117 used #%1 from hl for load into de.\n"
"	ld	hl,#%1\n"
"	ld	e,l\n"
"	ld	d,h\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix),l\n"
"	ld	%2 (ix),h\n"
"	ld	%3,%1 (ix)\n"
"	ld	%4,%2 (ix)\n"
"} by {	\n"
"	ld	%1 (ix),l\n"
"	ld	%2 (ix),h\n"
"	; peephole 118 used hl instead of %2 (ix), %1 (ix) to load %4%3.\n"
"	ld	%3,l\n"
"	ld	%4,h\n"
"} if operandsNotRelated('h' %3)\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	; peephole 119 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%5, a\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%5, a\n"
"	; peephole 120 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1), notSame(%5 %1), notSame(%5 '(hl)' '(de)' '(bc)'), notSame(%5 %6), notSame(%6 '(hl)' '(de)' '(bc)'), notSame(%5 'a'), notSame(%6 'a')\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, #%2\n"
"	adc	a, #%3\n"
"	ld	%5, a\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, #%2\n"
"	adc	a, #%3\n"
"	ld	%5, a\n"
"	; peephole 121 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1), notSame(%5 %1), notSame(%5 %6 '(hl)' '(de)' '(bc)'), notSame(%6 'a')\n"
"\n"
"replace restart {\n"
"	ld	hl, #%1\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"	ld	hl, #%3\n"
"	add	hl, de\n"
"} by {\n"
"	ld	hl, #%1+%3\n"
"	add	hl, %2\n"
"	; peephole 122 removed addition and loads exploiting commutativity of addition.\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ld	%1,l\n"
"	ld	%2,h\n"
"	ex	de,hl\n"
"	ld	(hl),%1\n"
"	inc	hl\n"
"	ld	(hl),%2\n"
"} by {\n"
"	ld	%1,l\n"
"	ex	de,hl\n"
"	; peephole 122a used de instead of going through %1%2.\n"
"	ld	(hl),e\n"
"	inc	hl\n"
"	ld	(hl),d\n"
"} if notUsed(%2), notSame(%1 'l' 'h' 'e' 'd'), notSame(%2 'l' 'h' 'e' 'd')\n"
"	\n"
"replace restart {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	hl, #0x0001\n"
"	add	hl, de\n"
"} by {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	inc	hl\n"
"	; peephole 123 replaced addition by increment.\n"
"}\n"
"\n"
"replace restart {	\n"
"	ld      sp,hl\n"
"	ld      hl,#0x0002\n"
"	add     hl,sp\n"
"} by {\n"
"	ld	sp, hl\n"
"	inc	hl\n"
"	inc	hl\n"
"	; peephole 124 replaced addition by increment.\n"
"}\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	ld	hl, #%1\n"
"	add	hl, de\n"
"} by {\n"
"	; peephole 125 removed ex exploiting commutativity of addition.\n"
"	ld	de, #%1\n"
"	add	hl, de\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	bc\n"
"	ex	de, hl\n"
"} by {\n"
"	push	bc\n"
"	; peephole 126 canceled subsequent ex de, hl.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	hl, #%1\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"	inc	de\n"
"} by {\n"
"	ld	hl, #%1+1\n"
"	; peephole 127 moved increment to constant.\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	a,#0x01\n"
"	jp	%1\n"
"%2:\n"
"	xor	a,a\n"
"%1:\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	xor	a,a\n"
"	jp	%1\n"
"%2:\n"
"	ld	a,#0x01\n"
"%1:\n"
"	; peephole 128 removed negation.\n"
"} if labelRefCount(%1 1)\n"
"\n"
"replace restart {\n"
"	and	a,#0x01\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	and	a,#0x01\n"
"	xor	a,#0x01\n"
"	; peephole 129 used xor for negation.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,a\n"
"	sub	a,#%1\n"
"} by {\n"
"	; peephole 130 removed redundant or.\n"
"	sub	a,#%1\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,#0x00\n"
"	rla\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	ld	a,#0x00\n"
"	ccf\n"
"	; peephole 131 moved negation from bit 0 to carry flag.\n"
"	rla\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, #<(%1)\n"
"	add	a, l\n"
"	ld	l, a\n"
"	ld	a, #>(%1)\n"
"	adc	a, h\n"
"	ld	h, a\n"
"	push	bc\n"
"} by {\n"
"	push	bc\n"
"	ld	bc, #%1\n"
"	add	hl, bc\n"
"	; peephole 132 used 16 bit addition by moving push bc\n"
"	ld	a, h\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	push	hl\n"
"} by {\n"
"	; peephole 133 used ex to move hl onto the stack.\n"
"	ex	(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	hl, #%1\n"
"	push	hl\n"
"} by {\n"
"	ld	hl, #%1\n"
"	; peephole 134 used ex to move hl onto the stack.\n"
"	ex	(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	inc	sp\n"
"	ld	hl,#%1\n"
"	push	hl\n"
"} by {\n"
"	inc	sp\n"
"	ld	hl,#%1\n"
"	; peephole 135 used ex to move #%1 onto the stack.\n"
"	ex	(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	ld	h,#%1\n"
"	ex	(sp),hl\n"
"	; peephole 136 used ex to move #%1 onto the stack.\n"
"	inc	sp\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	%3 (%1),a\n"
"%4:\n"
"	ld	%1,%5\n"
"} by {\n"
"	ld	(#%2 + %3),a\n"
"	; peephole 137 directly used #%2 instead of going through %1 using indirect addressing.\n"
"%4:\n"
"	ld	%1,%5\n"
"}\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	%1,#%2\n"
"	ld	%3 (%1),%4\n"
"	ld	%1,#%5\n"
"} by {\n"
"	ld	a,%4\n"
"	ld	(#%2 + %3),a\n"
"	; peephole 138 used #%2 directly instead of going through %1 using indirect addressing.\n"
"	pop	af\n"
"	ld	%1,#%5\n"
"} if notSame(%3 'a')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	bit	%2,%1\n"
"} by {\n"
"	bit	%2,a\n"
"	; peephole 139 tested bit %2 of a directly instead of going through %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	sbc	a,%1\n"
"	bit	7,a\n"
"	jp	Z,%2\n"
"} by {\n"
"	sbc	a,%1\n"
"	jp	P,%2\n"
"	; peephole 140 used sign flag instead of testing bit 7.\n"
"}\n"
"\n"
"replace restart {\n"
"	sbc	a,%1\n"
"	bit	7,a\n"
"	jp	NZ,%2\n"
"} by {\n"
"	sbc	a,%1\n"
"	jp	M,%2\n"
"	; peephole 141 used sign flag instead of testing bit 7.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	or	a,a\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,a\n"
"	jp	%3,%4\n"
"	; peephole 142 used value still in a instead of reloading from %1.\n"
"}\n"
"\n"
"replace {\n"
"	jp	%5\n"
"	ret\n"
"} by {\n"
"	jp	%5\n"
"	; peephole 143 removed unused ret.\n"
"}\n"
"\n"
"replace {\n"
"	jp	%5\n"
"	ld	sp,ix\n"
"	pop	ix\n"
"	ret\n"
"} by {\n"
"	jp	%5\n"
"	; peephole 144 removed unused ret.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	xor	a,a\n"
"	jp	%3\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	; peephole 145 removed redundant zeroing of a (which has just been tested to be #0x00).\n"
"	jp	%3\n"
"}\n"
"\n"
"barrier\n"
"\n"
"replace restart {\n"
"	ld	d,h\n"
"	ld	e,l\n"
"} by {\n"
"	; peephole 146 used ex to load hl into de.\n"
"	ex	de,hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	e,l\n"
"	ld	d,h\n"
"} by {\n"
"	; peephole 147 used ex to load hl into de.\n"
"	ex	de,hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,e\n"
"	ld	h,d\n"
"} by {\n"
"	; peephole 148 used ex to load de into hl.\n"
"	ex	de,hl\n"
"} if notUsed('de')\n"
"\n"
"barrier\n"
"\n"
"replace restart {\n"
"%1:\n"
"} by {\n"
"	; peephole 149 removed unused label %1.\n"
"} if labelRefCount(%1 0)\n"
"\n"
"barrier\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"barrier\n"
"\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	ret\n"
"	; peephole 151 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	ret	%1\n"
"	; peephole 152 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace {\n"
"	jp	%5\n"
"} by {\n"
"	jr	%5\n"
"	; peephole 153 changed absolute to relative unconditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	Z,%5\n"
"} by {\n"
"	jr	Z,%5\n"
"	; peephole 154 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	NZ,%5\n"
"} by {\n"
"	jr	NZ,%5\n"
"	; peephole 155 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	C,%5\n"
"} by {\n"
"	jr	C,%5\n"
"	; peephole 156 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	NC,%5\n"
"} by {\n"
"	jr	NC,%5\n"
"	; peephole 157 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
};

static char _gbz80_defaultRules[] = {
/* ROSE MODIFICATION (DQ (1/26/2014): Including header for initialization.
   include "peeph.rul"
   include "peeph-gbz80.rul"
*/
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"\n"
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"\n"
"replace restart {\n"
"	ld	%1,%1\n"
"} by {\n"
"	; peephole 1 removed redundant load.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	jp	NC,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	C,%2\n"
"	; peephole 60 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	C,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NC,%2\n"
"	; peephole 61 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	NZ,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	Z,%2\n"
"	; peephole 62 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	Z,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NZ,%2\n"
"	; peephole 63 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	jp	%6\n"
"	; peephole 64 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	jp	%1,%6\n"
"	; peephole 65 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	ld	a,#0x00\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} by {\n"
"	ld	a,#0x00\n"
"	jp	%3\n"
"	; peephole 65a jumped directly to %3 instead of testing a first.\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} if labelRefCountChange(%3 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1\n"
"%2:\n"
"%1:\n"
"} by {\n"
"   ; peephole 65a' eliminated jump.\n"
"%2:\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	jp	%4\n"
"} by {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	; peephole 65b removed unreachable jump to %3.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"%3:\n"
"} by {\n"
"	ld	%1, %2\n"
"%3:\n"
"	; peephole 65c removed redundant jump to %3.\n"
"} if labelRefCountChange(%3 -1)\n"
"\n"
"replace restart {\n"
"	ld	%1, #0x01\n"
"	bit	0, %1\n"
"	jp	Z, %2\n"
"} by {\n"
"	ld	%1, #0x01\n"
"	; peephole 65d removed impossible jump to %2.\n"
"} if labelRefCountChange(%2 -1)\n"
"\n"
"replace restart {\n"
"	xor	a,a\n"
"	ld	a,#0x00\n"
"} by {\n"
"	xor	a,a\n"
"	; peephole 10 removed redundant load of 0 into a.\n"
"}\n"
"\n"
"replace {\n"
"	ld	e,#0x%1\n"
"	ld	d,#0x%2\n"
"} by {\n"
"	ld	de,#0x%2%1\n"
"	; peephole 11 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	l,#0x%1\n"
"	ld	h,#0x%2\n"
"} by {\n"
"	ld	hl,#0x%2%1\n"
"	; peephole 12 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	c,#0x%1\n"
"	ld	b,#0x%2\n"
"} by {\n"
"	ld	bc,#0x%2%1\n"
"	; peephole 13 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	; peephole 14 removed redundant load from %1 into a.\n"
"} if notVolatile(%1), notSame(%1 '(hl+)'), notSame(%1 '(hl-)')\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	ld	%1,a\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 15 removed redundant load from a into %1.\n"
"} if notVolatile(%1), notSame(%1 '(hl+)'), notSame(%1 '(hl-)')\n"
"\n"
"replace restart {\n"
"	ld	%2,%3\n"
"	ld	a,%2\n"
"	and	a,%1\n"
"	ld	%2,%4\n"
"} by {\n"
"	ld	a,%3\n"
"	; peephole 16 moved %3 directly into a instead of going through %2.\n"
"	and	a,%1\n"
"	ld	%2,%4\n"
"} if notVolatile(%2), notSame(%1 %2)\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	or	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,%2\n"
"	; peephole 17 removed load by reordering or arguments.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	xor	a,a\n"
"	or	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,a\n"
"	; peephole 18 used value still in a instead of loading it from %1.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1\n"
"	; peephole 19 removed redundant or after or.\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	and	a,%1\n"
"	; peephole 20 removed redundant or after and.\n"
"}\n"
"\n"
"replace restart {\n"
"	xor	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	xor	a,%1\n"
"	; peephole 21 removed redundant or after xor.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	and	a,%2\n"
"	ld	%1,a\n"
"} by {\n"
"	; peephole 22 removed redundant load into %1.\n"
"	and	a,%2\n"
"	ld	%1,a\n"
"} if notVolatile(%1)\n"
"\n"
"replace {\n"
"	ld	%1,%2\n"
"	ld	a,%2\n"
"} by {\n"
"	ld	a,%2\n"
"	ld	%1,a\n"
"	; peephole 23 load value in a first and use it next\n"
"} if notVolatile(%1 %2)\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	ld	%2,%1\n"
"	ld	%4,%3\n"
"} by {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	; peephole 24 removed redundant load from %3%1 into %4%2\n"
"} if notVolatile(%1 %2 %3 %4)\n"
"\n"
"replace restart {\n"
"	ld	b,%1\n"
"	ld	a,b\n"
"	pop	bc\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 25 removed load into b\n"
"	pop	bc\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	c,%1\n"
"	ld	a,c\n"
"	pop	bc\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 26 removed load into c\n"
"	pop	bc\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	d,%1\n"
"	ld	a,d\n"
"	pop	de\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 27 removed load into d\n"
"	pop	de\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	e,%1\n"
"	ld	a,e\n"
"	pop	de\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 28 removed load into e\n"
"	pop	de\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	h,%1\n"
"	ld	a,h\n"
"	pop	hl\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 29 removed load into h\n"
"	pop	hl\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	l,%1\n"
"	ld	a,l\n"
"	pop	hl\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 30 removed load into l\n"
"	pop	hl\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,c\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%2\n"
"	push	af\n"
"	inc	sp\n"
"	call	%3\n"
"} by {\n"
"	ld	b,c\n"
"	ld	c,#%2\n"
"	push	bc\n"
"	; peephole 31 moved and pushed arguments c and #%2 through bc instead of pushing them individually.\n"
"	call	%3\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,e\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%2\n"
"	push	af\n"
"	inc	sp\n"
"	call	%3\n"
"} by {\n"
"	ld	d,e\n"
"	ld	e,#%2\n"
"	push	de\n"
"	; peephole 32 moved and pushed arguments e and #%2 through de instead of pushing them individually.\n"
"	call	%3\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	a,%1\n"
"	cp	a,%2\n"
"	jp	%3,%4\n"
"	; peephole 33 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	cp	a,#%2\n"
"	jp	%3,%4\n"
"	; peephole 34 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"}\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"} by {\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	Z,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 35 replaced sub a,#0xFF by inc a.\n"
"	jp	Z,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	NZ,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 36 replaced sub a,#0xFF by inc a.\n"
"	jp	NZ,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1 + %2\n"
"	ld	a,c\n"
"	add	a,%3\n"
"	ld	c,a\n"
"	ld	a,b\n"
"	adc	a,%4\n"
"	ld	b,a\n"
"} by {\n"
"	ld	a,#<(%1 + %2)\n"
"	add	a,%3\n"
"	ld	c,a\n"
"	ld	a,#>(%1 + %2)\n"
"	; peephole 37 directly used (%1 + %2) in calculation instead of placing it in bc first.\n"
"	adc	a,%4\n"
"	ld	b,a\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	de,#%1 + %2\n"
"	ld	a,e\n"
"	add	a,%3\n"
"	ld	e,a\n"
"	ld	a,d\n"
"	adc	a,%4\n"
"	ld	d,a\n"
"} by {\n"
"	ld	a,#<(%1 + %2)\n"
"	add	a,%3\n"
"	ld	e,a\n"
"	ld	a,#>(%1 + %2)\n"
"	; peephole 38 directly used (%1 + %2) in calculation instead of placing it in de first.\n"
"	adc	a,%4\n"
"	ld	d,a\n"
"}\n"
"\n"
"replace restart {\n"
"	rlca\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	rlca\n"
"	and	a,#0x01\n"
"	; peephole 39 replaced zero load, rla by and since rlca writes the same value to carry bit and least significant bit.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	push	%1\n"
"	pop	%4\n"
"	ld	%1,%3\n"
"} by {\n"
"	ld	%4,%2\n"
"	; peephole 40 moved %2 directly into de instead of going through %1.\n"
"	ld	%1,%3\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a,#0x00\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	adc	a,%4\n"
"} by {\n"
"	; peephole 41 removed lower part of multibyte addition.\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	add	a,%4\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"	ld	%1,a\n"
"} by {\n"
"	; peephole 42 removed loads by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"	ld	%1,a\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	sla	%1\n"
"	ld	a, %2\n"
"	//add	%3, %4\n"
"} by {\n"
"	add	a, a\n"
"	; peephole 42a shifts in accumulator insted of %1\n"
"	ld	%1, a\n"
"	ld	a, %2\n"
"	//add	%3, %4\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"} by {\n"
"	ld	%1, a\n"
"	; peephole 43 removed load by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	xor	a,a\n"
"	or	a,%3\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	; peephole 44 removed redundant zeroing of a (which has just been tested to be #0x00).\n"
"	or	a,%3\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 45 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 46 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 47 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,#0x00\n"
"} by {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,a\n"
"	; peephole 48 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 50 shortened or using a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 51 shortened or using a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	a,%2\n"
"	or	a,a\n"
"} by {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	or	a,%2\n"
"	; peephole 52 shortened or using a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,a\n"
"	; peephole 53 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,#0x00\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,a\n"
"	; peephole 54 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,#0x00\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,a\n"
"	; peephole 55 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	push	%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	push	%2\n"
"	ld	%3,a\n"
"	; peephole 56 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	de,#%1 + %2\n"
"	inc	de\n"
"	inc	de\n"
"	inc	de\n"
"} by {\n"
"	ld	de,#%1 + %2 + 3\n"
"	; peephole 57 moved triple increment of de to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	de,#%1 + %2\n"
"	inc	de\n"
"	inc	de\n"
"} by {\n"
"	ld	de,#%1 + %2 + 2\n"
"	; peephole 58 moved double increment of de to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	de,#%1 + %2\n"
"	inc	de\n"
"} by {\n"
"	ld	de,#%1 + %2 + 1\n"
"	; peephole 59 moved increment of de to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1 + %2\n"
"	inc	bc\n"
"	inc	bc\n"
"	inc	bc\n"
"} by {\n"
"	ld	bc,#%1 + %2 + 3\n"
"	; peephole 60 moved triple increment of bc to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1 + %2\n"
"	inc	bc\n"
"	inc	bc\n"
"} by {\n"
"	ld	bc,#%1 + %2 + 2\n"
"	; peephole 61 moved double increment of bc to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1 + %2\n"
"	inc	bc\n"
"} by {\n"
"	ld	bc,#%1 + %2 + 1\n"
"	; peephole 62 moved increment of bc to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1\n"
"	ld	a,c\n"
"	add	a,#0x%2\n"
"	ld	c,a\n"
"	ld	a,b\n"
"	adc	a,#0x%3\n"
"	ld	b,a\n"
"} by {\n"
"	ld	bc,#%1 + 0x%3%2\n"
"	; peephole 63 moved addition of constant 0x%3%2 to bc to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	bc,#%1 + %4\n"
"	ld	a,c\n"
"	add	a,#0x%2\n"
"	ld	c,a\n"
"	ld	a,b\n"
"	adc	a,#0x%3\n"
"	ld	b,a\n"
"} by {\n"
"	ld	bc,#%1 + %4 + 0x%3%2\n"
"	; peephole 64 moved addition of constant 0x%3%2 to bc to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	ret\n"
"} by {\n"
"	jp	%1\n"
"	; peephole 65 replaced call at end of function by jump.\n"
"}\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	pop	ix\n"
"	ret\n"
"} by {\n"
"	pop	ix\n"
"	jp	%1\n"
"	; peephole 66 replaced call at end of function by jump moving call beyond pop ix.\n"
"}\n"
"\n"
"replace {\n"
"	ld	a,(hl)\n"
"	inc	hl\n"
"} by {\n"
"	ld	a,(hl+)\n"
"	; peephole GB1 used ldi to increment hl after load\n"
"}\n"
"\n"
"replace {\n"
"	ld	a,(hl)\n"
"	dec	hl\n"
"} by {\n"
"	ld	a,(hl-)\n"
"	; peephole GB2 used ldd to decrement hl after load\n"
"}\n"
"\n"
"replace {\n"
"	ld	(hl),a\n"
"	inc	hl\n"
"} by {\n"
"	ld	(hl+),a\n"
"	; peephole GB3 used ldi to increment hl after load\n"
"}\n"
"\n"
"replace {\n"
"	ld	(hl),a\n"
"	dec	hl\n"
"} by {\n"
"	ld	(hl-),a\n"
"	; peephole GB4 used ldd to decrement hl after load\n"
"}\n"
"\n"
"replace {\n"
"	inc hl\n"
"	dec hl\n"
"} by {\n"
"	; peephole GB5 removed inc hl / dec hl pair\n"
"}\n"
"\n"
"\n"
"replace {\n"
"	ld	(hl),a\n"
"	inc	de\n"
"	ld	a,(de)\n"
"	inc	hl\n"
"} by {\n"
"	ld	(hl+),a\n"
"	; peephole E used ldi to increment hl\n"
"	inc	de\n"
"	ld	a,(de)\n"
"}\n"
"\n"
"barrier\n"
"\n"
"replace restart {\n"
"%1:\n"
"} by {\n"
"	; peephole 149 removed unused label %1.\n"
"} if labelRefCount(%1 0)\n"
"\n"
"barrier\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"barrier\n"
"\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	ret\n"
"	; peephole 151 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	ret	%1\n"
"	; peephole 152 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace {\n"
"	jp	%5\n"
"} by {\n"
"	jr	%5\n"
"	; peephole 153 changed absolute to relative unconditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	%1,%5\n"
"} by {\n"
"	jr	%1,%5\n"
"	; peephole 154 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
};

static char _r2k_defaultRules[] = {
/* ROSE MODIFICATION (DQ (1/26/2014): Including header for initialization.
   include "peeph.rul"
   include "peeph-r2k.rul"
*/
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"\n"
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"replace restart {\n"
"	ld	%1, %1\n"
"} by {\n"
"	; peephole 0 removed redundant load.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"} by {\n"
"	; peephole 1 removed dead load from %2 into %1.\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	add	ix,sp\n"
"} by {\n"
"	; peephole 1a removed dead frame pointer setup.\n"
"} if notUsed('ix')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 + %3\n"
"} by {\n"
"	; peephole 2 removed dead load from %2 + %3 into %1.\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, (iy)\n"
"} by {\n"
"	ld	%1, 0 (iy)\n"
"	; peephole 3 made 0 offset explicit.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	(iy), %1\n"
"} by {\n"
"	ld	0 (iy), %1\n"
"	; peephole 4 made 0 offset explicit.\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"} by {\n"
"	; peephole 5 removed dead increment of hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	dec	hl\n"
"} by {\n"
"	; peephole 6 removed dead decrement of hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (iy)\n"
"} by {\n"
"	; peephole 7 removed dead load from %2 (iy) into %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (ix)\n"
"} by {\n"
"	; peephole 8 removed dead load from %2 (ix) into %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3, %1\n"
"} by {\n"
"	; peephole 9 loaded %3 from %2 directly instead of going through %1.\n"
"	ld	%3, %2\n"
"} if canAssign(%3 %2), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3, %4\n"
"	ld	%5, %1\n"
"} by {\n"
"	ld	%5, %2\n"
"	; peephole 10 loaded %5 from %2 directly instead of going through %1.\n"
"	ld	%3, %4\n"
"} if canAssign(%5 %2), notVolatile(%1), operandsNotRelated(%1 %4), operandsNotRelated(%1 %3), operandsNotRelated(%4 %5), notUsed(%1), notSame(%3 %4 '(hl)' '(de)' '(bc)'), notVolatile(%5)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %1\n"
"} by {\n"
"	; peephole 11 loaded %2 (%3) into %4 directly instead of going through %1.\n"
"	ld	%4, %2 (%3)\n"
"} if canAssign(%4 %2 %3), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	ld	%3 (%4), %1\n"
"} by {\n"
"	; peephole 12 loaded %2 into %3 (%4) directly instead of going through %1.\n"
"	ld	%3 (%4), %2\n"
"} if canAssign(%3 %4 %2), notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %5 (%6)\n"
"	ld	%7, %1\n"
"} by {\n"
"	ld	%7, %2 (%3)\n"
"	; peephole 13 loaded %2 (%3) into %7 directly instead of going through %1.\n"
"	ld	%4, %5 (%6)\n"
"} if canAssign(%7 %2 %3), notVolatile(%1), notUsed(%1), notSame(%1 %4), notSame(%7 %4)\n"
"\n"
"replace restart {\n"
"	ld	%1, %7\n"
"	ld	%5 (%6), %4\n"
"	ld	%2 (%3), %1\n"
"} by {\n"
"	ld	%5 (%6), %4\n"
"	; peephole 14 loaded %7 into %2 (%3) directly instead of going through %1.\n"
"	ld	%2 (%3), %7\n"
"} if canAssign(%2 %3 %7), notVolatile(%1), notUsed(%1), notSame(%1 %4)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	ld	%4, %5\n"
"	ld	%7, %1\n"
"} by {\n"
"	ld	%7, %2 (%3)\n"
"	; peephole 15 loaded %2 (%3) into %7 directly instead of going through %1.\n"
"	ld	%4, %5\n"
"} if canAssign(%7 %2 %3), notVolatile(%1), notUsed(%1), notSame(%1 %5), notSame(%7 %4), notSame(%7 %5), notSame(%4 '(hl)' '(de)' '(bc)'), notSame(%5 '(hl)' '(de)' '(bc)' '(iy)')\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	a,%3 (%1)\n"
"} by {\n"
"	; peephole 16 loaded %2 into a directly instead of going through %1.\n"
"	ld	a,(#%2 + %3)\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	a,(hl)\n"
"} by {\n"
"	ld	a,(#%1)\n"
"	; peephole 17 loaded a from (#%1) directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1 + %2\n"
"	ld	a,(hl)\n"
"} by {\n"
"	; peephole 18 loaded %2 into a directly instead of using hl.\n"
"	ld	a,(#%1 + %2)\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	(#%1),a\n"
"	; peephole 19 loaded (#%1) from a directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	(#%1 + %2),a\n"
"	; peephole 20 loaded (#%1) from a directly instead of using hl.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	srl	%1\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 21 shifted in a instead of %1.\n"
"	srl	a\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	a, (de)\n"
"	srl	a\n"
"	ld	(de), a\n"
"} by {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	srl	(hl)\n"
"	; peephole 21a shifted in (hl) instead of a.\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	ld	%2, %3 (iy)\n"
"	srl	%2\n"
"	bit %4, %3 (iy)\n"
"} by {\n"
"	ld	hl, #%1 + %3\n"
"	; peephole 21c used hl instead of iy.\n"
"	ld	%2, (hl)\n"
"	srl %2\n"
"	bit %4, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,(hl)\n"
"	ld	a,%2 (%3)\n"
"	sub	a,%1\n"
"} by {\n"
"	ld	a,%2 (%3)\n"
"	; peephole 22 used (hl) in sub directly instead of going through %1.\n"
"	sub	a,(hl)\n"
"} if notVolatile(%1), notUsed(%1)\n"
"\n"
"replace restart {\n"
"	inc	bc\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	ld	l,c\n"
"	ld	h,b\n"
"	; peephole 23 incremented in hl instead of bc.\n"
"	inc	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	inc	de\n"
"	ld	l,e\n"
"	ld	h,d\n"
"} by {\n"
"	ld	l,e\n"
"	ld	h,d\n"
"	; peephole 24 incremented in hl instead of de.\n"
"	inc	hl\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	a,#%1\n"
"	ld	(bc),a\n"
"} by {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	(hl),#%1\n"
"	; peephole 25 loaded #%1 into (hl) instead of (bc).\n"
"}\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	de\n"
"} by {\n"
"	; peephole 26 pushed hl directly instead of going through de.\n"
"	push	hl\n"
"} if notUsed('de'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,%1\n"
"	ld	h,d\n"
"	push	hl\n"
"} by {\n"
"	; peephole 27 pushed de instead of hl removing a load.\n"
"	ld	e,%1\n"
"	push	de\n"
"} if notUsed('hl'), notUsed('e')\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	bc\n"
"	push	de\n"
"} by {\n"
"	; peephole 28 pushed hl directly instead of going through de.\n"
"	push	bc\n"
"	push	hl\n"
"} if notUsed('de'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,c\n"
"	ld	h,b\n"
"	push	hl\n"
"} by {\n"
"	; peephole 29 pushed bc directly instead of going through hl.\n"
"	push	bc\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,%1\n"
"	ld	h,b\n"
"	push	hl\n"
"} by {\n"
"	; peephole 30 pushed bc instead of hl removing a load.\n"
"	ld	c,%1\n"
"	push	bc\n"
"} if notUsed('hl'), notUsed('c')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	push	%1\n"
"	push	bc\n"
"} by {\n"
"	; peephole 31 pushed hl directly instead of going through bc.\n"
"	push	%1\n"
"	push	hl\n"
"} if notUsed('bc'), notSame(%1 'bc')\n"
"\n"
"replace restart {\n"
"	pop	de\n"
"	ld	l, e\n"
"	ld	h, d\n"
"} by {\n"
"	; peephole 32 popped hl directly instead of going through de.\n"
"	pop	hl\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	pop	bc\n"
"	ld	l, c\n"
"	ld	h, b\n"
"} by {\n"
"	; peephole 33 popped hl directly instead of going through bc.\n"
"	pop	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix), %2\n"
"	ld	%3, %1 (ix)\n"
"} by {\n"
"	; peephole 34 loaded %3 from %2 instead of going through %1 (ix).\n"
"	ld	%1 (ix), %2\n"
"	ld	%3, %2\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix), a\n"
"	push	de\n"
"	ld	%2, %1 (ix)\n"
"} by {\n"
"	ld	%1 (ix), a\n"
"	push	de\n"
"	; peephole 34a loaded %2 from a instead of %1 (ix)\n"
"	ld	%2, a\n"
"}	\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,e\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 35 pushed de instead of pushing a twice.\n"
"	ld	d,a\n"
"	push	de\n"
"} if notUsed('d'), notUsed('a')\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 36 pushed de instead of pushing a twice.\n"
"	ld	d,a\n"
"	ld	e,#%1\n"
"	push	de\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 37 pushed bc instead of pushing a twice.\n"
"	ld	b,a\n"
"	ld	c,#%1\n"
"	push	bc\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	push	bc\n"
"	inc	sp\n"
"	push	de\n"
"	inc	sp\n"
"} by {\n"
"	ld	c, d\n"
"	; peephole 37a combined pushing of b and d.\n"
"	push	bc\n"
"} if notUsed('c')\n"
"\n"
"replace restart {\n"
"	push	bc\n"
"	inc	sp\n"
"	ld	a, c\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	push	bc\n"
"	ld	a, c\n"
"	; peephole 38 simplified pushing bc.\n"
"}\n"
"\n"
"replace restart {\n"
"	push	de\n"
"	inc	sp\n"
"	ld	a, #%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	ld	e, #%1\n"
"	push	de\n"
"	; peephole 39 simplified pushing de.\n"
"} if notUsed('e')\n"
"\n"
"replace restart {\n"
"	ld	a,#%1\n"
"	ld	d,a\n"
"} by {\n"
"	; peephole 40 loaded #%1 into d directly instead of going through a.\n"
"	ld	d,#%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	%2,%1\n"
"} by {\n"
"	; peephole 41 loaded %2 from a directly instead of going through %1.\n"
"	ld	%2,a\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	a,%1 (ix)\n"
"	push	af\n"
"	inc	sp\n"
"	ld	a,%2 (ix)\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	; peephole 42 pushed %1 (ix), %2(ix) through hl instead of af.\n"
"	ld	h,%1 (ix)\n"
"	ld	l,%2 (ix)\n"
"	push	hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	c, l\n"
"	ld	b, h\n"
"	push	bc\n"
"} by {\n"
"	; peephole 43 pushed hl instead of bc.\n"
"	push	hl\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	pop	%1\n"
"	push	%1\n"
"} by {\n"
"	; peephole 44 eleminated dead pop/push pair.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	or	a,%2 (iy)\n"
"} by {\n"
"	; peephole 45 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	or	a,(hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2,%3 (iy)\n"
"} by {\n"
"	; peephole 46 used hl instead of iy.\n"
"	ld	hl,#%1 + %3\n"
"	ld	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	h,%3 (iy)\n"
"} by {\n"
"	; peephole 46a used hl instead of iy.\n"
"	ld	hl,#%1 + %3\n"
"	ld	h, (hl)\n"
"} if notUsed('iy'), notUsed('l')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (iy), %3\n"
"} by {\n"
"	; peephole 46b used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl), %3\n"
"} if notUsed('iy'), notUsed('hl'), notSame(%3 'h' 'l')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2,0 (iy)\n"
"	ld	%3,1 (iy)\n"
"} by {\n"
"	; peephole 47 used hl instead of iy.\n"
"	ld	hl,#%1\n"
"	ld	%2, (hl)\n"
"	inc	hl\n"
"	ld	%3, (hl)\n"
"} if notUsed('iy'), notUsed('hl'), operandsNotRelated(%2 'hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (iy),%3\n"
"	ld	l,%2 (iy)\n"
"} by {\n"
"	; peephole 48 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl),%3\n"
"	ld	l,(hl)\n"
"} if notUsed('iy'), notUsed('h')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	ld	%2 (%3), %4\n"
"} by {\n"
"	; peephole 49 used hl instead of iy.\n"
"	ld	hl,#%1 + %2\n"
"	ld	(hl), %4\n"
"} if notUsed('iy'), notUsed('hl'), operandsNotRelated(%4 'hl')\n"
"\n"
"replace restart {\n"
"	ld	iy,#%1\n"
"	bit	%2,%3 (iy)\n"
"} by {\n"
"	; peephole 49a used hl instead of iy.\n"
"	ld	hl,#%1+%3\n"
"	bit	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	%2, %3 (iy)\n"
"} by {\n"
"	; peephole 49b used hl instead of iy.\n"
"	ld	hl, #%1+%3\n"
"	add	hl, sp\n"
"	ld	%2, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	%2, 0 (iy)\n"
"	ld	%3, 1 (iy)\n"
"} by {\n"
"	; peephole 49c used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	%2, (hl)\n"
"	inc	hl\n"
"	ld	%3, (hl)\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	l, 0 (iy)\n"
"	ld	h, 1 (iy)\n"
"} by {\n"
"	; peephole 49d used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	a, (hl)\n"
"	inc	hl\n"
"	ld	h, (hl)\n"
"	ld	l, a\n"
"} if notUsed('iy'), notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	0 (iy), #%2\n"
"	ld	1 (iy), #%3\n"
"} by {\n"
"	; peephole 49e used hl instead of iy.\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	(hl), #%2\n"
"	inc	hl\n"
"	ld	(hl), #%3\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	ld	a, 1 (iy)\n"
"	or	a, 0 (iy)\n"
"} by {\n"
"	ld	hl, #%1\n"
"	ld	a, (hl)\n"
"	inc	hl\n"
"	or	a, (hl)\n"
"	; peephole 49f used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	ld	a, 1 (iy)\n"
"	or	a, 0 (iy)\n"
"} by {\n"
"	ld	hl, #%1\n"
"	add	hl, sp\n"
"	ld	a, (hl)\n"
"	inc	hl\n"
"	or	a, (hl)\n"
"	; peephole 49f' used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	bit	%2, %3 (iy)\n"
"} by {\n"
"	ld	hl, #%1+%3\n"
"	add	hl, sp\n"
"	bit	%2, (hl)\n"
"	; peephole 49g used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	iy, #%1\n"
"	add	iy, sp\n"
"	or	a, %2 (iy)\n"
"} by {\n"
"	ld	hl, #%1+%2\n"
"	add	hl, sp\n"
"	or	a, (hl)\n"
"	; peephole 49h used hl instead of iy.\n"
"} if notUsed('iy'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,(hl)\n"
"	or	a,%1\n"
"} by {\n"
"	or	a,(hl)\n"
"	; peephole 50 used (hl) directly instead of going through %1.\n"
"} if notUsed(%1), notSame(%1 'a')\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	inc	bc\n"
"} by {\n"
"	; peephole 51 incremented in hl instead of bc.\n"
"	inc	hl\n"
"	ld	c,l\n"
"	ld	b,h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	a,%1 (%2)\n"
"	bit	%3,a\n"
"} by {\n"
"	; peephole 52 tested bit of %1 (%2) directly instead of going through a.\n"
"	bit	%3,%1 (%2)\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	bit	%2,a\n"
"} by {\n"
"	; peephole 53 tested bit %2 of %1 directly instead of going through a.\n"
"	bit	%2,%1\n"
"} if notUsed('a'), canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1\n"
"	set	%2, a\n"
"	ld	%1, a\n"
"} by {\n"
"	; peephole 54 set bit %2 of %1 directly instead of going through a.\n"
"	set	%2, %1\n"
"	ld	a, %1\n"
"} if canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1 (%2)\n"
"	set	%3, a\n"
"	ld	%1 (%2), a\n"
"} by {\n"
"	; peephole 55 set bit %3 of %1 (%2) directly instead of going through a.\n"
"	set	%3, %1 (%2)\n"
"	ld	a, %1 (%2)\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, %1\n"
"	res	%2, a\n"
"	ld	%1, a\n"
"} by {\n"
"	; peephole 56 reset bit %2 of %1 directly instead of going through a.\n"
"	res	%2, %1\n"
"	ld	a, %1\n"
"} if canAssign(%1 'b')\n"
"\n"
"replace restart {\n"
"	ld	a, %1 (%2)\n"
"	res	%3, a\n"
"	ld	%1 (%2), a\n"
"} by {\n"
"	; peephole 57 reset bit %3 of %1 (%2) directly instead of going through a.\n"
"	res	%3, %1 (%2)\n"
"	ld	a, %1 (%2)\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	c, %1 (%2)\n"
"	ld	b, %3 (%4)\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	; peephole 58 stored %1 (%2) %3 (%4) into hl directly instead of going through bc.\n"
"	ld	l, %1 (%2)\n"
"	ld	h, %3 (%4)\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	c, %1\n"
"	ld	b, %2\n"
"	ld	l,c\n"
"	ld	h,b\n"
"} by {\n"
"	; peephole 59 stored %2%1 into hl directly instead of going through bc.\n"
"	ld	l, %1\n"
"	ld	h, %2\n"
"} if notUsed('bc'), operandsNotRelated(%2 'l')\n"
"\n"
"replace restart {\n"
"	jp	NC,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	C,%2\n"
"	; peephole 60 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	C,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NC,%2\n"
"	; peephole 61 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	NZ,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	Z,%2\n"
"	; peephole 62 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	Z,%1\n"
"	jp	%2\n"
"%1:\n"
"} by {\n"
"	jp	NZ,%2\n"
"	; peephole 63 removed jp by using inverse jump logic\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	jp	%6\n"
"	; peephole 64 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	jp	%1,%6\n"
"	; peephole 65 jumped to %6 directly instead of via %5.\n"
"} if labelIsUncondJump(), notSame(%5 %6), labelRefCountChange(%5 -1), labelRefCountChange(%6 +1)\n"
"\n"
"replace restart {\n"
"	jp	%1\n"
"%2:\n"
"%1:\n"
"} by {\n"
"   ; peephole 65a eliminated jump.\n"
"%2:\n"
"%1:\n"
"} if labelRefCountChange(%1 -1)\n"
"\n"
"replace restart {\n"
"	ld	a,#0x00\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} by {\n"
"	ld	a,#0x00\n"
"	jp	%3\n"
"	; peephole 65a jumped directly to %3 instead of testing a first.\n"
"%1:\n"
"	bit	%2,a\n"
"	jp	Z,%3\n"
"} if labelRefCountChange(%3 +1)\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	jp	%4\n"
"} by {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"	; peephole 65b removed unreachable jump to %3.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1, %2\n"
"	jp	%3\n"
"%3:\n"
"} by {\n"
"	ld	%1, %2\n"
"%3:\n"
"	; peephole 65c removed redundant jump to %3.\n"
"} if labelRefCountChange(%3 -1)\n"
"\n"
"replace restart {\n"
"	ld	%1, #0x01\n"
"	bit	0, %1\n"
"	jp	Z, %2\n"
"} by {\n"
"	ld	%1, #0x01\n"
"	; peephole 65d removed impossible jump to %2.\n"
"} if labelRefCountChange(%2 -1)\n"
"	\n"
"replace restart {\n"
"	xor	a,a\n"
"	ld	a,#0x00\n"
"} by {\n"
"	xor	a,a\n"
"	; peephole 66 removed redundant load of 0 into a.\n"
"}\n"
"\n"
"replace {\n"
"	ld	e,#0x%1\n"
"	ld	d,#0x%2\n"
"} by {\n"
"	ld	de,#0x%2%1\n"
"	; peephole 67 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	l,#0x%1\n"
"	ld	h,#0x%2\n"
"} by {\n"
"	ld	hl,#0x%2%1\n"
"	; peephole 68 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace {\n"
"	ld	c,#0x%1\n"
"	ld	b,#0x%2\n"
"} by {\n"
"	ld	bc,#0x%2%1\n"
"	; peephole 69 combined constant loads into register pair.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	; peephole 70 removed redundant load from %1 into a.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	ld	%1,a\n"
"} by {\n"
"	ld	a,%1\n"
"	; peephole 71 removed redundant load from a into %1.\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	or	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,%2\n"
"	; peephole 72 removed load by reordering or arguments.\n"
"} if notVolatile(%1), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1\n"
"	; peephole 73 removed redundant or after or.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1 (%2)\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1 (%2)\n"
"	; peephole 74 removed redundant or after or.\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	and	a,%1\n"
"	; peephole 75 removed redundant or after and.\n"
"}\n"
"\n"
"replace restart {\n"
"	xor	a,%1\n"
"	or	a,a\n"
"} by {\n"
"	xor	a,%1\n"
"	; peephole 76 removed redundant or after xor.\n"
"}\n"
"\n"
"replace restart {\n"
"	xor	a,%1 (%2)\n"
"	or	a,a\n"
"} by {\n"
"	xor	a,%1 (%2)\n"
"	; peephole 77 removed redundant or after xor.\n"
"}\n"
"\n"
"replace {\n"
"	ld	%1,%2\n"
"	ld	a,%2\n"
"} by {\n"
"	ld	a,%2\n"
"	ld	%1,a\n"
"	; peephole 78 load value in a first and use it next\n"
"} if notVolatile(%1 %2)\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	ld	%2,%1\n"
"	ld	%4,%3\n"
"} by {\n"
"	ld	%1,%2\n"
"	ld	%3,%4\n"
"	; peephole 79 removed redundant load from %3%1 into %4%2\n"
"} if notVolatile(%1 %2 %3 %4)\n"
"\n"
"replace restart {\n"
"	push	de\n"
"	inc	sp\n"
"	ld	a,e\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	push	de\n"
"	; peephole 80 pushed de\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	iy,%1\n"
"	add	iy,sp\n"
"	ld	sp,iy\n"
"} by {\n"
"	ld	hl,%1\n"
"	add	hl,sp\n"
"	ld	sp,hl\n"
"	; peephole 81 fixed stack using hl instead of iy.\n"
"} if notUsed('hl'), notUsed('iy')\n"
"\n"
"replace restart {\n"
"	ld	a,%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	a,%1\n"
"	cp	a,%2\n"
"	jp	%3,%4\n"
"	; peephole 82 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"} if notVolatile(%1)\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"	sub	a,%2\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	cp	a,%2\n"
"	jp	%3,%4\n"
"	; peephole 83 removed load by replacing sub with cp\n"
"	assert	a=%1\n"
"}\n"
"\n"
"replace restart {\n"
"	assert	a=%1\n"
"} by {\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	Z,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 84 replaced sub a,#0xFF by inc a.\n"
"	jp	Z,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,#0xFF\n"
"	jp	NZ,%1\n"
"} by {\n"
"	inc	a\n"
"	; peephole 85 replaced sub a,#0xFF by inc a.\n"
"	jp	NZ,%1\n"
"}\n"
"\n"
"replace restart {\n"
"	rlca\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	rlca\n"
"	and	a,#0x01\n"
"	; peephole 86 replaced zero load, rla by and since rlca writes the same value to carry bit and least significant bit.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,%2\n"
"	push	%1\n"
"	pop	%4\n"
"	ld	%1,%3\n"
"} by {\n"
"	ld	%4,%2\n"
"	; peephole 87 moved %2 directly into de instead of going through %1.\n"
"	ld	%1,%3\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a,#0x00\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	adc	a,%4\n"
"} by {\n"
"	; peephole 88 removed lower part of multibyte addition.\n"
"	ld	%2,a\n"
"	ld	a,%3\n"
"	add	a,%4\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, l\n"
"	add	a, #0x%1\n"
"	ld	e, a\n"
"	ld	a, h\n"
"	adc	a, #0x%2\n"
"	ld	d, a\n"
"} by {\n"
"	ld	de, #0x%2%1\n"
"	add	hl, de\n"
"	; peephole 89 used 16-bit addition.\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	a, h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	a, l\n"
"	add	a, #0x%1\n"
"	ld	c, a\n"
"	ld	a, h\n"
"	adc	a, #0x%2\n"
"	ld	b, a\n"
"} by {\n"
"	ld	bc, #0x%2%1\n"
"	add	hl,bc\n"
"	; peephole 90 used 16-bit addition.\n"
"	ld	c, l\n"
"	ld	b, h\n"
"	ld	a, h\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"} by {\n"
"	; peephole 91 removed loads by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"} if notVolatile(%1), notUsed(%1), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix),a\n"
"	ld	a,#%2\n"
"	add	a,%1 (ix)\n"
"} by {\n"
"	ld	%1 (ix),a\n"
"	; peephole 92 removed loads by exploiting commutativity of addition.\n"
"	add	a,#%2\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	ld	a,(hl)\n"
"	inc	a\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	ld	(hl),a\n"
"} by {\n"
"	ld	l,%1 (ix)\n"
"	ld	h,%2 (ix)\n"
"	inc	(hl)\n"
"	; peephole 93 incremented in (hl) instead of going through a.\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	%1, %2 (%3)\n"
"	inc	%1\n"
"	ld	%2 (%3), %1\n"
"} by {\n"
"	inc	%2 (%3)\n"
"	ld	%1, %2 (%3)\n"
"	; peephole 93a incremented in %2 (%3) instead of going through %1.\n"
"} if notSame(%3 'sp')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	ld	a,%2\n"
"	add	a,%1\n"
"} by {\n"
"	ld	%1, a\n"
"	; peephole 94 removed load by exploiting commutativity of addition.\n"
"	add	a,%2\n"
"} if notSame(%2 '(bc)' '(de)'), canAssign('b' %2)\n"
"\n"
"replace restart {\n"
"	ld	c,l\n"
"	ld	b,h\n"
"	ld	hl,#%1\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 95 removed loads by exploiting commutativity of addition.\n"
"	ld	bc,#%1\n"
"	add	hl,bc\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	add	hl,%2\n"
"	ld	bc,#%4\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 96 removed loads by exploiting commutativity of addition.\n"
"	ld	hl,#%1 + %4\n"
"	add	hl,%2\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	ld	c,e\n"
"	ld	b,d\n"
"	ld	hl,#%1\n"
"	add	hl,bc\n"
"} by {\n"
"	; peephole 97 removed loads by exploiting commutativity of addition.\n"
"	ld	hl,#%1\n"
"	add	hl,de\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 98 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	and	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 99 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,#0x00\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	%3,a\n"
"	; peephole 100 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	a\n"
"	jp	NZ,%1\n"
"	ld	%2,#0x00\n"
"} by {\n"
"	inc	a\n"
"	jp	NZ,%1\n"
"	ld	%2,a\n"
"	; peephole 101 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,#0x00\n"
"} by {\n"
"	dec	a\n"
"	jp	NZ,%1\n"
"	ld	%2,a\n"
"	; peephole 102 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 103 shortened or using a (which has just been tested to be #0x00).\n"
"} if canAssign('b' %3)\n"
"\n"
"replace restart {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	ld	a,%3\n"
"	or	a,a\n"
"} by {\n"
"	sub	a,%1\n"
"	jp	NZ,%2\n"
"	or	a,%3\n"
"	; peephole 104 shortened or using a (which has just been tested to be #0x00).\n"
"} if canAssign('b' %3)\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,#0x00\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	push	%3\n"
"	ld	%4,a\n"
"	; peephole 105 replaced constant #0x00 by a (which has just been tested to be #0x00).\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	(hl),#0x00\n"
"	inc	hl\n"
"	ld	(hl),#0x00\n"
"} by {\n"
"	xor	a, a\n"
"	; peephole 106 cached zero in a.\n"
"	ld	(hl), a\n"
"	inc	hl\n"
"	ld	(hl), a\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	add	hl,%2\n"
"	inc	hl\n"
"} by {\n"
"	ld	hl,#%1+1\n"
"	add	hl,%2\n"
"	; peephole 107 moved increment of hl to constant.\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"	ld	%1,#%2\n"
"	add	hl,%1\n"
"} by {\n"
"	ld	%1,#%2+1\n"
"	add	hl,%1\n"
"	; peephole 108 moved increment of hl to constant.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	dec	hl\n"
"	ld	%1,#%2\n"
"	add	hl,%1\n"
"} by {\n"
"	ld	%1,#%2-1\n"
"	add	hl,%1\n"
"	; peephole 109 moved decrement of hl to constant.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"	inc	hl\n"
"	inc	hl\n"
"	ld	hl, (hl)\n"
"} by {\n"
"	; peephole 109a moved increment of hl to offset.\n"
"	ld	hl, 3 (hl)\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"	inc	hl\n"
"	ld	hl, (hl)\n"
"} by {\n"
"	; peephole 109b moved increment of hl to offset.\n"
"	ld	hl, 2 (hl)\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	hl\n"
"	ld	hl, (hl)\n"
"} by {\n"
"	; peephole 109c moved increment of hl to offset.\n"
"	ld	hl, 1 (hl)\n"
"}\n"
"\n"
"replace restart {\n"
"	inc	iy\n"
"	ld	%1, %2 (iy)\n"
"} by {\n"
"	ld	%1, %2+1 (iy)\n"
"	; peephole 110 moved increment of iy to offset.\n"
"} if notUsed('iy')\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	iy\n"
"	pop	hl\n"
"	inc	iy\n"
"} by {\n"
"	inc	hl\n"
"	push	hl\n"
"	pop	iy\n"
"	pop	hl\n"
"	; peephole 111 incremented in hl instead of iy.\n"
"}\n"
"\n"
"replace restart {\n"
"	push	hl\n"
"	pop	iy\n"
"	inc	iy\n"
"} by {\n"
"	inc	hl\n"
"	push	hl\n"
"	pop	iy\n"
"	; peephole 111a incremented in hl instead of iy.\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	hl,%1\n"
"	add	hl,%2\n"
"	push	hl\n"
"	pop	iy\n"
"} by {\n"
"	ld	iy,%1\n"
"	add	iy,%2\n"
"	; peephole 111b added in iy instead of hl.\n"
"} if notUsed('hl'), notSame(%2 'hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	sp,%1\n"
"} by {\n"
"	; peephole 112 removed redundant pop af.\n"
"	ld	sp,%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	inc	sp\n"
"	ld	sp,%1\n"
"} by {\n"
"	; peephole 113 removed redundant inc sp.\n"
"	ld	sp,%1\n"
"} if notUsed('a')\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	ret\n"
"} by {\n"
"	jp	%1\n"
"	; peephole 114 replaced call at end of function by jump (tail call optimization).\n"
"}\n"
"\n"
"replace restart {\n"
"	call	%1\n"
"	pop	ix\n"
"	ret\n"
"} by {\n"
"	pop	ix\n"
"	jp	%1\n"
"	; peephole115 replaced call at end of function by jump moving call beyond pop ix (tail call optimization).\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	%3,%4\n"
"	ld	%1,#%2\n"
"} by {\n"
"	ld	%1,#%2\n"
"	ld	%3,%4\n"
"	; peephole 116 removed load of #%2 into %1 since it's still there.\n"
"} if notVolatile(%1), operandsNotRelated(%3 %1)\n"
"\n"
"replace restart {\n"
"	ld	hl,#%1\n"
"	ld	de,#%1\n"
"} by {\n"
"	; peephole 117 used #%1 from hl for load into de.\n"
"	ld	hl,#%1\n"
"	ld	e,l\n"
"	ld	d,h\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1 (ix),l\n"
"	ld	%2 (ix),h\n"
"	ld	%3,%1 (ix)\n"
"	ld	%4,%2 (ix)\n"
"} by {	\n"
"	ld	%1 (ix),l\n"
"	ld	%2 (ix),h\n"
"	; peephole 118 used hl instead of %2 (ix), %1 (ix) to load %4%3.\n"
"	ld	%3,l\n"
"	ld	%4,h\n"
"} if operandsNotRelated('h' %3)\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	; peephole 119 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%5, a\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, %2 (%3)\n"
"	adc	a, #%4\n"
"	ld	%5, a\n"
"	; peephole 120 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1), notSame(%5 %1), notSame(%5 '(hl)' '(de)' '(bc)'), notSame(%5 %6), notSame(%6 '(hl)' '(de)' '(bc)'), notSame(%5 'a'), notSame(%6 'a')\n"
"\n"
"replace restart {\n"
"	ld	%1, a\n"
"	ld	a, #%2\n"
"	adc	a, #%3\n"
"	ld	%5, a\n"
"	ld	%6, %1\n"
"} by {\n"
"	ld	%6, a\n"
"	ld	a, #%2\n"
"	adc	a, #%3\n"
"	ld	%5, a\n"
"	; peephole 121 loaded %6 from a directly instead of going through %1.\n"
"} if notUsed(%1), notSame(%5 %1), notSame(%5 %6 '(hl)' '(de)' '(bc)'), notSame(%6 'a')\n"
"\n"
"replace restart {\n"
"	ld	hl, #%1\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"	ld	hl, #%3\n"
"	add	hl, de\n"
"} by {\n"
"	ld	hl, #%1+%3\n"
"	add	hl, %2\n"
"	; peephole 122 removed addition and loads exploiting commutativity of addition.\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ld	%1,l\n"
"	ld	%2,h\n"
"	ex	de,hl\n"
"	ld	(hl),%1\n"
"	inc	hl\n"
"	ld	(hl),%2\n"
"} by {\n"
"	ld	%1,l\n"
"	ex	de,hl\n"
"	; peephole 122a used de instead of going through %1%2.\n"
"	ld	(hl),e\n"
"	inc	hl\n"
"	ld	(hl),d\n"
"} if notUsed(%2), notSame(%1 'l' 'h' 'e' 'd'), notSame(%2 'l' 'h' 'e' 'd')\n"
"	\n"
"replace restart {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	ld	hl, #0x0001\n"
"	add	hl, de\n"
"} by {\n"
"	ld	e, l\n"
"	ld	d, h\n"
"	inc	hl\n"
"	; peephole 123 replaced addition by increment.\n"
"}\n"
"\n"
"replace restart {	\n"
"	ld      sp,hl\n"
"	ld      hl,#0x0002\n"
"	add     hl,sp\n"
"} by {\n"
"	ld	sp, hl\n"
"	inc	hl\n"
"	inc	hl\n"
"	; peephole 124 replaced addition by increment.\n"
"}\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	ld	hl, #%1\n"
"	add	hl, de\n"
"} by {\n"
"	; peephole 125 removed ex exploiting commutativity of addition.\n"
"	ld	de, #%1\n"
"	add	hl, de\n"
"} if notUsed('de')\n"
"\n"
"replace restart {\n"
"	ex	de, hl\n"
"	push	bc\n"
"	ex	de, hl\n"
"} by {\n"
"	push	bc\n"
"	; peephole 126 canceled subsequent ex de, hl.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	hl, #%1\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"	inc	de\n"
"} by {\n"
"	ld	hl, #%1+1\n"
"	; peephole 127 moved increment to constant.\n"
"	add	hl, %2\n"
"	ex	de, hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	a,#0x01\n"
"	jp	%1\n"
"%2:\n"
"	xor	a,a\n"
"%1:\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	xor	a,a\n"
"	jp	%1\n"
"%2:\n"
"	ld	a,#0x01\n"
"%1:\n"
"	; peephole 128 removed negation.\n"
"} if labelRefCount(%1 1)\n"
"\n"
"replace restart {\n"
"	and	a,#0x01\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	and	a,#0x01\n"
"	xor	a,#0x01\n"
"	; peephole 129 used xor for negation.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,a\n"
"	sub	a,#%1\n"
"} by {\n"
"	; peephole 130 removed redundant or.\n"
"	sub	a,#%1\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a,#0x00\n"
"	rla\n"
"	sub	a,#0x01\n"
"	ld	a,#0x00\n"
"	rla\n"
"} by {\n"
"	ld	a,#0x00\n"
"	ccf\n"
"	; peephole 131 moved negation from bit 0 to carry flag.\n"
"	rla\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	a, #<(%1)\n"
"	add	a, l\n"
"	ld	l, a\n"
"	ld	a, #>(%1)\n"
"	adc	a, h\n"
"	ld	h, a\n"
"	push	bc\n"
"} by {\n"
"	push	bc\n"
"	ld	bc, #%1\n"
"	add	hl, bc\n"
"	; peephole 132 used 16 bit addition by moving push bc\n"
"	ld	a, h\n"
"} if notUsed('bc')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	push	hl\n"
"} by {\n"
"	; peephole 133 used ex to move hl onto the stack.\n"
"	ld	0(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	hl, #%1\n"
"	push	hl\n"
"} by {\n"
"	ld	hl, #%1\n"
"	; peephole 134 used 0(sp) to move hl onto the stack.\n"
"	ld	0(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	inc	sp\n"
"	ld	hl,#%1\n"
"	push	hl\n"
"} by {\n"
"	inc	sp\n"
"	ld	hl,#%1\n"
"	; peephole 135 used 0(sp) to move #%1 onto the stack.\n"
"	ld	0(sp),hl\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	a,#%1\n"
"	push	af\n"
"	inc	sp\n"
"} by {\n"
"	ld	h,#%1\n"
"	ld	0(sp),hl\n"
"	; peephole 136 used 0(sp) to move #%1 onto the stack.\n"
"	inc	sp\n"
"} if notUsed('a'), notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	%1,#%2\n"
"	ld	%3 (%1),a\n"
"%4:\n"
"	ld	%1,%5\n"
"} by {\n"
"	ld	(#%2 + %3),a\n"
"	; peephole 137 directly used #%2 instead of going through %1 using indirect addressing.\n"
"%4:\n"
"	ld	%1,%5\n"
"}\n"
"\n"
"replace restart {\n"
"	pop	af\n"
"	ld	%1,#%2\n"
"	ld	%3 (%1),%4\n"
"	ld	%1,#%5\n"
"} by {\n"
"	ld	a,%4\n"
"	ld	(#%2 + %3),a\n"
"	; peephole 138 used #%2 directly instead of going through %1 using indirect addressing.\n"
"	pop	af\n"
"	ld	%1,#%5\n"
"} if notSame(%3 'a')\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	bit	%2,%1\n"
"} by {\n"
"	bit	%2,a\n"
"	; peephole 139 tested bit %2 of a directly instead of going through %1.\n"
"} if notUsed(%1)\n"
"\n"
"replace restart {\n"
"	sbc	a,%1\n"
"	bit	7,a\n"
"	jp	Z,%2\n"
"} by {\n"
"	sbc	a,%1\n"
"	jp	P,%2\n"
"	; peephole 140 used sign flag instead of testing bit 7.\n"
"}\n"
"\n"
"replace restart {\n"
"	sbc	a,%1\n"
"	bit	7,a\n"
"	jp	NZ,%2\n"
"} by {\n"
"	sbc	a,%1\n"
"	jp	M,%2\n"
"	; peephole 141 used sign flag instead of testing bit 7.\n"
"}\n"
"\n"
"replace restart {\n"
"	ld	%1,a\n"
"	or	a,a\n"
"	jp	%3,%4\n"
"	ld	a,%1\n"
"} by {\n"
"	ld	%1,a\n"
"	or	a,a\n"
"	jp	%3,%4\n"
"	; peephole 142 used value still in a instead of reloading from %1.\n"
"}\n"
"\n"
"replace {\n"
"	jp	%5\n"
"	ret\n"
"} by {\n"
"	jp	%5\n"
"	; peephole 143 removed unused ret.\n"
"}\n"
"\n"
"replace {\n"
"	jp	%5\n"
"	ld	sp,ix\n"
"	pop	ix\n"
"	ret\n"
"} by {\n"
"	jp	%5\n"
"	; peephole 144 removed unused ret.\n"
"}\n"
"\n"
"replace restart {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	xor	a,a\n"
"	jp	%3\n"
"} by {\n"
"	or	a,%1\n"
"	jp	NZ,%2\n"
"	; peephole 145 removed redundant zeroing of a (which has just been tested to be #0x00).\n"
"	jp	%3\n"
"}\n"
"\n"
"barrier\n"
"\n"
"replace restart {\n"
"	ld	d,h\n"
"	ld	e,l\n"
"} by {\n"
"	; peephole 146 used ex to load hl into de.\n"
"	ex	de,hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	e,l\n"
"	ld	d,h\n"
"} by {\n"
"	; peephole 147 used ex to load hl into de.\n"
"	ex	de,hl\n"
"} if notUsed('hl')\n"
"\n"
"replace restart {\n"
"	ld	l,e\n"
"	ld	h,d\n"
"} by {\n"
"	; peephole 148 used ex to load de into hl.\n"
"	ex	de,hl\n"
"} if notUsed('de')\n"
"\n"
"barrier\n"
"\n"
"replace restart {\n"
"%1:\n"
"} by {\n"
"	; peephole 149 removed unused label %1.\n"
"} if labelRefCount(%1 0)\n"
"\n"
"barrier\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"%2:\n"
"} by {\n"
"	; peephole 150-3' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"%2:\n"
"} by {\n"
"	; peephole 150-4' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"%2:\n"
"} by {\n"
"	; peephole 150-5' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"%2:\n"
"} by {\n"
"	; peephole 150-6' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7 removed addition using short jumps in jump-table.\n"
"	pop	de\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"replace {\n"
"	add	hl,de\n"
"	jp	(hl)\n"
"%1:\n"
"	jp	%5\n"
"	jp	%6\n"
"	jp	%7\n"
"	jp	%8\n"
"	jp	%9\n"
"	jp	%10\n"
"	jp	%11\n"
"%2:\n"
"} by {\n"
"	; peephole 150-7' removed addition using short jumps in jump-table.\n"
"	jp	(hl)\n"
"%1:\n"
"	jr	%5\n"
"	jr	%6\n"
"	jr	%7\n"
"	jr	%8\n"
"	jr	%9\n"
"	jr	%10\n"
"	jr	%11\n"
"%2:\n"
"} if labelJTInRange\n"
"\n"
"barrier\n"
"\n"
"\n"
"replace restart {\n"
"	jp	%5\n"
"} by {\n"
"	ret\n"
"	; peephole 151 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace restart {\n"
"	jp	%1,%5\n"
"} by {\n"
"	ret	%1\n"
"	; peephole 152 replaced jump by return.\n"
"} if labelIsReturnOnly(), labelRefCountChange(%5 -1)\n"
"\n"
"replace {\n"
"	jp	%5\n"
"} by {\n"
"	jr	%5\n"
"	; peephole 153 changed absolute to relative unconditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	Z,%5\n"
"} by {\n"
"	jr	Z,%5\n"
"	; peephole 154 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	NZ,%5\n"
"} by {\n"
"	jr	NZ,%5\n"
"	; peephole 155 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	C,%5\n"
"} by {\n"
"	jr	C,%5\n"
"	; peephole 156 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
"replace {\n"
"	jp	NC,%5\n"
"} by {\n"
"	jr	NC,%5\n"
"	; peephole 157 changed absolute to relative conditional jump.\n"
"} if labelInRange()\n"
"\n"
};

Z80_OPTS z80_opts;

static OPTION _z80_options[] = {
  {0, OPTION_CALLEE_SAVES_BC, &z80_opts.calleeSavesBC, "Force a called function to always save BC"},
  {0, OPTION_PORTMODE,        NULL, "Determine PORT I/O mode (z80/z180)"},
  {0, OPTION_ASM,             NULL, "Define assembler name (rgbds/asxxxx/isas/z80asm)"},
  {0, OPTION_CODE_SEG,        &options.code_seg, "<name> use this name for the code segment", CLAT_STRING},
  {0, OPTION_CONST_SEG,       &options.const_seg, "<name> use this name for the const segment", CLAT_STRING},
  {0, OPTION_NO_STD_CRT0,     &options.no_std_crt0, "For the z80/gbz80 do not link default crt0.rel"},
  {0, OPTION_RESERVE_IY,      &z80_opts.reserveIY, "Do not use IY (incompatible with --fomit-frame-pointer)"},
  {0, OPTION_OLDRALLOC,       &options.oldralloc, "Use old register allocator"},
  {0, OPTION_FRAMEPOINTER,    &z80_opts.noOmitFramePtr, "Do not omit frame pointer"},
  {0, NULL}
};

static OPTION _gbz80_options[] = {
  {0, OPTION_BO,              NULL, "<num> use code bank <num>"},
  {0, OPTION_BA,              NULL, "<num> use data bank <num>"},
  {0, OPTION_CALLEE_SAVES_BC, &z80_opts.calleeSavesBC, "Force a called function to always save BC"},
  {0, OPTION_CODE_SEG,        &options.code_seg, "<name> use this name for the code segment", CLAT_STRING},
  {0, OPTION_CONST_SEG,       &options.const_seg, "<name> use this name for the const segment", CLAT_STRING},
  {0, OPTION_NO_STD_CRT0,     &options.no_std_crt0, "For the z80/gbz80 do not link default crt0.rel"},
  {0, NULL}
};

typedef enum
{
  /* Must be first */
  ASM_TYPE_ASXXXX,
  ASM_TYPE_RGBDS,
  ASM_TYPE_ISAS,
  ASM_TYPE_Z80ASM
}
ASM_TYPE;

static struct
{
  ASM_TYPE asmType;
  /* determine if we can register a parameter */
  int regParams;
}
_G;

static char *_keywords[] = {
  "sfr",
  "nonbanked",
  "banked",
  "at",                         //.p.t.20030714 adding support for 'sfr at ADDR' construct
  "_naked",                     //.p.t.20030714 adding support for '_naked' functions
  "critical",
  "interrupt",
  NULL
};

extern PORT gbz80_port;
extern PORT z80_port;
extern PORT r2k_port;

#include "mappings.i"

static builtins _z80_builtins[] = {
  {"__builtin_memcpy", "vg*", 3, {"vg*", "Cvg*", "ui"}},
  {"__builtin_strcpy", "cg*", 2, {"cg*", "Ccg*"}},
  {"__builtin_strncpy", "cg*", 3, {"cg*", "Ccg*", "ui"}},
  {"__builtin_strchr", "cg*", 2, {"Ccg*", "i"}},
  {"__builtin_memset", "vg*", 3, {"vg*", "i", "ui"}},
  {NULL, NULL, 0, {NULL}}
};

static void
_z80_init (void)
{
  z80_opts.sub = SUB_Z80;
  asm_addTree (&_asxxxx_z80);
}

static void
_z180_init (void)
{
  z80_opts.sub = SUB_Z180;
  asm_addTree (&_asxxxx_z80);
}

static void
_r2k_init (void)
{
  z80_opts.sub = SUB_R2K;
  asm_addTree (&_asxxxx_r2k);
}

static void
_r3ka_init (void)
{
  z80_opts.sub = SUB_R3KA;
  asm_addTree (&_asxxxx_r2k);
}

static void
_gbz80_init (void)
{
  z80_opts.sub = SUB_GBZ80;
}

static void
_reset_regparm (void)
{
  _G.regParams = 0;
}

static int
_reg_parm (sym_link * l, bool reentrant)
{
  if (options.noRegParams)
    {
      return FALSE;
    }
  else
    {
      if (!IS_REGISTER (l) || getSize (l) > 2)
        {
          return FALSE;
        }
      if (_G.regParams == 2)
        {
          return FALSE;
        }
      else
        {
          _G.regParams++;
          return TRUE;
        }
    }
}

enum
{
  P_BANK = 1,
  P_PORTMODE,
  P_CODESEG,
  P_CONSTSEG,
};

static int
do_pragma (int id, const char *name, const char *cp)
{
  struct pragma_token_s token;
  int err = 0;
  int processed = 1;

  init_pragma_token (&token);

  switch (id)
    {
    case P_BANK:
      {
        struct dbuf_s buffer;

        dbuf_init (&buffer, 128);

        cp = get_pragma_token (cp, &token);

        switch (token.type)
          {
          case TOKEN_EOL:
            err = 1;
            break;

          case TOKEN_INT:
            switch (_G.asmType)
              {
              case ASM_TYPE_ASXXXX:
                dbuf_printf (&buffer, "CODE_%d", token.val.int_val);
                break;

              case ASM_TYPE_RGBDS:
                dbuf_printf (&buffer, "CODE,BANK[%d]", token.val.int_val);
                break;

              case ASM_TYPE_ISAS:
                /* PENDING: what to use for ISAS? */
                dbuf_printf (&buffer, "CODE,BANK(%d)", token.val.int_val);
                break;

              default:
                wassert (0);
              }
            break;

          default:
            {
              const char *str = get_pragma_string (&token);

              dbuf_append_str (&buffer, (0 == strcmp ("BASE", str)) ? "HOME" : str);
            }
            break;
          }

        cp = get_pragma_token (cp, &token);
        if (TOKEN_EOL != token.type)
          {
            err = 1;
            break;
          }

        dbuf_c_str (&buffer);
        /* ugly, see comment in src/port.h (borutr) */
        gbz80_port.mem.code_name = dbuf_detach (&buffer);
        code->sname = gbz80_port.mem.code_name;
        options.code_seg = (char *) gbz80_port.mem.code_name;
      }
      break;

    case P_PORTMODE:
      {                         /*.p.t.20030716 - adding pragma to manipulate z80 i/o port addressing modes */
        const char *str;

        cp = get_pragma_token (cp, &token);

        if (TOKEN_EOL == token.type)
          {
            err = 1;
            break;
          }

        str = get_pragma_string (&token);

        cp = get_pragma_token (cp, &token);
        if (TOKEN_EOL != token.type)
          {
            err = 1;
            break;
          }

        if (!strcmp (str, "z80"))
          {
            z80_opts.port_mode = 80;
          }
        else if (!strcmp (str, "z180"))
          {
            z80_opts.port_mode = 180;
          }
        else if (!strcmp (str, "save"))
          {
            z80_opts.port_back = z80_opts.port_mode;
          }
        else if (!strcmp (str, "restore"))
          {
            z80_opts.port_mode = z80_opts.port_back;
          }
        else
          err = 1;
      }
      break;

    case P_CODESEG:
    case P_CONSTSEG:
      {
        char *segname;

        cp = get_pragma_token (cp, &token);
        if (token.type == TOKEN_EOL)
          {
            err = 1;
            break;
          }

        segname = Safe_strdup (get_pragma_string (&token));

        cp = get_pragma_token (cp, &token);
        if (token.type != TOKEN_EOL)
          {
            Safe_free (segname);
            err = 1;
            break;
          }

        if (id == P_CODESEG)
          {
            if (options.code_seg)
              Safe_free (options.code_seg);
            options.code_seg = segname;
          }
        else
          {
            if (options.const_seg)
              Safe_free (options.const_seg);
            options.const_seg = segname;
          }
      }
      break;

    default:
      processed = 0;
      break;
    }

  get_pragma_token (cp, &token);

  if (1 == err)
    werror (W_BAD_PRAGMA_ARGUMENTS, name);

  free_pragma_token (&token);
  return processed;
}

static struct pragma_s pragma_tbl[] = {
  {"bank", P_BANK, 0, do_pragma},
  {"portmode", P_PORTMODE, 0, do_pragma},
  {"codeseg", P_CODESEG, 0, do_pragma},
  {"constseg", P_CONSTSEG, 0, do_pragma},
  {NULL, 0, 0, NULL},
};

static int
_process_pragma (const char *s)
{
  return process_pragma_tbl (pragma_tbl, s);
}

static const char *_gbz80_rgbasmCmd[] = {
  "rgbasm", "-o$1.rel", "$1.asm", NULL
};

static const char *_gbz80_rgblinkCmd[] = {
  "xlink", "-tg", "-n$1.sym", "-m$1.map", "-zFF", "$1.lnk", NULL
};

static void
_gbz80_rgblink (void)
{
  FILE *lnkfile;
  struct dbuf_s lnkFileName;
  char *buffer;

  dbuf_init (&lnkFileName, PATH_MAX);

  /* first we need to create the <filename>.lnk file */
  dbuf_append_str (&lnkFileName, dstFileName);
  dbuf_append_str (&lnkFileName, ".lk");
  if (!(lnkfile = fopen (dbuf_c_str (&lnkFileName), "w")))
    {
      werror (E_FILE_OPEN_ERR, dbuf_c_str (&lnkFileName));
      dbuf_destroy (&lnkFileName);
      exit (1);
    }
  dbuf_destroy (&lnkFileName);

  fprintf (lnkfile, "[Objects]\n");

  fprintf (lnkfile, "%s.rel\n", dstFileName);

  fputStrSet (lnkfile, relFilesSet);

  fprintf (lnkfile, "\n[Libraries]\n");
  /* additional libraries if any */
  fputStrSet (lnkfile, libFilesSet);

  fprintf (lnkfile, "\n[Output]\n" "%s.gb", dstFileName);

  fclose (lnkfile);

  buffer = buildCmdLine (port->linker.cmd, dstFileName, NULL, NULL, NULL);
  /* call the linker */
  if (sdcc_system (buffer))
    {
      Safe_free (buffer);
      perror ("Cannot exec linker");
      exit (1);
    }
  Safe_free (buffer);
}

static bool
_parseOptions (int *pargc, char **argv, int *i)
{
  if (argv[*i][0] == '-')
    {
      if (IS_GB)
        {
          if (!strncmp (argv[*i], OPTION_BO, sizeof (OPTION_BO) - 1))
            {
              /* ROM bank */
              int bank = getIntArg (OPTION_BO, argv, i, *pargc);
              struct dbuf_s buffer;

              dbuf_init (&buffer, 16);
              dbuf_printf (&buffer, "CODE_%u", bank);
              dbuf_c_str (&buffer);
              /* ugly, see comment in src/port.h (borutr) */
              gbz80_port.mem.code_name = dbuf_detach (&buffer);
              options.code_seg = (char *) gbz80_port.mem.code_name;
              return TRUE;
            }
          else if (!strncmp (argv[*i], OPTION_BA, sizeof (OPTION_BA) - 1))
            {
              /* RAM bank */
              int bank = getIntArg (OPTION_BA, argv, i, *pargc);
              struct dbuf_s buffer;

              dbuf_init (&buffer, 16);
              dbuf_printf (&buffer, "DATA_%u", bank);
              dbuf_c_str (&buffer);
              /* ugly, see comment in src/port.h (borutr) */
              gbz80_port.mem.data_name = dbuf_detach (&buffer);
              return TRUE;
            }
        }
      else if (!strncmp (argv[*i], OPTION_ASM, sizeof (OPTION_ASM) - 1))
        {
          char *asmblr = getStringArg (OPTION_ASM, argv, i, *pargc);

          if (!strcmp (asmblr, "rgbds"))
            {
              asm_addTree (&_rgbds_gb);
              gbz80_port.assembler.cmd = _gbz80_rgbasmCmd;
              gbz80_port.linker.cmd = _gbz80_rgblinkCmd;
              gbz80_port.linker.do_link = _gbz80_rgblink;
              _G.asmType = ASM_TYPE_RGBDS;
              return TRUE;
            }
          else if (!strcmp (asmblr, "asxxxx"))
            {
              _G.asmType = ASM_TYPE_ASXXXX;
              return TRUE;
            }
          else if (!strcmp (asmblr, "isas"))
            {
              asm_addTree (&_isas_gb);
              /* Munge the function prefix */
              gbz80_port.fun_prefix = "";
              _G.asmType = ASM_TYPE_ISAS;
              return TRUE;
            }
          else if (!strcmp (asmblr, "z80asm"))
            {
              port->assembler.externGlobal = TRUE;
              asm_addTree (&_z80asm_z80);
              _G.asmType = ASM_TYPE_ISAS;
              return TRUE;
            }
        }
      else if (!strncmp (argv[*i], OPTION_PORTMODE, sizeof (OPTION_PORTMODE) - 1))
        {
          char *portmode = getStringArg (OPTION_ASM, argv, i, *pargc);

          if (!strcmp (portmode, "z80"))
            {
              z80_opts.port_mode = 80;
              return TRUE;
            }
          else if (!strcmp (portmode, "z180"))
            {
              z80_opts.port_mode = 180;
              return TRUE;
            }
        }
    }
  return FALSE;
}

static void
_setValues (void)
{
  const char *s;
  struct dbuf_s dbuf;

  if (options.nostdlib == FALSE)
    {
      const char *s;
      char *path;
      struct dbuf_s dbuf;

      dbuf_init (&dbuf, PATH_MAX);

      for (s = setFirstItem (libDirsSet); s != NULL; s = setNextItem (libDirsSet))
        {
          path = buildCmdLine2 ("-k\"%s" DIR_SEPARATOR_STRING "{port}\" ", s);
          dbuf_append_str (&dbuf, path);
          Safe_free (path);
        }
      path = buildCmdLine2 ("-l\"{port}.lib\"", s);
      dbuf_append_str (&dbuf, path);
      Safe_free (path);

      setMainValue ("z80libspec", dbuf_c_str (&dbuf));
      dbuf_destroy (&dbuf);

      for (s = setFirstItem (libDirsSet); s != NULL; s = setNextItem (libDirsSet))
        {
          struct stat stat_buf;

          path = buildCmdLine2 ("%s" DIR_SEPARATOR_STRING "{port}" DIR_SEPARATOR_STRING "crt0{objext}", s);
          if (stat (path, &stat_buf) == 0)
            {
              Safe_free (path);
              break;
            }
          else
            Safe_free (path);
        }

      if (s == NULL)
        setMainValue ("z80crt0", "\"crt0{objext}\"");
      else
        {
          struct dbuf_s dbuf;

          dbuf_init (&dbuf, 128);
          dbuf_printf (&dbuf, "\"%s\"", path);
          setMainValue ("z80crt0", dbuf_c_str (&dbuf));
          dbuf_destroy (&dbuf);
        }
    }
  else
    {
      setMainValue ("z80libspec", "");
      setMainValue ("z80crt0", "");
    }

  setMainValue ("z80extralibfiles", (s = joinStrSet (libFilesSet)));
  Safe_free ((void *) s);
  setMainValue ("z80extralibpaths", (s = joinStrSet (libPathsSet)));
  Safe_free ((void *) s);

  if (IS_GB)
    {
      setMainValue ("z80outputtypeflag", "-Z");
      setMainValue ("z80outext", ".gb");
    }
  else
    {
      setMainValue ("z80outputtypeflag", "-i");
      setMainValue ("z80outext", ".ihx");
    }

  setMainValue ("stdobjdstfilename", "{dstfilename}{objext}");
  setMainValue ("stdlinkdstfilename", "{dstfilename}{z80outext}");

  setMainValue ("z80extraobj", (s = joinStrSet (relFilesSet)));
  Safe_free ((void *) s);

  dbuf_init (&dbuf, 128);
  dbuf_printf (&dbuf, "-b_CODE=0x%04X -b_DATA=0x%04X", options.code_loc, options.data_loc);
  setMainValue ("z80bases", dbuf_c_str (&dbuf));
  dbuf_destroy (&dbuf);

  /* For the old register allocator (with the new one we decide to omit the frame pointer for each function individually) */
  if (!IS_GB && options.omitFramePtr)
    port->stack.call_overhead = 2;
}

static void
_finaliseOptions (void)
{
  port->mem.default_local_map = data;
  port->mem.default_globl_map = data;
  if (_G.asmType == ASM_TYPE_ASXXXX && IS_GB)
    asm_addTree (&_asxxxx_gb);

  if (IY_RESERVED)
    port->num_regs -= 2;

  _setValues ();
}

static void
_setDefaultOptions (void)
{
  options.nopeep = 0;
  options.stackAuto = 1;
  /* first the options part */
  options.intlong_rent = 1;
  options.float_rent = 1;
  options.noRegParams = 1;
  /* Default code and data locations. */
  options.code_loc = 0x200;

  options.data_loc = IS_GB ? 0xC000 : 0x8000;
  options.out_fmt = 'i';        /* Default output format is ihx */
}

/* Mangling format:
    _fun_policy_params
    where:
      policy is the function policy
      params is the parameter format

   policy format:
    rsp
    where:
      r is 'r' for reentrant, 's' for static functions
      s is 'c' for callee saves, 'r' for caller saves
      f is 'f' for profiling on, 'x' for profiling off
    examples:
      rr - reentrant, caller saves
   params format:
    A combination of register short names and s to signify stack variables.
    examples:
      bds - first two args appear in BC and DE, the rest on the stack
      s - all arguments are on the stack.
*/
static const char *
_mangleSupportFunctionName (const char *original)
{
  struct dbuf_s dbuf;

  if (strstr (original, "longlong"))
    return (original);

  dbuf_init (&dbuf, 128);
  dbuf_printf (&dbuf, "%s_rr%s_%s", original, options.profile ? "f" : "x", options.noRegParams ? "s" : "bds"    /* MB: but the library only has hds variants ??? */
    );

  return dbuf_detach_c_str (&dbuf);
}

static const char *
_getRegName (const struct reg_info *reg)
{
  if (reg)
    {
      return reg->name;
    }
  /*  assert (0); */
  return "err";
}

static bool
_hasNativeMulFor (iCode * ic, sym_link * left, sym_link * right)
{
  sym_link *test = NULL;
  int result_size = IS_SYMOP(IC_RESULT(ic)) ? getSize(OP_SYM_TYPE(IC_RESULT(ic))) : 4;

  if (ic->op != '*')
    {
      return FALSE;
    }

  if (IS_LITERAL (left))
    test = left;
  else if (IS_LITERAL (right))
    test = right;
  /* 8x8 unsigned multiplication code is shorter than
     call overhead for the multiplication routine. */
  else if (IS_CHAR (right) && IS_UNSIGNED (right) && IS_CHAR (left) && IS_UNSIGNED (left) && !IS_GB)
    {
      return TRUE;
    }
  /* Same for any multiplication with 8 bit result. */
  else if (result_size == 1 && !IS_GB)
    {
      return TRUE;
    }
  else
    {
      return FALSE;
    }

  if (getSize (test) <= 2)
    {
      return TRUE;
    }

  return FALSE;
}

/* Indicate which extended bit operations this port supports */
static bool
hasExtBitOp (int op, int size)
{
  if (op == GETHBIT)
    return TRUE;
  else
    return FALSE;
}

/* Indicate the expense of an access to an output storage class */
static int
oclsExpense (struct memmap *oclass)
{
  if (IN_FARSPACE (oclass))
    return 1;

  return 0;
}


//#define LINKCMD "sdld{port} -nf {dstfilename}"
/*
#define LINKCMD \
    "sdld{port} -n -c -- {z80bases} -m -j" \
    " {z80libspec}" \
    " {z80extralibfiles} {z80extralibpaths}" \
    " {z80outputtypeflag} \"{linkdstfilename}\"" \
    " {z80crt0}" \
    " \"{dstfilename}{objext}\"" \
    " {z80extraobj}"
*/

static const char *_z80LinkCmd[] = {
  "sdldz80", "-nf", "$1", NULL
};

static const char *_gbLinkCmd[] = {
  "sdldgb", "-nf", "$1", NULL
};

/* $3 is replaced by assembler.debug_opts resp. port->assembler.plain_opts */
static const char *_z80AsmCmd[] = {
  "sdasz80", "$l", "$3", "$2", "$1.asm", NULL
};

static const char *_gbAsmCmd[] = {
  "sdasgb", "$l", "$3", "$2", "$1.asm", NULL
};

static const char *_r2kAsmCmd[] = {
  "sdasrab", "$l", "$3", "$2", "$1.asm", NULL
};

static const char *const _crt[] = { "crt0.rel", NULL, };
static const char *const _libs_z80[] = { "z80", NULL, };
static const char *const _libs_z180[] = { "z180", NULL, };
static const char *const _libs_r2k[] = { "r2k", NULL, };
static const char *const _libs_r3ka[] = { "r3ka", NULL, };
static const char *const _libs_gb[] = { "gbz80", NULL, };

/* Globals */
PORT z80_port = {
  TARGET_ID_Z80,
  "z80",
  "Zilog Z80",                  /* Target name */
  NULL,                         /* Processor name */
  {
   glue,
   FALSE,
   NO_MODEL,
   NO_MODEL,
   NULL,                        /* model == target */
   },
  {                             /* Assembler */
   _z80AsmCmd,
   NULL,
   "-plosgffwy",                /* Options with debug */
   "-plosgffw",                 /* Options without debug */
   0,
   ".asm"},
  {                             /* Linker */
   _z80LinkCmd,                 //NULL,
   NULL,                        //LINKCMD,
   NULL,
   ".rel",
   1,
   _crt,                        /* crt */
   _libs_z80,                   /* libs */
   },
  {                             /* Peephole optimizer */
   _z80_defaultRules,
   z80instructionSize,
   0,
   0,
   0,
   z80notUsed,
   z80canAssign,
   z80notUsedFrom,
   },
  {
   /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
   1, 2, 2, 4, 8, 2, 2, 2, 1, 4, 4},
  /* tags for generic pointers */
  {0x00, 0x40, 0x60, 0x80},     /* far, near, xstack, code */
  {
   "XSEG",
   "STACK",
   "CODE",
   "DATA",
   NULL,                        /* idata */
   NULL,                        /* pdata */
   NULL,                        /* xdata */
   NULL,                        /* bit */
   "RSEG (ABS)",
   "GSINIT",                    /* static initialization */
   NULL,                        /* overlay */
   "GSFINAL",
   "HOME",
   NULL,                        /* xidata */
   NULL,                        /* xinit */
   NULL,                        /* const_name */
   "CABS (ABS)",                /* cabs_name */
   "DABS (ABS)",                /* xabs_name */
   NULL,                        /* iabs_name */
   "INITIALIZED",               /* name of segment for initialized variables */
   "INITIALIZER",               /* name of segment for copies of initialized variables in code space */
   NULL,
   NULL,
   1,                           /* CODE  is read-only */
   1                            /* No fancy alignments supported. */
   },
  {NULL, NULL},
  {
   -1, 0, 0, 4, 0, 2},
  /* Z80 has no native mul/div commands */
  {
   0, -1},
  {
   z80_emitDebuggerSymbol},
  {
   255,                         /* maxCount */
   3,                           /* sizeofElement */
   /* The rest of these costs are bogus. They approximate */
   /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
   {4, 4, 4},                   /* sizeofMatchJump[] */
   {0, 0, 0},                   /* sizeofRangeCompare[] */
   0,                           /* sizeofSubtract */
   3,                           /* sizeofDispatch */
   },
  "_",
  _z80_init,
  _parseOptions,
  _z80_options,
  NULL,
  _finaliseOptions,
  _setDefaultOptions,
  z80_assignRegisters,
  _getRegName,
  _keywords,
  0,                            /* no assembler preamble */
  NULL,                         /* no genAssemblerEnd */
  0,                            /* no local IVT generation code */
  0,                            /* no genXINIT code */
  NULL,                         /* genInitStartup */
  _reset_regparm,
  _reg_parm,
  _process_pragma,
  _mangleSupportFunctionName,
  _hasNativeMulFor,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  TRUE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* Array initializer support. */
  0,                            /* no CSE cost estimation yet */
  _z80_builtins,                /* builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local statics allowed */
  9,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

PORT z180_port = {
  TARGET_ID_Z180,
  "z180",
  "Zilog Z180",                  /* Target name */
  NULL,                         /* Processor name */
  {
   glue,
   FALSE,
   NO_MODEL,
   NO_MODEL,
   NULL,                        /* model == target */
   },
  {                             /* Assembler */
   _z80AsmCmd,
   NULL,
   "-plosgffwy",                /* Options with debug */
   "-plosgffw",                 /* Options without debug */
   0,
   ".asm"},
  {                             /* Linker */
   _z80LinkCmd,                 //NULL,
   NULL,                        //LINKCMD,
   NULL,
   ".rel",
   1,
   _crt,                        /* crt */
   _libs_z180,                  /* libs */
   },
  {                             /* Peephole optimizer */
   _z80_defaultRules,
   z80instructionSize,
   0,
   0,
   0,
   z80notUsed,
   z80canAssign,
   z80notUsedFrom,
   },
  {
   /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
   1, 2, 2, 4, 8, 2, 2, 2, 1, 4, 4},
  /* tags for generic pointers */
  {0x00, 0x40, 0x60, 0x80},     /* far, near, xstack, code */
  {
   "XSEG",
   "STACK",
   "CODE",
   "DATA",
   NULL,                        /* idata */
   NULL,                        /* pdata */
   NULL,                        /* xdata */
   NULL,                        /* bit */
   "RSEG (ABS)",
   "GSINIT",
   NULL,                        /* overlay */
   "GSFINAL",
   "HOME",
   NULL,                        /* xidata */
   NULL,                        /* xinit */
   NULL,                        /* const_name */
   "CABS (ABS)",                /* cabs_name */
   "DABS (ABS)",                /* xabs_name */
   NULL,                        /* iabs_name */
   "INITIALIZED",               /* name of segment for initialized variables */
   "INITIALIZER",               /* name of segment for copies of initialized variables in code space */
   NULL,
   NULL,
   1,                           /* CODE  is read-only */
   1                            /* No fancy alignments supported. */
   },
  {NULL, NULL},
  {
   -1, 0, 0, 4, 0, 2},
  /* Z80 has no native mul/div commands */
  {
   0, -1},
  {
   z80_emitDebuggerSymbol},
  {
   255,                         /* maxCount */
   3,                           /* sizeofElement */
   /* The rest of these costs are bogus. They approximate */
   /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
   {4, 4, 4},                   /* sizeofMatchJump[] */
   {0, 0, 0},                   /* sizeofRangeCompare[] */
   0,                           /* sizeofSubtract */
   3,                           /* sizeofDispatch */
   },
  "_",
  _z180_init,
  _parseOptions,
  _z80_options,
  NULL,
  _finaliseOptions,
  _setDefaultOptions,
  z80_assignRegisters,
  _getRegName,
  _keywords,
  0,                            /* no assembler preamble */
  NULL,                         /* no genAssemblerEnd */
  0,                            /* no local IVT generation code */
  0,                            /* no genXINIT code */
  NULL,                         /* genInitStartup */
  _reset_regparm,
  _reg_parm,
  _process_pragma,
  _mangleSupportFunctionName,
  _hasNativeMulFor,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  TRUE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* Array initializer support. */
  0,                            /* no CSE cost estimation yet */
  _z80_builtins,                /* builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local statics allowed */
  9,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

PORT r2k_port = {
  TARGET_ID_R2K,
  "r2k",
  "Rabbit 2000",                  /* Target name */
  NULL,                         /* Processor name */
  {
   glue,
   FALSE,
   NO_MODEL,
   NO_MODEL,
   NULL,                        /* model == target */
   },
  {                             /* Assembler */
   _r2kAsmCmd,
   NULL,
   "-plosgffwy",                /* Options with debug */
   "-plosgffw",                 /* Options without debug */
   0,
   ".asm"},
  {                             /* Linker */
   _z80LinkCmd,                 //NULL,
   NULL,                        //LINKCMD,
   NULL,
   ".rel",
   1,
   _crt,                        /* crt */
   _libs_r2k,                   /* libs */
   },
  {                             /* Peephole optimizer */
   _r2k_defaultRules,
   z80instructionSize,
   0,
   0,
   0,
   z80notUsed,
   z80canAssign,
   z80notUsedFrom,
   },
  {
   /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
   1, 2, 2, 4, 8, 2, 2, 2, 1, 4, 4},
  /* tags for generic pointers */
  {0x00, 0x40, 0x60, 0x80},     /* far, near, xstack, code */
  {
   "XSEG",
   "STACK",
   "CODE",
   "DATA",
   NULL,                        /* idata */
   NULL,                        /* pdata */
   NULL,                        /* xdata */
   NULL,                        /* bit */
   "RSEG (ABS)",
   "GSINIT",
   NULL,                        /* overlay */
   "GSFINAL",
   "HOME",
   NULL,                        /* xidata */
   NULL,                        /* xinit */
   NULL,                        /* const_name */
   "CABS (ABS)",                /* cabs_name */
   "DABS (ABS)",                /* xabs_name */
   NULL,                        /* iabs_name */
   "INITIALIZED",               /* name of segment for initialized variables */
   "INITIALIZER",               /* name of segment for copies of initialized variables in code space */
   NULL,
   NULL,
   1,                           /* CODE  is read-only */
   1                            /* No fancy alignments supported. */
   },
  {NULL, NULL},
  {
   -1, 0, 0, 4, 0, 2},
  /* Z80 has no native mul/div commands */
  {
   0, -1},
  {
   z80_emitDebuggerSymbol},
  {
   255,                         /* maxCount */
   3,                           /* sizeofElement */
   /* The rest of these costs are bogus. They approximate */
   /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
   {4, 4, 4},                   /* sizeofMatchJump[] */
   {0, 0, 0},                   /* sizeofRangeCompare[] */
   0,                           /* sizeofSubtract */
   3,                           /* sizeofDispatch */
   },
  "_",
  _r2k_init,
  _parseOptions,
  _z80_options,
  NULL,
  _finaliseOptions,
  _setDefaultOptions,
  z80_assignRegisters,
  _getRegName,
  _keywords,
  0,                            /* no assembler preamble */
  NULL,                         /* no genAssemblerEnd */
  0,                            /* no local IVT generation code */
  0,                            /* no genXINIT code */
  NULL,                         /* genInitStartup */
  _reset_regparm,
  _reg_parm,
  _process_pragma,
  _mangleSupportFunctionName,
  _hasNativeMulFor,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  TRUE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* Array initializer support. */
  0,                            /* no CSE cost estimation yet */
  _z80_builtins,                /* builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local statics allowed */
  9,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

PORT r3ka_port = {
  TARGET_ID_R3KA,
  "r3ka",
  "Rabbit 3000A",               /* Target name */
  NULL,                         /* Processor name */
  {
   glue,
   FALSE,
   NO_MODEL,
   NO_MODEL,
   NULL,                        /* model == target */
   },
  {                             /* Assembler */
   _r2kAsmCmd,
   NULL,
   "-plosgffwy",                /* Options with debug */
   "-plosgffw",                 /* Options without debug */
   0,
   ".asm"},
  {                             /* Linker */
   _z80LinkCmd,                 //NULL,
   NULL,                        //LINKCMD,
   NULL,
   ".rel",
   1,
   _crt,                        /* crt */
   _libs_r3ka,                  /* libs */
   },
  {                             /* Peephole optimizer */
   _r2k_defaultRules,
   z80instructionSize,
   0,
   0,
   0,
   z80notUsed,
   z80canAssign,
   z80notUsedFrom,
   },
  {
   /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
   1, 2, 2, 4, 8, 2, 2, 2, 1, 4, 4},
  /* tags for generic pointers */
  {0x00, 0x40, 0x60, 0x80},     /* far, near, xstack, code */
  {
   "XSEG",
   "STACK",
   "CODE",
   "DATA",
   NULL,                        /* idata */
   NULL,                        /* pdata */
   NULL,                        /* xdata */
   NULL,                        /* bit */
   "RSEG (ABS)",
   "GSINIT",
   NULL,                        /* overlay */
   "GSFINAL",
   "HOME",
   NULL,                        /* xidata */
   NULL,                        /* xinit */
   NULL,                        /* const_name */
   "CABS (ABS)",                /* cabs_name */
   "DABS (ABS)",                /* xabs_name */
   NULL,                        /* iabs_name */
   "INITIALIZED",               /* name of segment for initialized variables */
   "INITIALIZER",               /* name of segment for copies of initialized variables in code space */
   NULL,
   NULL,
   1,                           /* CODE  is read-only */
   1                            /* No fancy alignments supported. */
   },
  {NULL, NULL},
  {
   -1, 0, 0, 4, 0, 2},
  /* Z80 has no native mul/div commands */
  {
   0, -1},
  {
   z80_emitDebuggerSymbol},
  {
   255,                         /* maxCount */
   3,                           /* sizeofElement */
   /* The rest of these costs are bogus. They approximate */
   /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
   {4, 4, 4},                   /* sizeofMatchJump[] */
   {0, 0, 0},                   /* sizeofRangeCompare[] */
   0,                           /* sizeofSubtract */
   3,                           /* sizeofDispatch */
   },
  "_",
  _r3ka_init,
  _parseOptions,
  _z80_options,
  NULL,
  _finaliseOptions,
  _setDefaultOptions,
  z80_assignRegisters,
  _getRegName,
  _keywords,
  0,                            /* no assembler preamble */
  NULL,                         /* no genAssemblerEnd */
  0,                            /* no local IVT generation code */
  0,                            /* no genXINIT code */
  NULL,                         /* genInitStartup */
  _reset_regparm,
  _reg_parm,
  _process_pragma,
  _mangleSupportFunctionName,
  _hasNativeMulFor,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  TRUE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* Array initializer support. */
  0,                            /* no CSE cost estimation yet */
  _z80_builtins,                /* builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local statics allowed */
  9,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

/* Globals */
PORT gbz80_port = {
  TARGET_ID_GBZ80,
  "gbz80",
  "Gameboy Z80-like",           /* Target name */
  NULL,
  {
   glue,
   FALSE,
   NO_MODEL,
   NO_MODEL,
   NULL,                        /* model == target */
   },
  {                             /* Assembler */
   _gbAsmCmd,
   NULL,
   "-plosgffwy",                /* Options with debug */
   "-plosgffw",                 /* Options without debug */
   0,
   ".asm",
   NULL                         /* no do_assemble function */
   },
  {                             /* Linker */
   _gbLinkCmd,                  //NULL,
   NULL,                        //LINKCMD,
   NULL,
   ".rel",
   1,
   _crt,                        /* crt */
   _libs_gb,                    /* libs */
   },
  {                             /* Peephole optimizer */
   _gbz80_defaultRules,
   0,
   0,
   0,
   0,
   0,
   0,
   },
  {
   /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
   1, 2, 2, 4, 8, 2, 2, 2, 1, 4, 4},
  /* tags for generic pointers */
  {0x00, 0x40, 0x60, 0x80},     /* far, near, xstack, code */
  {
   "XSEG",
   "STACK",
   "CODE",
   "DATA",
   NULL,                        /* idata */
   NULL,                        /* pdata */
   NULL,                        /* xdata */
   NULL,                        /* bit */
   "RSEG",
   "GSINIT",
   NULL,                        /* overlay */
   "GSFINAL",
   "HOME",
   NULL,                        /* xidata */
   NULL,                        /* xinit */
   NULL,                        /* const_name */
   "CABS (ABS)",                /* cabs_name */
   "DABS (ABS)",                /* xabs_name */
   NULL,                        /* iabs_name */
   NULL,                        /* name of segment for initialized variables */
   NULL,                        /* name of segment for copies of initialized variables in code space */
   NULL,
   NULL,
   1,                           /* CODE is read-only */
   1                            /* No fancy alignments supported. */
   },
  {NULL, NULL},
  {
   -1, 0, 0, 2, 0, 4},
  /* gbZ80 has no native mul/div commands */
  {
   0, -1},
  {
   z80_emitDebuggerSymbol},
  {
   255,                         /* maxCount */
   3,                           /* sizeofElement */
   /* The rest of these costs are bogus. They approximate */
   /* the behavior of src/SDCCicode.c 1.207 and earlier.  */
   {4, 4, 4},                   /* sizeofMatchJump[] */
   {0, 0, 0},                   /* sizeofRangeCompare[] */
   0,                           /* sizeofSubtract */
   3,                           /* sizeofDispatch */
   },
  "_",
  _gbz80_init,
  _parseOptions,
  _gbz80_options,
  NULL,
  _finaliseOptions,
  _setDefaultOptions,
  z80_assignRegisters,
  _getRegName,
  _keywords,
  0,                            /* no assembler preamble */
  NULL,                         /* no genAssemblerEnd */
  0,                            /* no local IVT generation code */
  0,                            /* no genXINIT code */
  NULL,                         /* genInitStartup */
  _reset_regparm,
  _reg_parm,
  _process_pragma,
  _mangleSupportFunctionName,
  _hasNativeMulFor,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  TRUE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* Array initializer support. */
  0,                            /* no CSE cost estimation yet */
  NULL,                         /* no builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local statics allowed */
  5,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

