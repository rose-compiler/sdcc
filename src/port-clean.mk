clean:
	rm -f $(LIB) *.o *~ port.a *.lst *.asm *.sym *~ *.cdb *.dep *.rul
#	DQ (5/8/2014): ROSE MODIFICATION: adding to clean rule
	rm -f rose_*

distclean: clean
	rm -f Makefile