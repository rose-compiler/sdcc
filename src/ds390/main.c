/*-------------------------------------------------------------------------
  main.h - ds390 specific general functions

  Copyright (C) 2000, Kevin Vigor

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2, or (at your option) any
  later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
-------------------------------------------------------------------------*/
/*
  Note that mlh prepended _ds390_ on the static functions.  Makes
  it easier to set a breakpoint using the debugger.
*/

#include "common.h"
#include "main.h"
#include "ralloc.h"
#include "gen.h"
#include "dbuf_string.h"

static char _defaultRules[] =
{
/* ROSE MODIFICATION (DQ (1/26/2014): Including header for initialization.
   include "peeph.rul"
*/
/* Generated file, DO NOT Edit!  */
/* To Make changes to rules edit */
/* <port>/peeph.def instead.     */
"\n"
"\n"
"replace restart {\n"
"        xch  a,%1\n"
"        xch  a,%1\n"
"} by {\n"
"        ; Peephole 2.a   removed redundant xch xch\n"
"}\n"
"\n"
"replace restart {\n"
"        mov  %1,#0x00\n"
"        mov  a,#0x00\n"
"} by {\n"
"        ; Peephole 3.a   changed mov to clr\n"
"        clr  a\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace restart {\n"
"        mov  %1,#0x00\n"
"        clr  a\n"
"} by {\n"
"        ; Peephole 3.b   changed mov to clr\n"
"        clr  a\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace restart {\n"
"        mov  %1,#0x00\n"
"        mov  %2,#0x00\n"
"        mov  a,%3\n"
"} by {\n"
"        ; Peephole 3.c   changed mov to clr\n"
"        clr  a\n"
"        mov  %1,a\n"
"        mov  %2,a\n"
"        mov  a,%3\n"
"}\n"
"\n"
"replace restart {\n"
"	mov	a,#0\n"
"} by {\n"
"        ; Peephole 3.d   changed mov to clr\n"
"	clr	a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  dptr,#%2\n"
"        mov  a,%1\n"
"        movx @dptr,a\n"
"} by {\n"
"        ; Peephole 100   removed redundant mov\n"
"        mov  %1,a\n"
"        mov  dptr,#%2\n"
"        movx @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	mov  a,acc\n"
"} by {\n"
"	;  Peephole 100.a   removed redundant mov\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,%1\n"
"        movx @dptr,a\n"
"        inc  dptr\n"
"        mov  a,%1\n"
"        movx @dptr,a\n"
"} by {\n"
"        ; Peephole 101   removed redundant mov\n"
"        mov  a,%1\n"
"        movx @dptr,a\n"
"        inc  dptr\n"
"        movx @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,%2\n"
"        ljmp %3\n"
"%4:\n"
"        mov  %1,%5\n"
"%3:\n"
"        mov  dpl,%1\n"
"%7:\n"
"        mov  sp,bp\n"
"        pop  bp\n"
"} by {\n"
"        ; Peephole 102   removed redundant mov\n"
"        mov  dpl,%2\n"
"        ljmp %3\n"
"%4:\n"
"        mov  dpl,%5\n"
"%3:\n"
"%7:\n"
"        mov  sp,bp\n"
"        pop  bp\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,%2\n"
"        ljmp %3\n"
"%4:\n"
"        mov  a%1,%5\n"
"%3:\n"
"        mov  dpl,%1\n"
"%7:\n"
"        mov  sp,bp\n"
"        pop  bp\n"
"} by {\n"
"        ; Peephole 103   removed redundant mov\n"
"        mov  dpl,%2\n"
"        ljmp %3\n"
"%4:\n"
"        mov  dpl,%5\n"
"%3:\n"
"%7:\n"
"        mov  sp,bp\n"
"        pop  bp\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,bp\n"
"        clr  c\n"
"        add  a,#0x01\n"
"        mov  r%1,a\n"
"} by {\n"
"        ; Peephole 104   optimized increment (acc not set to r%1, flags undefined)\n"
"        mov  r%1,bp\n"
"        inc  r%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 105   removed redundant mov\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        clr  c\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 106   removed redundant mov\n"
"        mov  %1,a\n"
"        clr  c\n"
"}\n"
"\n"
"replace {\n"
"        ljmp %1\n"
"%1:\n"
"} by {\n"
"        ; Peephole 107   removed redundant ljmp\n"
"%1:\n"
"}\n"
"\n"
"replace {\n"
"        jc   %1\n"
"        ljmp %5\n"
"%1:\n"
"} by {\n"
"        ; Peephole 108   removed ljmp by inverse jump logic\n"
"        jnc  %5\n"
"%1:\n"
"} if labelInRange\n"
"\n"
"replace {\n"
"        jz   %1\n"
"        ljmp %5\n"
"%1:\n"
"} by {\n"
"        ; Peephole 109   removed ljmp by inverse jump logic\n"
"        jnz  %5\n"
"%1:\n"
"} if labelInRange\n"
"\n"
"replace {\n"
"        jnz  %1\n"
"        ljmp %5\n"
"%1:\n"
"} by {\n"
"        ; Peephole 110   removed ljmp by inverse jump logic\n"
"        jz  %5\n"
"%1:\n"
"} if labelInRange\n"
"\n"
"replace {\n"
"        jb   %1,%2\n"
"        ljmp %5\n"
"%2:\n"
"} by {\n"
"        ; Peephole 111   removed ljmp by inverse jump logic\n"
"        jnb  %1,%5\n"
"%2:\n"
"} if labelInRange\n"
"\n"
"replace {\n"
"        jnb  %1,%2\n"
"        ljmp %5\n"
"%2:\n"
"} by {\n"
"       ; Peephole 112   removed ljmp by inverse jump logic\n"
"        jb   %1,%5\n"
"%2:\n"
"} if labelInRange\n"
"\n"
"replace {\n"
"        ljmp %5\n"
"%1:\n"
"} by {\n"
"        ; Peephole 132   changed ljmp to sjmp\n"
"        sjmp %5\n"
"%1:\n"
"} if labelInRange\n"
"\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cpl  a\n"
"%3:\n"
"        rrc  a\n"
"        mov  %4,c\n"
"} by {\n"
"        ; Peephole 113   optimized misc sequence\n"
"        clr  %4\n"
"        cjne %1,%2,%3\n"
"        setb %4\n"
"%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cpl  a\n"
"%3:\n"
"        rrc  a\n"
"        mov  %4,c\n"
"} by {\n"
"        ; Peephole 114   optimized misc sequence\n"
"        clr  %4\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        setb %4\n"
"%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cpl  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 115   jump optimization\n"
"        cjne %1,%2,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cpl  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 116   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        cpl  a\n"
"%3:\n"
"        jnz %4\n"
"} by {\n"
"        ; Peephole 117   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        cjne %13,%14,%3\n"
"        cpl  a\n"
"%3:\n"
"        jnz %4\n"
"} by {\n"
"        ; Peephole 118   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        cjne %13,%14,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        clr  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 119   jump optimization\n"
"        cjne %1,%2,%4\n"
"%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        clr  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 120   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %10,%11,%4\n"
"%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        clr  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 121   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %10,%11,%4\n"
"        cjne %12,%13,%4\n"
"%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        cjne %14,%15,%3\n"
"        clr  a\n"
"%3:\n"
"        jnz  %4\n"
"} by {\n"
"        ; Peephole 122   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %10,%11,%4\n"
"        cjne %12,%13,%4\n"
"        cjne %14,%15,%4\n"
"%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        clr  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 123   jump optimization\n"
"        cjne %1,%2,%3\n"
"        smp  %4\n"
"%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        clr  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 124   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        sjmp  %4\n"
"%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        clr  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 125   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        mov  a,#0x01\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        cjne %14,%15,%3\n"
"        clr  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 126   jump optimization\n"
"        cjne %1,%2,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %12,%13,%3\n"
"        cjne %14,%15,%3\n"
"        sjmp %4\n"
"%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        push psw\n"
"        mov  psw,%1\n"
"        push bp\n"
"        mov  bp,%2\n"
"%3:\n"
"        mov  %2,bp\n"
"        pop  bp\n"
"        pop  psw\n"
"        ret\n"
"} by {\n"
"        ; Peephole 127   removed misc sequence\n"
"        ret\n"
"} if labelRefCount %3 0\n"
"\n"
"replace {\n"
"        clr  a\n"
"        rlc  a\n"
"        jz   %1\n"
"} by {\n"
"        ; Peephole 128   jump optimization\n"
"        jnc  %1\n"
"}\n"
"\n"
"replace {\n"
"        clr  a\n"
"        rlc  a\n"
"        jnz  %1\n"
"} by {\n"
"        ; Peephole 129   jump optimization\n"
"        jc   %1\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,@r%2\n"
"} by {\n"
"        ; Peephole 130   changed target address mode r%1 to ar%1\n"
"        mov  ar%1,@r%2\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,%1\n"
"        subb a,#0x01\n"
"        mov  %2,a\n"
"        mov  %1,%2\n"
"} by {\n"
"        ; Peephole 131   optimized decrement (not caring for c)\n"
"        dec  %1\n"
"        mov  %2,%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,%2\n"
"        mov  ar%3,@r%1\n"
"        inc  r%3\n"
"        mov  r%4,%2\n"
"        mov  @r%4,ar%3\n"
"} by {\n"
"        ; Peephole 133   removed redundant moves\n"
"        mov  r%1,%2\n"
"        inc  @r%1\n"
"        mov  ar%3,@r%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,%2\n"
"        mov  ar%3,@r%1\n"
"        dec  r%3\n"
"        mov  r%4,%2\n"
"        mov  @r%4,ar%3\n"
"} by {\n"
"        ; Peephole 134   removed redundant moves\n"
"        mov  r%1,%2\n"
"        dec  @r%1\n"
"        mov  ar%3,@r%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        mov  a,r%2\n"
"        orl  a,r%1\n"
"} by {\n"
"        ; Peephole 135   removed redundant mov\n"
"        mov  r%1,a\n"
"        orl  a,r%2\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"	mov  dpx,%4\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 136a   removed redundant moves\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"	mov  dpx,%4\n"
"} if 24bitMode\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 136   removed redundant moves\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"}\n"
"\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        anl  ar%1,%2\n"
"        mov  a,r%1\n"
"} by {\n"
"        ; Peephole 139   removed redundant mov\n"
"        anl  a,%2\n"
"        mov  r%1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        orl  ar%1,%2\n"
"        mov  a,r%1\n"
"} by {\n"
"        ; Peephole 140   removed redundant mov\n"
"        orl  a,%2\n"
"        mov  r%1,a }\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        xrl  ar%1,%2\n"
"        mov  a,r%1\n"
"} by {\n"
"        ; Peephole 141   removed redundant mov\n"
"        xrl  a,%2\n"
"        mov  r%1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        mov  r%2,ar%1\n"
"        mov  ar%1,@r%2\n"
"} by {\n"
"        ; Peephole 142   removed redundant moves\n"
"        mov  r%2,a\n"
"        mov  ar%1,@r%2\n"
"}\n"
"\n"
"replace {\n"
"        rlc  a\n"
"        mov  acc.0,c\n"
"} by {\n"
"        ; Peephole 143   converted rlc to rl\n"
"        rl   a\n"
"}\n"
"\n"
"replace {\n"
"        rrc  a\n"
"        mov  acc.7,c\n"
"} by {\n"
"        ; Peephole 144   converted rrc to rc\n"
"        rr   a\n"
"}\n"
"\n"
"replace {\n"
"        clr  c\n"
"        addc a,%1\n"
"} by {\n"
"        ; Peephole 145   changed to add without carry\n"
"        add  a,%1\n"
"}\n"
"\n"
"replace {\n"
"        clr  c\n"
"        mov  a,%1\n"
"        addc a,%2\n"
"} by {\n"
"        ; Peephole 146   changed to add without carry\n"
"        mov  a,%1\n"
"        add  a,%2\n"
"}\n"
"\n"
"replace {\n"
"        orl  r%1,a\n"
"} by {\n"
"        ; Peephole 147   changed target address mode r%1 to ar%1\n"
"        orl  ar%1,a\n"
"}\n"
"\n"
"replace {\n"
"        anl  r%1,a\n"
"} by {\n"
"        ; Peephole 148   changed target address mode r%1 to ar%1\n"
"        anl  ar%1,a\n"
"}\n"
"\n"
"replace {\n"
"        xrl  r%1,a\n"
"} by {\n"
"        ; Peephole 149   changed target address mode r%1 to ar%1\n"
"        xrl  ar%1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  dpl,%1\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 150   removed misc moves via dpl before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  dpl,%1\n"
"        mov  dph,%2\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 151   removed misc moves via dph, dpl before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  dpl,%1\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 152   removed misc moves via dph, dpl before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  dpl,%1\n"
"        mov  dph,%2\n"
"        mov  b,%3\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 153   removed misc moves via dph, dpl, b before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  dpl,%1\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 154   removed misc moves via dph, dpl, b before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  dpl,%1\n"
"        mov  dph,%2\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 155   removed misc moves via dph, dpl, b before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  %4,a\n"
"        mov  dpl,%1\n"
"        mov  dph,%2\n"
"        mov  b,%3\n"
"        mov  a,%4\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 156   removed misc moves via dph, dpl, b, a before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  %4,a\n"
"        mov  dpl,%1\n"
"        mov  dph,%2\n"
"%9:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 157   removed misc moves via dph, dpl, b, a before return\n"
"%9:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,dpl\n"
"        mov  %2,dph\n"
"        mov  %3,b\n"
"        mov  %4,a\n"
"        mov  dpl,%1\n"
"%9:\n"
"        ret } by {\n"
"        ; Peephole 158   removed misc moves via dph, dpl, b, a before return\n"
"%9:\n"
"        ret }\n"
"\n"
"replace {\n"
"        mov  %1,#%2\n"
"        xrl  %1,#0x80\n"
"} by {\n"
"        ; Peephole 159   avoided xrl during execution\n"
"        mov  %1,#(%2 ^ 0x80)\n"
"}\n"
"\n"
"replace {\n"
"        jnc  %1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 160   removed sjmp by inverse jump logic\n"
"        jc   %2\n"
"%1:}\n"
"\n"
"replace {\n"
"        jc   %1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 161   removed sjmp by inverse jump logic\n"
"        jnc  %2\n"
"%1:}\n"
"\n"
"replace {\n"
"        jnz  %1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 162   removed sjmp by inverse jump logic\n"
"        jz   %2\n"
"%1:}\n"
"\n"
"replace {\n"
"        jz   %1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 163   removed sjmp by inverse jump logic\n"
"        jnz  %2\n"
"%1:}\n"
"\n"
"replace {\n"
"        jnb  %3,%1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 164   removed sjmp by inverse jump logic\n"
"        jb   %3,%2\n"
"%1:\n"
"}\n"
"\n"
"replace {\n"
"        jb   %3,%1\n"
"        sjmp %2\n"
"%1:\n"
"} by {\n"
"        ; Peephole 165   removed sjmp by inverse jump logic\n"
"        jnb  %3,%2\n"
"%1:\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,%2\n"
"        mov  %3,%1\n"
"        mov  %2,%1\n"
"} by {\n"
"        ; Peephole 166   removed redundant mov\n"
"        mov  %1,%2\n"
"        mov  %3,%1 }\n"
"\n"
"replace {\n"
"        mov  c,%1\n"
"        cpl  c\n"
"        mov  %1,c\n"
"} by {\n"
"        ; Peephole 167   removed redundant bit moves (c not set to %1)\n"
"        cpl  %1 }\n"
"\n"
"replace {\n"
"        jnb  %1,%2\n"
"        sjmp %3\n"
"%2:} by {\n"
"        ; Peephole 168   jump optimization\n"
"        jb   %1,%3\n"
"%2:}\n"
"\n"
"replace {\n"
"        jb   %1,%2\n"
"        sjmp %3\n"
"%2:} by {\n"
"        ; Peephole 169   jump optimization\n"
"        jnb  %1,%3\n"
"%2:}\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cpl  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 170   jump optimization\n"
"        cjne %1,%2,%4\n"
"%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cpl  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 171   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %9,%10,%4\n"
"%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        cpl  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 172   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %9,%10,%4\n"
"        cjne %11,%12,%4\n"
"%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        clr  a\n"
"        cjne %1,%2,%3\n"
"        cjne %9,%10,%3\n"
"        cjne %11,%12,%3\n"
"        cjne %13,%14,%3\n"
"        cpl  a\n"
"%3:\n"
"        jz   %4\n"
"} by {\n"
"        ; Peephole 173   jump optimization\n"
"        cjne %1,%2,%4\n"
"        cjne %9,%10,%4\n"
"        cjne %11,%12,%4\n"
"        cjne %13,%14,%4\n"
"%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        mov  r%1,%2\n"
"        clr  c\n"
"        mov  a,r%1\n"
"        subb a,#0x01\n"
"        mov  %2,a\n"
"} by {\n"
"        ; Peephole 174   optimized decrement (acc not set to %2, flags undefined)\n"
"        mov  r%1,%2\n"
"        dec  %2\n"
"}\n"
"\n"
"\n"
"replace {\n"
"        mov  r%1,%2\n"
"        mov  a,r%1\n"
"        add  a,#0x01\n"
"        mov  %2,a\n"
"} by {\n"
"        ; Peephole 175   optimized increment (acc not set to %2, flags undefined)\n"
"        mov  r%1,%2\n"
"        inc  %2\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,@r%2\n"
"        inc  %1\n"
"        mov  @r%2,%1\n"
"} by {\n"
"        ; Peephole 176   optimized increment, removed redundant mov\n"
"        inc  @r%2\n"
"        mov  %1,@r%2\n"
"}\n"
"\n"
"\n"
"replace {\n"
"        mov  a,%1\n"
"        mov  b,a\n"
"        mov  a,%2\n"
"} by {\n"
"        ; Peephole 178   removed redundant mov\n"
"        mov  b,%1\n"
"        mov  a,%2\n"
"}\n"
"\n"
"replace {\n"
"        mov  b,#0x00\n"
"        mov  a,#0x00\n"
"} by {\n"
"        ; Peephole 179   changed mov to clr\n"
"        clr  a\n"
"        mov  b,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,#0x00\n"
"} by {\n"
"        ; Peephole 180   changed mov to clr\n"
"        clr  a\n"
"}\n"
"\n"
"replace {\n"
"        mov  dpl,#0x00\n"
"        mov  dph,#0x00\n"
" 	mov  dpx,#0x00\n"
"} by {\n"
"        ; Peephole 181a   used 24 bit load of dptr\n"
"        mov  dptr,#0x0000\n"
"} if 24bitMode\n"
"\n"
"replace {\n"
"        mov  dpl,#0x00\n"
"        mov  dph,#0x00\n"
"} by {\n"
"        ; Peephole 181   used 16 bit load of dptr\n"
"        mov  dptr,#0x0000\n"
"}\n"
"\n"
"replace {\n"
"	mov dpl,#%1\n"
"	mov dph,#(%1 >> 8)\n"
"	mov dpx,#(%1 >> 16)\n"
"} by {\n"
"	; Peephole 182b used 24 bit load of DPTR\n"
"	mov dptr,#%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  dpl,#0x%1\n"
"        mov  dph,#0x%2\n"
"	mov  dpx,#0x%3\n"
"} by {\n"
"        ; Peephole 182a   used 24 bit load of dptr\n"
"        mov	dptr,#0x%3%2%1\n"
"} if 24bitMode(), portIsDS390\n"
"\n"
"replace {\n"
"        mov  dpl,#%1\n"
"        mov  dph,#%2\n"
"	mov  dpx,#%3\n"
"} by {\n"
"        ; Peephole 182b   used 24 bit load of dptr\n"
"        mov  dptr,#((%3 << 16) + (%2 << 8) + %1)\n"
"} if 24bitMode(), portIsDS390\n"
"\n"
"replace {\n"
"        mov  dpl,#0x%1\n"
"        mov  dph,#0x%2\n"
"} by {\n"
"        ; Peephole 182c   used 16 bit load of dptr\n"
"        mov  dptr,#0x%2%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  dpl,#%1\n"
"        mov  dph,#%2\n"
"} by {\n"
"        ; Peephole 182   used 16 bit load of dptr\n"
"        mov  dptr,#(((%2)<<8) + %1)\n"
"}\n"
"\n"
"replace {\n"
"        anl  %1,#%2\n"
"        anl  %1,#%3\n"
"} by {\n"
"        ; Peephole 183   avoided anl during execution\n"
"        anl  %1,#(%2 & %3)\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        cpl  a\n"
"        mov  %1,a\n"
"} by {\n"
"        ; Peephole 184   removed redundant mov\n"
"        cpl  a\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        inc  %1\n"
"} by {\n"
"        ; Peephole 185   changed order of increment (acc incremented also!)\n"
"        inc  a\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace restart {\n"
"	add  a,#%1\n"
"	mov  dpl,a\n"
"	clr  a\n"
"	addc a,#(%1 >> 8)\n"
"	mov  dph,a\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %2,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %3,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %4,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"} by {\n"
"	; Peephole 186.a   optimized movc sequence\n"
"	mov  b,a\n"
"	mov  dptr,#%1\n"
"	movc a,@a+dptr\n"
"	mov  %2,a\n"
"	inc  dptr\n"
"	mov  a,b\n"
"	movc a,@a+dptr\n"
"	mov  %3,a\n"
"	inc  dptr\n"
"	mov  a,b\n"
"	movc a,@a+dptr\n"
"	mov  %4,a\n"
"	inc  dptr\n"
"	mov  a,b\n"
"	movc a,@a+dptr\n"
"}\n"
"\n"
"replace restart {\n"
"	add  a,#%1\n"
"	mov  dpl,a\n"
"	clr  a\n"
"	addc a,#(%1 >> 8)\n"
"	mov  dph,a\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %2,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %3,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"} by {\n"
"	; Peephole 186.b   optimized movc sequence\n"
"	mov  b,a\n"
"	mov  dptr,#%1\n"
"	movc a,@a+dptr\n"
"	mov  %2,a\n"
"	inc  dptr\n"
"	mov  a,b\n"
"	movc a,@a+dptr\n"
"	mov  %3,a\n"
"	inc  dptr\n"
"	mov  a,b\n"
"	movc a,@a+dptr\n"
"}\n"
"\n"
"replace restart {\n"
"	add  a,#%1\n"
"	mov  dpl,a\n"
"	clr  a\n"
"	addc a,#(%1 >> 8)\n"
"	mov  dph,a\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"	mov  %2,a\n"
"	inc  dptr\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"} by {\n"
"	; Peephole 186.c   optimized movc sequence\n"
"	mov  %2,a\n"
"	mov  dptr,#%1\n"
"	movc a,@a+dptr\n"
"	xch  a,%2\n"
"	inc  dptr\n"
"	movc a,@a+dptr\n"
"}\n"
"\n"
"replace {\n"
"	add  a,#%1\n"
"	mov  dpl,a\n"
"	clr  a\n"
"	addc a,#(%1 >> 8)\n"
"	mov  dph,a\n"
"	clr  a\n"
"	movc a,@a+dptr\n"
"} by {\n"
"	; Peephole 186.d  optimized movc sequence\n"
"	mov  dptr,#%1\n"
"	movc a,@a+dptr\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,%2\n"
"        anl  ar%1,#%3\n"
"        mov  a,r%1\n"
"} by {\n"
"        ; Peephole 187   used a instead of ar%1 for anl\n"
"        mov  a,%2\n"
"        anl  a,#%3\n"
"        mov  r%1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  dptr,%2\n"
"        movc a,@a+dptr\n"
"        mov  %1,a\n"
"} by {\n"
"        ; Peephole 188   removed redundant mov\n"
"        mov  dptr,%2\n"
"        movc a,@a+dptr\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace {\n"
"        anl  a,#0x0f\n"
"        mov  %1,a\n"
"        mov  a,#0x0f\n"
"        anl  a,%1\n"
"} by {\n"
"        ; Peephole 189   removed redundant mov and anl\n"
"        anl  a,#0x0f\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,%1\n"
"        lcall __gptrput\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 190   removed redundant mov\n"
"        mov  a,%1\n"
"        lcall __gptrput\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"        mov  b,%4\n"
"        mov  a,%1\n"
"} by {\n"
"        ; Peephole 191   removed redundant mov\n"
"        mov  %1,a\n"
"        mov  dpl,%2\n"
"        mov  dph,%3\n"
"        mov  b,%4\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,a\n"
"        mov  @r%2,ar%1\n"
"} by {\n"
"        ; Peephole 192   used a instead of ar%1 as source\n"
"        mov  r%1,a\n"
"        mov  @r%2,a\n"
"}\n"
"\n"
"replace {\n"
"        jnz  %3\n"
"        mov  a,%4\n"
"        jnz  %3\n"
"        mov  a,%9\n"
"        jnz  %3\n"
"        mov  a,%12\n"
"        cjne %13,%14,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 193.a optimized misc jump sequence\n"
"        jnz  %8\n"
"        mov  a,%4\n"
"        jnz  %8\n"
"        mov  a,%9\n"
"        jnz  %8\n"
"        mov  a,%12\n"
"        cjne %13,%14,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        mov  a,%4\n"
"        cjne %5,%6,%3\n"
"        mov  a,%9\n"
"        cjne %10,%11,%3\n"
"        mov  a,%12\n"
"        cjne %13,%14,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 193   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        mov  a,%4\n"
"        cjne %5,%6,%8\n"
"        mov  a,%9\n"
"        cjne %10,%11,%8\n"
"        mov  a,%12\n"
"        cjne %13,%14,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        cjne @%1,%2,%3\n"
"        inc  %1\n"
"        cjne @%1,%6,%3\n"
"        inc  %1\n"
"        cjne @%1,%11,%3\n"
"       	inc  %1\n"
"        cjne @%1,%14,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 193.a   optimized misc jump sequence\n"
"        cjne @%1,%2,%8\n"
"        inc  %1\n"
"        cjne @%1,%6,%8\n"
"        inc  %1\n"
"        cjne @%1,%11,%8\n"
"        inc  %1\n"
"        cjne @%1,%14,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        cjne %5,%6,%3\n"
"        cjne %10,%11,%3\n"
"        cjne %13,%14,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 194   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        cjne %5,%6,%8\n"
"        cjne %10,%11,%8\n"
"        cjne %13,%14,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 4\n"
"\n"
"replace {\n"
"        jnz  %3\n"
"        mov  a,%4\n"
"        jnz  %3\n"
"        mov  a,%9\n"
"        cjne %10,%11,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 195.a optimized misc jump sequence\n"
"        jnz  %8\n"
"        mov  a,%4\n"
"        jnz  %8\n"
"        mov  a,%9\n"
"        cjne %10,%11,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        mov  a,%4\n"
"        cjne %5,%6,%3\n"
"        mov  a,%9\n"
"        cjne %10,%11,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 195   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        mov  a,%4\n"
"        cjne %5,%6,%8\n"
"        mov  a,%9\n"
"        cjne %10,%11,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        cjne @%1,%2,%3\n"
"        inc  %1\n"
"        cjne @%1,%6,%3\n"
"        inc  %1\n"
"        cjne @%1,%11,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 195.a   optimized misc jump sequence\n"
"        cjne @%1,%2,%8\n"
"        inc  %1\n"
"        cjne @%1,%6,%8\n"
"        inc  %1\n"
"        cjne @%1,%11,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        cjne %5,%6,%3\n"
"        cjne %10,%11,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 196   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        cjne %5,%6,%8\n"
"        cjne %10,%11,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 3\n"
"\n"
"replace {\n"
"        jnz  %3\n"
"        mov  a,%4\n"
"        cjne %5,%6,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 197.a optimized misc jump sequence\n"
"        jnz  %8\n"
"        mov  a,%4\n"
"        cjne %5,%6,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        mov  a,%4\n"
"        cjne %5,%6,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 197   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        mov  a,%4\n"
"        cjne %5,%6,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        cjne @%1,%2,%3\n"
"        inc  %1\n"
"        cjne @%1,%6,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 197.a   optimized misc jump sequence\n"
"        cjne @%1,%2,%8\n"
"        inc   %1\n"
"        cjne @%1,%6,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        cjne %5,%6,%3\n"
"        sjmp %7\n"
"%3:\n"
"        sjmp %8\n"
"} by {\n"
"        ; Peephole 198   optimized misc jump sequence\n"
"        cjne %1,%2,%8\n"
"        cjne %5,%6,%8\n"
"        sjmp %7\n"
";%3:\n"
"} if labelRefCount %3 2\n"
"\n"
"replace {\n"
"        cjne %1,%2,%3\n"
"        sjmp %4\n"
"%3:\n"
"        sjmp %5\n"
"} by {\n"
"        ; Peephole 199   optimized misc jump sequence\n"
"        cjne %1,%2,%5\n"
"        sjmp %4\n"
";%3:\n"
"} if labelRefCount %3 1\n"
"\n"
"replace {\n"
"        sjmp %1\n"
"%1:\n"
"} by {\n"
"        ; Peephole 200   removed redundant sjmp\n"
"%1:\n"
"}\n"
"\n"
"replace {\n"
"        sjmp %1\n"
"%2:\n"
"%1:\n"
"} by {\n"
"        ; Peephole 201   removed redundant sjmp\n"
"%2:\n"
"%1:\n"
"}\n"
"\n"
"replace {\n"
"        push  acc\n"
"        mov   dptr,%1\n"
"        pop   acc\n"
"} by {\n"
"        ; Peephole 202   removed redundant push pop\n"
"        mov   dptr,%1\n"
"}\n"
"\n"
"replace {\n"
"        push  acc\n"
"        pop   acc\n"
"} by {\n"
"        ; Peephole 202b   removed redundant push pop\n"
"}\n"
"\n"
"replace {\n"
"        mov  r%1,_spx\n"
"        lcall %2\n"
"        mov  r%1,_spx\n"
"} by {\n"
"        ; Peephole 203   removed mov  r%1,_spx\n"
"        lcall %2\n"
"}\n"
"\n"
"replace {\n"
"        mov  %1,a\n"
"        add  a,acc\n"
"        mov  %1,a\n"
"} by {\n"
"        ; Peephole 204   removed redundant mov\n"
"        add  a,acc\n"
"        mov  %1,a\n"
"}\n"
"\n"
"replace {\n"
"        djnz %1,%2\n"
"        sjmp  %3\n"
"%2:\n"
"        sjmp  %4\n"
"%3:\n"
"} by {\n"
"        ; Peephole 205   optimized misc jump sequence\n"
"        djnz %1,%4\n"
"%2:\n"
"%3:\n"
"} if labelRefCount %2 1\n"
"\n"
"replace {\n"
"        mov  %1,%1\n"
"} by {\n"
"        ; Peephole 206   removed redundant mov %1,%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,_bp\n"
"        add  a,#0x00\n"
"        mov  %1,a\n"
"} by {\n"
"        ; Peephole 207   removed zero add (acc not set to %1, flags undefined)\n"
"        mov  %1,_bp\n"
"}\n"
"\n"
"replace {\n"
"        push  acc\n"
"        mov   r%1,_bp\n"
"        pop   acc\n"
"} by {\n"
"        ; Peephole 208   removed redundant push pop\n"
"        mov   r%1,_bp\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,_bp\n"
"        add  a,#0x00\n"
"        inc  a\n"
"        mov  %1,a\n"
"} by {\n"
"        ; Peephole 209   optimized increment (acc not set to %1, flags undefined)\n"
"        mov  %1,_bp\n"
"        inc  %1\n"
"}\n"
"\n"
"replace {\n"
"        mov  dptr,#((((%1 >> 16)) <<16) + (((%1 >> 8)) <<8) + %1)\n"
"} by {\n"
"        ; Peephole 210a   simplified expression\n"
"        mov  dptr,#%1\n"
"} if 24bitMode\n"
"\n"
"replace {\n"
"        mov  dptr,#((((%1 >> 8)) <<8) + %1)\n"
"} by {\n"
"        ; Peephole 210   simplified expression\n"
"        mov  dptr,#%1\n"
"}\n"
"\n"
"replace restart {\n"
"        push ar%1\n"
"        pop  ar%1\n"
"} by {\n"
"        ; Peephole 211   removed redundant push r%1 pop r%1\n"
"}\n"
"\n"
"replace {\n"
"	mov  a,_bp\n"
"	add  a,#0x01\n"
"	mov  r%1,a\n"
"} by {\n"
"	; Peephole 212  reduced add sequence to inc\n"
"	mov  r%1,_bp\n"
"	inc  r%1\n"
"}\n"
"\n"
"replace {\n"
"	mov  %1,#(( %2 >> 8 ) ^ 0x80)\n"
"} by {\n"
"	; Peephole 213.a inserted fix\n"
"	mov  %1,#(%2 >> 8)\n"
"	xrl  %1,#0x80\n"
"} if portIsDS390\n"
"\n"
"replace {\n"
"	mov  %1,#(( %2 >> 16 ) ^ 0x80)\n"
"} by {\n"
"	; Peephole 213.b inserted fix\n"
"	mov  %1,#(%2 >> 16)\n"
"	xrl  %1,#0x80\n"
"} if portIsDS390\n"
"\n"
"replace {\n"
"	mov  %1,#(( %2 + %3 >> 8 ) ^ 0x80)\n"
"} by {\n"
"	; Peephole 213.c inserted fix\n"
"	mov  %1,#((%2 + %3) >> 8)\n"
"	xrl  %1,#0x80\n"
"} if portIsDS390\n"
"\n"
"replace  {\n"
"	mov  %1,a\n"
"	mov  a,%2\n"
"	add  a,%1\n"
"} by {\n"
"	; Peephole 214 reduced some extra movs\n"
"	mov  %1,a\n"
"	add  a,%2\n"
"} if notSame(%1 %2)\n"
"\n"
"replace {\n"
"	mov  %1,a\n"
"	add  a,%2\n"
"	mov  %1,a\n"
"} by {\n"
"	; Peephole 215 removed some movs\n"
"	add  a,%2\n"
"	mov  %1,a\n"
"} if notSame(%1 %2)\n"
"\n"
"replace {\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"} by {\n"
"	; Peephole 216 simplified clear (2bytes)\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"}\n"
"\n"
"replace {\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	inc   r%1\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"} by {\n"
"	; Peephole 217 simplified clear (3bytes)\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"}\n"
"\n"
"replace {\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	inc   r%1\n"
"	inc   r%1\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"	dec   r%1\n"
"	mov   @r%1,a\n"
"} by {\n"
"	; Peephole 218 simplified clear (4bytes)\n"
"	mov   r%1,%2\n"
"	clr   a\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"	inc   r%1\n"
"	mov   @r%1,a\n"
"}\n"
"\n"
"replace {\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"	mov   dptr,%1\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"} by {\n"
"	; Peephole 219 removed redundant clear\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"	mov   dptr,%1\n"
"	movx  @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"	mov   dptr,%1\n"
"	movx  @dptr,a\n"
"	mov   dptr,%2\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"} by {\n"
"	; Peephole 219a removed redundant clear\n"
"	clr   a\n"
"	movx  @dptr,a\n"
"	mov   dptr,%1\n"
"	movx  @dptr,a\n"
"	mov   dptr,%2\n"
"	movx  @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"        mov     dps, #0\n"
"        mov     dps, #1\n"
"} by {\n"
"        ; Peephole 220a removed bogus DPS set\n"
"        mov     dps, #1\n"
"}\n"
"\n"
"replace {\n"
"        mov     dps, #1\n"
"        mov     dps, #0\n"
"} by {\n"
"        ; Peephole 220b removed bogus DPS set\n"
"        mov     dps, #0\n"
"}\n"
"\n"
"replace {\n"
"        mov     dps, #0\n"
"        mov     dps, #0x01\n"
"} by {\n"
"        ; Peephole 220c removed bogus DPS set\n"
"}\n"
"\n"
"replace {\n"
"        mov     dps,#1\n"
"	inc	dptr\n"
"        mov     dps,#1\n"
"} by {\n"
"        ; Peephole 220d removed bogus DPS set\n"
"        mov     dps,#1\n"
"	inc	dptr\n"
"}\n"
"\n"
"replace {\n"
"	mov	%1 + %2,(%2 + %1)\n"
"} by {\n"
"	; Peephole 221a remove redundant move\n"
"}\n"
"\n"
"replace {\n"
"	mov	(%1 + %2 + %3),((%2 + %1) + %3)\n"
"} by {\n"
"	; Peephole 221b remove redundant move\n"
"}\n"
"\n"
"replace {\n"
"	dec	r%1\n"
"	inc	r%1\n"
"} by {\n"
"	; removed dec/inc pair\n"
"}\n"
"\n"
"replace {\n"
"	mov     dps, #0\n"
"	mov     %1,a\n"
"	mov     dps, #1\n"
"} by {\n"
"	; Peephole 222 removed DPS abuse.\n"
"	mov     %1,a\n"
"        mov     dps, #1\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #0\n"
"	xch	a, ap\n"
"	mov 	%1, ap\n"
"	mov	dps, #1\n"
"} by {\n"
"	; Peephole 222a removed DPS abuse.\n"
"	xch     a, ap\n"
"        mov     %1, ap\n"
"        mov     dps, #1\n"
"}\n"
"\n"
"replace {\n"
"	mov 	dps, #%1\n"
"	inc	dptr\n"
"	movx	a,@dptr\n"
"	mov	%2,a\n"
"	mov	dps, #%1\n"
"} by {\n"
"	mov     dps, #%1\n"
"        inc     dptr\n"
"        movx    a,@dptr\n"
"        mov     %2,a\n"
"; Peephole 223: yet more DPS abuse removed.\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #0\n"
"	inc	dps\n"
"} by {\n"
"	mov	dps, #1\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #%1\n"
"	mov	dptr, %2\n"
"	mov	dps, #%1\n"
"} by {\n"
"	mov     dps, #%1\n"
"        mov     dptr, %2\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #1\n"
"	mov	dptr, %1\n"
"	mov	dps, #0\n"
"	mov	dptr, %2\n"
"	inc	dps\n"
"} by {\n"
"	mov	dps, #0\n"
"	mov	dptr, %2\n"
"	inc	dps\n"
"	mov	dptr, %1\n"
"; Peephole 224a: DPS usage re-arranged.\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #%1\n"
"	mov	dptr, %2\n"
"	mov	dps, #%3\n"
"	mov	dptr, %4\n"
"	mov	dps, #%1\n"
"} by {\n"
"	mov	dps, #%3\n"
"	mov	dptr, %4\n"
"	mov	dps, #%1\n"
"	mov	dptr, %2\n"
"; Peephole 224: DPS usage re-arranged.\n"
"}\n"
"\n"
"replace {\n"
"	mov	dps, #1\n"
"	mov	dptr, %1\n"
"	mov	dps, #0\n"
"} by {\n"
"	mov	dps, #1\n"
"	mov	dptr, %1\n"
"	dec	dps\n"
"}\n"
"\n"
"replace {\n"
"	xch	a, ap\n"
"	add	a, ap\n"
"} by {\n"
"	add 	a, ap\n"
"}\n"
"\n"
"replace {\n"
"	xch	a, ap\n"
"	addc	a, ap\n"
"} by {\n"
"	addc 	a, ap\n"
"}\n"
"\n"
"replace {\n"
"	inc 	dps\n"
"	mov	dps, #%1\n"
"} by {\n"
"	mov     dps, #%1\n"
"}\n"
"\n"
"replace {\n"
"	dec 	dps\n"
"	mov	dps, #%1\n"
"} by {\n"
"	mov     dps, #%1\n"
"}\n"
"\n"
"\n"
"replace restart {\n"
"	add	a,#%1\n"
"	mov	dpl,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 8)\n"
"	mov	dph,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 16)\n"
"	mov	dpx,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%2,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%3,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%4,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"} by {\n"
"	; Peephole 227.a movc optimize\n"
"	mov	b,a\n"
"	mov	dptr,#%1\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%2,a\n"
"	mov	a,b\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%3,a\n"
"	mov	a,b\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%4,a\n"
"	mov	a,b\n"
"	movc	a,@a+dptr\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a,#%1\n"
"	mov	dpl,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 8)\n"
"	mov	dph,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 16)\n"
"	mov	dpx,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%2,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%3,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"} by {\n"
"	; Peephole 227.b movc optimize\n"
"	mov	b,a\n"
"	mov	dptr,#%1\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%2,a\n"
"	mov	a,b\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%3,a\n"
"	mov	a,b\n"
"	movc	a,@a+dptr\n"
"}\n"
"\n"
"replace restart {\n"
"	add	a,#%1\n"
"	mov	dpl,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 8)\n"
"	mov	dph,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 16)\n"
"	mov	dpx,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	mov	%2,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"} by {\n"
"	; Peephole 227.c movc optimize\n"
"	mov	%2,a\n"
"	mov	dptr,#%1\n"
"	movc	a,@a+dptr\n"
"	inc	dptr\n"
"	xch	a,%2\n"
"	movc	a,@a+dptr\n"
"}\n"
"\n"
"replace {\n"
"	add	a,#%1\n"
"	mov	dpl,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 8)\n"
"	mov	dph,a\n"
"	clr  a\n"
"	addc	a,#(%1 >> 16)\n"
"	mov	dpx,a\n"
"	clr	a\n"
"	movc	a,@a+dptr\n"
"} by {\n"
"	; Peephole 227.d movc optimize\n"
"	mov	dptr,#%1\n"
"	movc	a,@a+dptr\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,%2\n"
"	mov	ar%1,%3\n"
"} by {\n"
"	; Peephole 228 redundant move\n"
"	mov	ar%1,%3\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,a\n"
"	dec	r%1\n"
"	mov	a,r%1\n"
"} by {\n"
"	; Peephole 229.a redundant move\n"
"	dec	a\n"
"	mov	r%1,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,a\n"
"	mov	r%2,b\n"
"	mov	a,r%1\n"
"} by {\n"
"	; Peephole 229.b redundant move\n"
"	mov	r%1,a\n"
"	mov	r%2,b\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,a\n"
"	mov	r%2,b\n"
"	add	a,#%3\n"
"	mov	r%1,a\n"
"	mov	a,r%2\n"
"	addc	a,#(%3 >> 8)\n"
"	mov	r%2,a\n"
"} by {\n"
"	; Peephole 229.c redundant move\n"
"	add	a,#%3\n"
"	mov	r%1,a\n"
"	mov	a,b\n"
"	addc	a,#(%3 >> 8)\n"
"	mov	r%2,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	a,%1\n"
"	mov	b,a\n"
"	movx	a,%2\n"
"} by {\n"
"	; Peephole 229.d redundant move\n"
"	mov	b,%1\n"
"	movx	a,%2\n"
"}\n"
"\n"
"replace {\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	mov  	r%4,a\n"
"	add  	a,#0x01\n"
"	mov  	r%5,a\n"
"	mov  	dpl,r%1\n"
"	mov  	dph,r%2\n"
"	mov  	dpx,r%3\n"
"	movx	@dptr,a\n"
"} by {\n"
"	; Peephole 230.a save reload dptr\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	mov  	r%4,a\n"
"	add  	a,#0x01\n"
"	mov  	r%5,a\n"
"	movx	@dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	mov	r%4,a\n"
"	dec	r%4\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	mov	a,r%4\n"
"	movx	@dptr,a\n"
"} by {\n"
"	; Peephole 230.b save reload dptr\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	dec	a\n"
"	mov	r%4,a\n"
"	movx	@dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	inc  	a\n"
"	mov  	r%4,a\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	mov	a,r%4\n"
"	movx	@dptr,a\n"
"} by {\n"
"	; Peephole 230.c save reload dptr\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"	movx	a,@dptr\n"
"	inc  	a\n"
"	mov  	r%4,a\n"
"	movx	@dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,dpl\n"
"	mov	r%2,dph\n"
"	mov	r%3,dpx\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"} by {\n"
"	; Peephole 230.d save reload dptr\n"
"	mov	r%1,dpl\n"
"	mov	r%2,dph\n"
"	mov	r%3,dpx\n"
"}\n"
"\n"
"replace {\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        movx    a,@dptr\n"
"        mov     r%4,a\n"
"        orl     ar%4,#%5\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        mov     a,r1\n"
"        movx    @dptr,a\n"
"} by {\n"
"	; Peephole 230.e save reload dptr\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        movx    a,@dptr\n"
"        orl     a,#%5\n"
"        mov     r%4,a\n"
"        movx    @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        movx    a,@dptr\n"
"        mov     r%4,a\n"
"        anl     ar%4,#%5\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        mov     a,r1\n"
"        movx    @dptr,a\n"
"} by {\n"
"	; Peephole 230.e save reload dptr\n"
"        mov     dpl,r%1\n"
"        mov     dph,r%2\n"
"        mov     dpx,r%3\n"
"        movx    a,@dptr\n"
"        anl     a,#%5\n"
"        mov     r%4,a\n"
"        movx    @dptr,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,dpl\n"
"	mov	r%2,dph\n"
"	mov	r%3,dpx\n"
"	mov	a,r%4\n"
"	inc	dps\n"
"	movx	@dptr,a\n"
"	inc	dptr\n"
"	mov	dps,#0\n"
"	mov	dpl,r%1\n"
"	mov	dph,r%2\n"
"	mov	dpx,r%3\n"
"} by {\n"
"	; Peephole 230.f save reload dptr\n"
"	mov	r%1,dpl\n"
"	mov	r%2,dph\n"
"	mov	r%3,dpx\n"
"	mov	a,r%4\n"
"	inc	dps\n"
"	movx	@dptr,a\n"
"	inc	dptr\n"
"	mov	dps,#0\n"
"}\n"
"\n"
"replace {\n"
"	mov	ar%1,r%2\n"
"	mov	ar%3,r%1\n"
"	mov	r%1,#0x00\n"
"	mov	ar%2,r%4\n"
"	mov	r3,#0x00\n"
"} by {\n"
"	; Peephole 231.a simplified moves\n"
"	mov	ar%3,r%2\n"
"	mov	ar%2,r%4\n"
"	mov	r%4,#0\n"
"	mov	r%1,#0\n"
"}\n"
"\n"
"replace {\n"
"	mov	r%1,#0\n"
"	mov	r%2,#0\n"
"	mov	a,r%2\n"
"	orl	a,r%3\n"
"	mov	%4,a\n"
"	mov	a,r%5\n"
"	orl	a,r%1\n"
"	mov	%6,a\n"
"} by {\n"
"	; Peephole 231.b simplified or\n"
"	mov	r%1,#0\n"
"	mov	r%2,#0\n"
"	mov	a,r%3\n"
"	mov	%4,a\n"
"	mov	a,r%5\n"
"	mov	%6,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	a,r%1\n"
"	mov	b,r%2\n"
"	mov	r%1,b\n"
"	mov	r%2,a\n"
"} by {\n"
"	; Peehole 232.a simplified xch\n"
"	mov	a,r%1\n"
"	xch	a,r%2\n"
"	mov	r%1,a\n"
"}\n"
"\n"
"replace {\n"
"	mov	a,#%1\n"
"	mov	b,#%2\n"
"	mov	r%3,b\n"
"	mov	r%4,a\n"
"} by {\n"
"	; Peehole 232.b simplified xch\n"
"	mov	r%3,#%2\n"
"	mov	r%4,#%1\n"
"}\n"
"\n"
"replace {\n"
"	mov	dpl1,#%1\n"
"	mov	dph1,#(%1 >> 8)\n"
"	mov	dpx1,#(%1 >> 16)\n"
"} by {\n"
"	; Peephole 233 24 bit load of dptr1\n"
"	inc	dps\n"
"	mov	dptr,#%1\n"
"	dec	dps\n"
"}\n"
"\n"
"\n"
"replace {\n"
"        add  a,ar%1\n"
"} by {\n"
"        ; Peephole 236a\n"
"        add  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        addc  a,ar%1\n"
"} by {\n"
"        ; Peephole 236b\n"
"        addc  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        anl  a,ar%1\n"
"} by {\n"
"        ; Peephole 236c\n"
"        anl  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        dec  ar%1\n"
"} by {\n"
"        ; Peephole 236d\n"
"        dec  r%1\n"
"}\n"
"\n"
"replace {\n"
"        djnz  ar%1,%2\n"
"} by {\n"
"        ; Peephole 236e\n"
"        djnz  r%1,%2\n"
"}\n"
"\n"
"replace {\n"
"        inc  ar%1\n"
"} by {\n"
"        ; Peephole 236f\n"
"        inc  r%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  a,ar%1\n"
"} by {\n"
"        ; Peephole 236g\n"
"        mov  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        mov  ar%1,#%2\n"
"} by {\n"
"        ; Peephole 236h\n"
"        mov  r%1,#%2\n"
"}\n"
"\n"
"replace {\n"
"        mov  ar%1,a\n"
"} by {\n"
"        ; Peephole 236i\n"
"        mov  r%1,a\n"
"}\n"
"\n"
"replace {\n"
"        mov  ar%1,ar%2\n"
"} by {\n"
"        ; Peephole 236j\n"
"        mov  r%1,ar%2\n"
"}\n"
"\n"
"replace {\n"
"        orl  a,ar%1\n"
"} by {\n"
"        ; Peephole 236k\n"
"        orl  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        subb  a,ar%1\n"
"} by {\n"
"        ; Peephole 236l\n"
"        subb  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        xch  a,ar%1\n"
"} by {\n"
"        ; Peephole 236m\n"
"        xch  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        xrl  a,ar%1\n"
"} by {\n"
"        ; Peephole 236n\n"
"        xrl  a,r%1\n"
"}\n"
"\n"
"replace {\n"
"        sjmp    %1\n"
"%2:\n"
"        mov     %3,%4\n"
"%1:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 237a  removed sjmp to ret\n"
"        ret\n"
"%2:\n"
"        mov     %3,%4\n"
"%1:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        sjmp    %1\n"
"%2:\n"
"        mov     %3,%4\n"
"        mov     dpl,%5\n"
"        mov     dph,%6\n"
"%1:\n"
"        ret\n"
"} by {\n"
"        ; Peephole 237b  removed sjmp to ret\n"
"        ret\n"
"%2:\n"
"        mov     %3,%4\n"
"        mov     dpl,%5\n"
"        mov     dph,%6\n"
"%1:\n"
"        ret\n"
"}\n"
"\n"
"replace {\n"
"        mov     %1,%9\n"
"        mov     %2,%10\n"
"        mov     %3,%11\n"
"        mov     %4,%12\n"
"\n"
"        mov     %5,%13\n"
"        mov     %6,%14\n"
"        mov     %7,%15\n"
"        mov     %8,%16\n"
"\n"
"        mov     %9,%1\n"
"        mov     %10,%2\n"
"        mov     %11,%3\n"
"        mov     %12,%4\n"
"} by {\n"
"        mov     %1,%9\n"
"        mov     %2,%10\n"
"        mov     %3,%11\n"
"        mov     %4,%12\n"
"\n"
"        mov     %5,%13\n"
"        mov     %6,%14\n"
"        mov     %7,%15\n"
"        mov     %8,%16\n"
"        ;       Peephole 238.a  removed 4 redundant moves\n"
"} if notSame(%1 %2 %3 %4 %5 %6 %7 %8)\n"
"\n"
"replace {\n"
"        mov     %1,%5\n"
"        mov     %2,%6\n"
"        mov     %3,%7\n"
"        mov     %4,%8\n"
"\n"
"        mov     %5,%1\n"
"        mov     %6,%2\n"
"        mov     %7,%3\n"
"} by {\n"
"        mov     %1,%5\n"
"        mov     %2,%6\n"
"        mov     %3,%7\n"
"        mov     %4,%8\n"
"        ;       Peephole 238.b  removed 3 redundant moves\n"
"} if notSame(%1 %2 %3 %4 %5 %6 %7)\n"
"\n"
"replace {\n"
"        mov     %1,%5\n"
"        mov     %2,%6\n"
"\n"
"        mov     %3,%7\n"
"        mov     %4,%8\n"
"\n"
"        mov     %5,%1\n"
"        mov     %6,%2\n"
"} by {\n"
"        mov     %1,%5\n"
"        mov     %2,%6\n"
"\n"
"        mov     %3,%7\n"
"        mov     %4,%8\n"
"        ;       Peephole 238.c  removed 2 redundant moves\n"
"} if notSame(%1 %2 %3 %4)\n"
"\n"
"replace {\n"
"        mov     %1,%4\n"
"        mov     %2,%5\n"
"        mov     %3,%6\n"
"\n"
"        mov     %4,%1\n"
"        mov     %5,%2\n"
"        mov     %6,%3\n"
"} by {\n"
"        mov     %1,%4\n"
"        mov     %2,%5\n"
"        mov     %3,%6\n"
"        ;       Peephole 238.d  removed 3 redundant moves\n"
"} if notSame(%1 %2 %3 %4 %5 %6)\n"
"\n"
"replace {\n"
"        mov     r%1,acc\n"
"} by {\n"
"        ;       Peephole 239    used a instead of acc\n"
"        mov     r%1,a\n"
"}\n"
"\n"
"replace restart {\n"
"	mov	a,%1\n"
"	addc	a,#0x00\n"
"} by {\n"
"	;	Peephole 240    use clr instead of addc a,#0\n"
"	clr	a\n"
"	addc	a,%1\n"
"}\n"
"\n"
"replace {\n"
"        cjne    r%1,#%2,%3\n"
"        cjne    r%4,#%5,%3\n"
"        cjne    r%6,#%7,%3\n"
"        cjne    r%8,#%9,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %10\n"
"%3:\n"
"        clr     a\n"
"%10:\n"
"} by {\n"
"        ;       Peephole 241.a  optimized compare\n"
"        clr     a\n"
"        cjne    r%1,#%2,%3\n"
"        cjne    r%4,#%5,%3\n"
"        cjne    r%6,#%7,%3\n"
"        cjne    r%8,#%9,%3\n"
"        inc     a\n"
"%3:\n"
"%10:\n"
"}\n"
"\n"
"replace {\n"
"        cjne    r%1,#%2,%3\n"
"        cjne    r%4,#%5,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %6\n"
"%3:\n"
"        clr     a\n"
"%6:\n"
"} by {\n"
"        ;       Peephole 241.b  optimized compare\n"
"        clr     a\n"
"        cjne    r%1,#%2,%3\n"
"        cjne    r%4,#%5,%3\n"
"        inc     a\n"
"%3:\n"
"%6:\n"
"}\n"
"\n"
"replace {\n"
"        cjne    r%1,#%2,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %4\n"
"%3:\n"
"        clr     a\n"
"%4:\n"
"} by {\n"
"        ;       Peephole 241.c  optimized compare\n"
"        clr     a\n"
"        cjne    r%1,#%2,%3\n"
"        inc     a\n"
"%3:\n"
"%4:\n"
"}\n"
"\n"
"replace {\n"
"        cjne    @r%1,#%2,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%4,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%5,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%6,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %7\n"
"%3:\n"
"        clr     a\n"
"%7:\n"
"} by {\n"
"        ;       Peephole 241.d  optimized compare\n"
"        clr     a\n"
"        cjne    @r%1,#%2,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%4,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%5,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%6,%3\n"
"        inc     a\n"
"%3:\n"
"%7:\n"
"}\n"
"\n"
"replace {\n"
"        cjne    @r%1,#%2,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%4,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %7\n"
"%3:\n"
"        clr     a\n"
"%7:\n"
"} by {\n"
"        ;       Peephole 241.e  optimized compare\n"
"        clr     a\n"
"        cjne    @r%1,#%2,%3\n"
"	inc	r%1\n"
"        cjne    @r%1,#%4,%3\n"
"        inc     a\n"
"%3:\n"
"%7:\n"
"}\n"
"\n"
"replace {\n"
"        cjne    @r%1,#%2,%3\n"
"        mov     a,#0x01\n"
"        sjmp    %7\n"
"%3:\n"
"        clr     a\n"
"%7:\n"
"} by {\n"
"        ;       Peephole 241.f  optimized compare\n"
"        clr     a\n"
"        cjne    @r%1,#%2,%3\n"
"        inc     a\n"
"%3:\n"
"%7:\n"
"}\n"
"\n"
"replace {\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"%1:\n"
"        jz      %4\n"
"} by {\n"
"        ;       Peephole 242.a  avoided branch jnz to jz\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"        jz      %4\n"
"%1:\n"
"} if labelRefCount %1 1\n"
"\n"
"replace {\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"        orl     a,%5\n"
"%1:\n"
"        jz      %4\n"
"} by {\n"
"        ;       Peephole 242.b  avoided branch jnz to jz\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"        orl     a,%5\n"
"        jz      %4\n"
"%1:\n"
"} if labelRefCount %1 1\n"
"\n"
"replace {\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"        orl     a,%5\n"
"        orl     a,%6\n"
"        orl     a,%7\n"
"%1:\n"
"        jz      %4\n"
"} by {\n"
"        ;       Peephole 242.c  avoided branch jnz to jz\n"
"        jnz     %1\n"
"        mov     %2,%3\n"
"        orl     a,%5\n"
"        orl     a,%6\n"
"        orl     a,%7\n"
"        jz      %4\n"
"%1:\n"
"} if labelRefCount %1 1\n"
"\n"
"replace {\n"
"        jnz     %1\n"
"%1:\n"
"} by {\n"
"        ;       Peephole 243a    jump optimization\n"
"} if labelRefCount %1 1\n"
"\n"
"replace {\n"
"        jz      %1\n"
"%1:\n"
"} by {\n"
"        ;       Peephole 243b    jump optimization\n"
"} if labelRefCount %1 1\n"
"\n"
"\n"
"replace {\n"
"        jnb     %1,%2\n"
"%3:\n"
"        clr     %1\n"
"} by {\n"
"        ;       Peephole 244.a  using atomic test and clear\n"
"        jbc     %1,%3\n"
"        sjmp    %2\n"
"%3:\n"
"} if labelRefCount %3 0\n"
"\n"
"replace {\n"
"        jb      %1,%2\n"
"        ljmp    %3\n"
"%2:\n"
"        clr     %1\n"
"} by {\n"
"        ;       Peephole 244.b  using atomic test and clear\n"
"        jbc     %1,%2\n"
"        ljmp    %3\n"
"%2:\n"
"} if labelRefCount %2 1\n"
"\n"
};

#define OPTION_STACK_8BIT       "--stack-8bit"
#define OPTION_FLAT24_MODEL     "--model-flat24"
#define OPTION_STACK_SIZE       "--stack-size"

static OPTION _ds390_options[] =
  {
    { 0, OPTION_FLAT24_MODEL,   NULL, "use the flat24 model for the ds390 (default)" },
    { 0, OPTION_STACK_8BIT,     NULL, "use the 8bit stack for the ds390 (not supported yet)" },
    { 0, OPTION_STACK_SIZE,     &options.stack_size, "Tells the linker to allocate this space for stack", CLAT_INTEGER },
    { 0, "--pack-iram",         NULL, "Tells the linker to pack variables in internal ram (default)"},
    { 0, "--no-pack-iram",      &options.no_pack_iram, "Deprecated: Tells the linker not to pack variables in internal ram"},
    { 0, "--stack-10bit",       &options.stack10bit, "use the 10bit stack for ds390 (default)" },
    { 0, "--use-accelerator",   &options.useAccelerator, "generate code for ds390 arithmetic accelerator"},
    { 0, "--protect-sp-update", &options.protect_sp_update, "will disable interrupts during ESP:SP updates"},
    { 0, "--parms-in-bank1",    &options.parms_in_bank1, "use Bank1 for parameter passing"},
    { 0, NULL }
  };


/* list of key words used by msc51 */
static char *_ds390_keywords[] =
{
  "at",
  "bit",
  "code",
  "critical",
  "data",
  "far",
  "idata",
  "interrupt",
  "near",
  "pdata",
  "reentrant",
  "sfr",
  "sfr16",
  "sfr32",
  "sbit",
  "using",
  "xdata",
  "_data",
  "_code",
  "_generic",
  "_near",
  "_xdata",
  "_pdata",
  "_idata",
  "_naked",
  NULL
};

static builtins __ds390_builtins[] = {
    { "__builtin_memcpy_x2x","v",3,{"cx*","cx*","i"}}, /* void __builtin_memcpy_x2x (xdata char *,xdata char *,int) */
    { "__builtin_memcpy_c2x","v",3,{"cx*","cp*","i"}}, /* void __builtin_memcpy_c2x (xdata char *,code  char *,int) */
    { "__builtin_memset_x","v",3,{"cx*","c","i"}},     /* void __builtin_memset     (xdata char *,char,int)         */
    /* __builtin_inp - used to read from a memory mapped port, increment first pointer */
    { "__builtin_inp","v",3,{"cx*","cx*","i"}},        /* void __builtin_inp        (xdata char *,xdata char *,int) */
    /* __builtin_inp - used to write to a memory mapped port, increment first pointer */
    { "__builtin_outp","v",3,{"cx*","cx*","i"}},       /* void __builtin_outp       (xdata char *,xdata char *,int) */
    { "__builtin_swapw","us",1,{"us"}},                /* unsigned short __builtin_swapw (unsigned short) */
    { "__builtin_memcmp_x2x","c",3,{"cx*","cx*","i"}}, /* void __builtin_memcmp_x2x (xdata char *,xdata char *,int) */
    { "__builtin_memcmp_c2x","c",3,{"cx*","cp*","i"}}, /* void __builtin_memcmp_c2x (xdata char *,code  char *,int) */
    { NULL , NULL,0, {NULL}}                           /* mark end of table */
};
void ds390_assignRegisters (ebbIndex * ebbi);

static int regParmFlg = 0;      /* determine if we can register a parameter */

static void
_ds390_init (void)
{
  asm_addTree (&asm_asxxxx_mapping);
}

static void
_ds390_reset_regparm (void)
{
  regParmFlg = 0;
}

static int
_ds390_regparm (sym_link * l, bool reentrant)
{
    if (IS_SPEC(l) && (SPEC_NOUN(l) == V_BIT))
        return 0;
    if (options.parms_in_bank1 == 0) {
        /* simple can pass only the first parameter in a register */
        if (regParmFlg)
            return 0;

        regParmFlg = 1;
        return 1;
    } else {
        int size = getSize(l);
        int remain ;

        /* first one goes the usual way to DPTR */
        if (regParmFlg == 0) {
            regParmFlg += 4 ;
            return 1;
        }
        /* second one onwards goes to RB1_0 thru RB1_7 */
        remain = regParmFlg - 4;
        if (size > (8 - remain)) {
            regParmFlg = 12 ;
            return 0;
        }
        regParmFlg += size ;
        return regParmFlg - size + 1;
    }
}

static bool
_ds390_parseOptions (int *pargc, char **argv, int *i)
{
  /* TODO: allow port-specific command line options to specify
   * segment names here.
   */
  if (!strcmp (argv[*i], OPTION_STACK_8BIT))
    {
      options.stack10bit = 0;
      return TRUE;
    }
  else if (!strcmp (argv[*i], OPTION_FLAT24_MODEL))
    {
      options.model = MODEL_FLAT24;
      return TRUE;
    }
  return FALSE;
}

static void
_ds390_finaliseOptions (void)
{
  if (options.noXinitOpt)
    {
      port->genXINIT=0;
    }

  /* Hack-o-matic: if we are using the flat24 model,
   * adjust pointer sizes.
   */
  if (options.model != MODEL_FLAT24)
    {
      fprintf (stderr,
               "*** warning: ds390 port small and large model experimental.\n");
      if (options.model == MODEL_LARGE)
        {
          port->mem.default_local_map = xdata;
          port->mem.default_globl_map = xdata;
        }
      else
        {
          port->mem.default_local_map = data;
          port->mem.default_globl_map = data;
        }
    }
  else
    {
      port->s.fptr_size = 3;
      port->s.gptr_size = 4;

      port->stack.isr_overhead += 2;      /* Will save dpx on ISR entry. */

      port->stack.call_overhead += 2;     /* This acounts for the extra byte
                                           * of return addres on the stack.
                                           * but is ugly. There must be a
                                           * better way.
                                           */

      port->mem.default_local_map = xdata;
      port->mem.default_globl_map = xdata;

      if (!options.stack10bit)
        {
          fprintf (stderr,
                   "*** error: ds390 port only supports the 10 bit stack mode.\n");
        }
      else
        {
          if (!options.stack_loc) options.stack_loc = 0x400008;
        }

      /* generate native code 16*16 mul/div */
      if (options.useAccelerator)
        port->support.muldiv=2;
      else
        port->support.muldiv=1;

      /* Fixup the memory map for the stack; it is now in
       * far space and requires an FPOINTER to access it.
       */
      istack->fmap = 1;
      istack->ptrType = FPOINTER;

    }  /* MODEL_FLAT24 */
}

static void
_ds390_setDefaultOptions (void)
{
  options.model=MODEL_FLAT24;
  options.stack10bit=1;
}

static const char *
_ds390_getRegName (const struct reg_info *reg)
{
  if (reg)
    return reg->name;
  return "err";
}

extern char * iComments2;

static void
_ds390_genAssemblerPreamble (FILE * of)
{
      fputs (iComments2, of);
      fputs ("; CPU specific extensions\n",of);
      fputs (iComments2, of);

      fputs ("\t.DS80C390\n", of);

      if (options.model == MODEL_FLAT24)
        fputs ("\t.amode\t2\t; 24 bit flat addressing\n", of);

      fputs ("dpl1\t=\t0x84\n", of);
      fputs ("dph1\t=\t0x85\n", of);
      fputs ("dps\t=\t0x86\n", of);
      fputs ("dpx\t=\t0x93\n", of);
      fputs ("dpx1\t=\t0x95\n", of);
      fputs ("esp\t=\t0x9B\n", of);
      fputs ("ap\t=\t0x9C\n", of);
      fputs ("_ap\t=\t0x9C\n", of);
      fputs ("mcnt0\t=\t0xD1\n", of);
      fputs ("mcnt1\t=\t0xD2\n", of);
      fputs ("ma\t=\t0xD3\n", of);
      fputs ("mb\t=\t0xD4\n", of);
      fputs ("mc\t=\t0xD5\n", of);
      fputs ("F1\t=\t0xD1\t; user flag\n", of);
      if (options.parms_in_bank1) {
          int i ;
          for (i=0; i < 8 ; i++ )
              fprintf (of,"b1_%d\t=\t0x%02X\n",i,8+i);
      }
}

/* Generate interrupt vector table. */
static int
_ds390_genIVT (struct dbuf_s * oBuf, symbol ** interrupts, int maxInterrupts)
{
  int i;

  if (options.model != MODEL_FLAT24)
    {
      dbuf_printf (oBuf, "\tljmp\t__sdcc_gsinit_startup\n");

      /* now for the other interrupts */
      for (i = 0; i < maxInterrupts; i++)
        {
          if (interrupts[i])
            {
              dbuf_printf (oBuf, "\tljmp\t%s\n", interrupts[i]->rname);
              if ( i != maxInterrupts - 1 )
                dbuf_printf (oBuf, "\t.ds\t5\n");
            }
          else
            {
              dbuf_printf (oBuf, "\treti\n");
              if ( i != maxInterrupts - 1 )
                dbuf_printf (oBuf, "\t.ds\t7\n");
            }
        }
      return TRUE;
    }

  dbuf_printf (oBuf, "\t.amode\t0\t; 16 bit addressing\n");
  dbuf_printf (oBuf, "\tljmp\t__reset_vect\n");
  dbuf_printf (oBuf, "\t.amode\t2\t; 24 bit flat addressing\n");

  /* now for the other interrupts */
  for (i = 0; i < maxInterrupts; i++)
    {
      if (interrupts[i])
        {
          dbuf_printf (oBuf, "\tljmp\t%s\n\t.ds\t4\n", interrupts[i]->rname);
        }
      else
        {
          dbuf_printf (oBuf, "\treti\n\t.ds\t7\n");
        }
    }

  dbuf_printf (oBuf, "\t.amode\t0\t; 16 bit addressing\n");
  dbuf_printf (oBuf, "__reset_vect:\n\tljmp\t__sdcc_gsinit_startup\n");
  dbuf_printf (oBuf, "\t.amode\t2\t; 24 bit flat addressing\n");

  return TRUE;
}

static void
_ds390_genInitStartup (FILE *of)
{
  fprintf (of, "__sdcc_gsinit_startup:\n");
  /* if external stack is specified then the
     higher order byte of the xdatalocation is
     going into P2 and the lower order going into
     spx */
  if (options.useXstack)
    {
      fprintf (of, "\tmov\tP2,#0x%02x\n",
               (((unsigned int) options.xdata_loc) >> 8) & 0xff);
      fprintf (of, "\tmov\t_spx,#0x%02x\n",
               (unsigned int) options.xdata_loc & 0xff);
    }

  // This should probably be a port option, but I'm being lazy.
  // on the 400, the firmware boot loader gives us a valid stack
  // (see '400 data sheet pg. 85 (TINI400 ROM Initialization code)
  if (!TARGET_IS_DS400)
    {
      /* initialise the stack pointer.  JCF: sdld takes care of the location */
      fprintf (of, "\tmov\tsp,#__start__stack - 1\n");     /* MOF */
    }

  if ((options.model == MODEL_FLAT24) && TARGET_IS_DS390)
    {
      fputs ("\t.amode\t0\t; 16 bit addressing\n", of);
      fprintf (of, "\tlcall\t__sdcc_external_startup\n");
      fputs ("\t.amode\t2\t; 24 bit flat addressing\n", of);
    }
  else
    {
      fprintf (of, "\tlcall\t__sdcc_external_startup\n");
    }
  fprintf (of, "\tmov\ta,dpl\n");
  fprintf (of, "\tjz\t__sdcc_init_data\n");
  fprintf (of, "\tljmp\t__sdcc_program_startup\n");
  fprintf (of, "__sdcc_init_data:\n");

  // if the port can copy the XINIT segment to XISEG
  if (port->genXINIT)
    {
      port->genXINIT(of);
    }
}

/* Generate code to copy XINIT to XISEG */
static void _ds390_genXINIT (FILE * of) {
  fprintf (of, ";       _ds390_genXINIT() start\n");
  fprintf (of, "        mov     a,#l_XINIT\n");
  fprintf (of, "        orl     a,#l_XINIT>>8\n");
  fprintf (of, "        jz      00003$\n");
  fprintf (of, "        mov     a,#s_XINIT\n");
  fprintf (of, "        add     a,#l_XINIT\n");
  fprintf (of, "        mov     r1,a\n");
  fprintf (of, "        mov     a,#s_XINIT>>8\n");
  fprintf (of, "        addc    a,#l_XINIT>>8\n");
  fprintf (of, "        mov     r2,a\n");
  fprintf (of, "        mov     dptr,#s_XINIT\n");
  fprintf (of, "        mov     dps,#0x21\n");
  fprintf (of, "        mov     dptr,#s_XISEG\n");
  fprintf (of, "00001$: clr     a\n");
  fprintf (of, "        movc    a,@a+dptr\n");
  fprintf (of, "        movx    @dptr,a\n");
  fprintf (of, "        inc     dptr\n");
  fprintf (of, "        inc     dptr\n");
  fprintf (of, "00002$: mov     a,dpl\n");
  fprintf (of, "        cjne    a,ar1,00001$\n");
  fprintf (of, "        mov     a,dph\n");
  fprintf (of, "        cjne    a,ar2,00001$\n");
  fprintf (of, "        mov     dps,#0\n");
  fprintf (of, "00003$:\n");
  fprintf (of, ";       _ds390_genXINIT() end\n");
}

/* Do CSE estimation */
static bool cseCostEstimation (iCode *ic, iCode *pdic)
{
    operand *result = IC_RESULT(ic);
    //operand *right  = IC_RIGHT(ic);
    //operand *left   = IC_LEFT(ic);
    sym_link *result_type = operandType(result);
    //sym_link *right_type  = (right ? operandType(right) : 0);
    //sym_link *left_type   = (left  ? operandType(left)  : 0);

    /* if it is a pointer then return ok for now */
    if (IC_RESULT(ic) && IS_PTR(result_type)) return 1;

    /* if bitwise | add & subtract then no since mcs51 is pretty good at it
       so we will cse only if they are local (i.e. both ic & pdic belong to
       the same basic block */
    if (IS_BITWISE_OP(ic) || ic->op == '+' || ic->op == '-') {
        /* then if they are the same Basic block then ok */
        if (ic->eBBlockNum == pdic->eBBlockNum) return 1;
        else return 0;
    }

    /* for others it is cheaper to do the cse */
    return 1;
}

bool _ds390_nativeMulCheck(iCode *ic, sym_link *left, sym_link *right)
{
    return FALSE; // #STUB
}

/* Indicate which extended bit operations this port supports */
static bool
hasExtBitOp (int op, int size)
{
  if (op == RRC
      || op == RLC
      || op == GETHBIT
      || (op == SWAP && size <= 2)
     )
    return TRUE;
  else
    return FALSE;
}

/* Indicate the expense of an access to an output storage class */
static int
oclsExpense (struct memmap *oclass)
{
  if (IN_FARSPACE(oclass))
    return 1;

  return 0;
}

static int
instructionSize(char *inst, char *op1, char *op2)
{
  int isflat24 = (options.model == MODEL_FLAT24);

  #define ISINST(s) (strncmp(inst, (s), sizeof(s)-1) == 0)
  #define IS_A(s) (*(s) == 'a' && *(s+1) == '\0')
  #define IS_C(s) (*(s) == 'c' && *(s+1) == '\0')
  #define IS_Rn(s) (*(s) == 'r' && *(s+1) >= '0' && *(s+1) <= '7')
  #define IS_atRi(s) (*(s) == '@' && *(s+1) == 'r')

  /* Based on the current (2003-08-22) code generation for the
     small library, the top instruction probability is:

       57% mov/movx/movc
        6% push
        6% pop
        4% inc
        4% lcall
        4% add
        3% clr
        2% subb
  */
  /* mov, push, & pop are the 69% of the cases. Check them first! */
  if (ISINST ("mov"))
    {
      if (*(inst+3)=='x') return 1; /* movx */
      if (*(inst+3)=='c') return 1; /* movc */
      if (IS_C (op1) || IS_C (op2)) return 2;
      if (IS_A (op1))
        {
          if (IS_Rn (op2) || IS_atRi (op2)) return 1;
          return 2;
        }
      if (IS_Rn(op1) || IS_atRi(op1))
        {
          if (IS_A(op2)) return 1;
          return 2;
        }
      if (strcmp (op1, "dptr") == 0) return 3+isflat24;
      if (IS_A (op2) || IS_Rn (op2) || IS_atRi (op2)) return 2;
      return 3;
    }

  if (ISINST ("push")) return 2;
  if (ISINST ("pop")) return 2;

  if (ISINST ("lcall")) return 3+isflat24;
  if (ISINST ("ret")) return 1;
  if (ISINST ("ljmp")) return 3+isflat24;
  if (ISINST ("sjmp")) return 2;
  if (ISINST ("rlc")) return 1;
  if (ISINST ("rrc")) return 1;
  if (ISINST ("rl")) return 1;
  if (ISINST ("rr")) return 1;
  if (ISINST ("swap")) return 1;
  if (ISINST ("jc")) return 2;
  if (ISINST ("jnc")) return 2;
  if (ISINST ("jb")) return 3;
  if (ISINST ("jnb")) return 3;
  if (ISINST ("jbc")) return 3;
  if (ISINST ("jmp")) return 1; // always jmp @a+dptr
  if (ISINST ("jz")) return 2;
  if (ISINST ("jnz")) return 2;
  if (ISINST ("cjne")) return 3;
  if (ISINST ("mul")) return 1;
  if (ISINST ("div")) return 1;
  if (ISINST ("da")) return 1;
  if (ISINST ("xchd")) return 1;
  if (ISINST ("reti")) return 1;
  if (ISINST ("nop")) return 1;
  if (ISINST ("acall")) return 2+isflat24;
  if (ISINST ("ajmp")) return 2+isflat24;


  if (ISINST ("add") || ISINST ("addc") || ISINST ("subb") || ISINST ("xch"))
    {
      if (IS_Rn(op2) || IS_atRi(op2)) return 1;
      return 2;
    }
  if (ISINST ("inc") || ISINST ("dec"))
    {
      if (IS_A(op1) || IS_Rn(op1) || IS_atRi(op1)) return 1;
      if (strcmp(op1, "dptr") == 0) return 1;
      return 2;
    }
  if (ISINST ("anl") || ISINST ("orl") || ISINST ("xrl"))
    {
      if (IS_C(op1)) return 2;
      if (IS_A(op1))
        {
          if (IS_Rn(op2) || IS_atRi(op2)) return 1;
          return 2;
        }
      else
        {
          if (IS_A(op2)) return 2;
          return 3;
        }
    }
  if (ISINST ("clr") || ISINST ("setb") || ISINST ("cpl"))
    {
      if (IS_A(op1) || IS_C(op1)) return 1;
      return 2;
    }
  if (ISINST ("djnz"))
    {
      if (IS_Rn(op1)) return 2;
      return 3;
    }

  /* If the instruction is unrecognized, we shouldn't try to optimize. */
  /* Return a large value to discourage optimization.                  */
  return 999;
}

asmLineNode *
ds390newAsmLineNode (int currentDPS)
{
  asmLineNode *aln;

  aln = Safe_alloc ( sizeof (asmLineNode));
  aln->size = 0;
  aln->regsRead = NULL;
  aln->regsWritten = NULL;
  aln->initialized = 0;
  aln->currentDPS = currentDPS;

  return aln;
}


typedef struct ds390operanddata
  {
    char name[6];
    int regIdx1;
    int regIdx2;
  }
ds390operanddata;

static ds390operanddata ds390operandDataTable[] =
  {
    {"_ap", AP_IDX, -1},
    {"a", A_IDX, -1},
    {"ab", A_IDX, B_IDX},
    {"ac", CND_IDX, -1},
    {"ap", AP_IDX, -1},
    {"acc", A_IDX, -1},
    {"ar0", R0_IDX, -1},
    {"ar1", R1_IDX, -1},
    {"ar2", R2_IDX, -1},
    {"ar3", R3_IDX, -1},
    {"ar4", R4_IDX, -1},
    {"ar5", R5_IDX, -1},
    {"ar6", R6_IDX, -1},
    {"ar7", R7_IDX, -1},
    {"b", B_IDX, -1},
    {"c", CND_IDX, -1},
    {"cy", CND_IDX, -1},
    {"dph", DPH_IDX, -1},
    {"dph0", DPH_IDX, -1},
    {"dph1", DPH1_IDX, -1},
    {"dpl", DPL_IDX, -1},
    {"dpl0", DPL_IDX, -1},
    {"dpl1", DPL1_IDX, -1},
/*  {"dptr", DPL_IDX, DPH_IDX}, */ /* dptr is special, based on currentDPS */
    {"dps", DPS_IDX, -1},
    {"dpx", DPX_IDX, -1},
    {"dpx0", DPX_IDX, -1},
    {"dpx1", DPX1_IDX, -1},
    {"f0", CND_IDX, -1},
    {"f1", CND_IDX, -1},
    {"ov", CND_IDX, -1},
    {"p", CND_IDX, -1},
    {"psw", CND_IDX, -1},
    {"r0", R0_IDX, -1},
    {"r1", R1_IDX, -1},
    {"r2", R2_IDX, -1},
    {"r3", R3_IDX, -1},
    {"r4", R4_IDX, -1},
    {"r5", R5_IDX, -1},
    {"r6", R6_IDX, -1},
    {"r7", R7_IDX, -1},
  };

static int
ds390operandCompare (const void *key, const void *member)
{
  return strcmp((const char *)key, ((ds390operanddata *)member)->name);
}

static void
updateOpRW (asmLineNode *aln, char *op, char *optype, int currentDPS)
{
  ds390operanddata *opdat;
  char *dot;
  int regIdx1 = -1;
  int regIdx2 = -1;
  int regIdx3 = -1;

  dot = strchr(op, '.');
  if (dot)
    *dot = '\0';

  opdat = bsearch (op, ds390operandDataTable,
                   sizeof(ds390operandDataTable)/sizeof(ds390operanddata),
                   sizeof(ds390operanddata), ds390operandCompare);

  if (opdat)
    {
      regIdx1 = opdat->regIdx1;
      regIdx2 = opdat->regIdx2;
    }
  if (!strcmp(op, "dptr"))
    {
      if (!currentDPS)
        {
          regIdx1 = DPL_IDX;
          regIdx2 = DPH_IDX;
          regIdx3 = DPX_IDX;
        }
      else
        {
          regIdx1 = DPL1_IDX;
          regIdx2 = DPH1_IDX;
          regIdx3 = DPX1_IDX;
        }
    }

  if (strchr(optype,'r'))
    {
      if (regIdx1 >= 0)
        aln->regsRead = bitVectSetBit (aln->regsRead, regIdx1);
      if (regIdx2 >= 0)
        aln->regsRead = bitVectSetBit (aln->regsRead, regIdx2);
      if (regIdx3 >= 0)
        aln->regsRead = bitVectSetBit (aln->regsRead, regIdx3);
    }
  if (strchr(optype,'w'))
    {
      if (regIdx1 >= 0)
        aln->regsWritten = bitVectSetBit (aln->regsWritten, regIdx1);
      if (regIdx2 >= 0)
        aln->regsWritten = bitVectSetBit (aln->regsWritten, regIdx2);
      if (regIdx3 >= 0)
        aln->regsWritten = bitVectSetBit (aln->regsWritten, regIdx3);
    }
  if (op[0] == '@')
    {
      if (!strcmp(op, "@r0"))
        aln->regsRead = bitVectSetBit (aln->regsRead, R0_IDX);
      if (!strcmp(op, "@r1"))
        aln->regsRead = bitVectSetBit (aln->regsRead, R1_IDX);
      if (strstr(op, "dptr"))
        {
          if (!currentDPS)
            {
              aln->regsRead = bitVectSetBit (aln->regsRead, DPL_IDX);
              aln->regsRead = bitVectSetBit (aln->regsRead, DPH_IDX);
              aln->regsRead = bitVectSetBit (aln->regsRead, DPX_IDX);
            }
          else
            {
              aln->regsRead = bitVectSetBit (aln->regsRead, DPL1_IDX);
              aln->regsRead = bitVectSetBit (aln->regsRead, DPH1_IDX);
              aln->regsRead = bitVectSetBit (aln->regsRead, DPX1_IDX);
            }
        }
      if (strstr(op, "a+"))
        aln->regsRead = bitVectSetBit (aln->regsRead, A_IDX);
    }
}

typedef struct ds390opcodedata
  {
    char name[6];
    char class[3];
    char pswtype[3];
    char op1type[3];
    char op2type[3];
  }
ds390opcodedata;

static ds390opcodedata ds390opcodeDataTable[] =
  {
    {"acall","j", "",   "",   ""},
    {"add",  "",  "w",  "rw", "r"},
    {"addc", "",  "rw", "rw", "r"},
    {"ajmp", "j", "",   "",   ""},
    {"anl",  "",  "",   "rw", "r"},
    {"cjne", "j", "w",  "r",  "r"},
    {"clr",  "",  "",   "w",  ""},
    {"cpl",  "",  "",   "rw", ""},
    {"da",   "",  "rw", "rw", ""},
    {"dec",  "",  "",   "rw", ""},
    {"div",  "",  "w",  "rw", ""},
    {"djnz", "j", "",  "rw",  ""},
    {"inc",  "",  "",   "rw", ""},
    {"jb",   "j", "",   "r",  ""},
    {"jbc",  "j", "",  "rw",  ""},
    {"jc",   "j", "",   "",   ""},
    {"jmp",  "j", "",  "",    ""},
    {"jnb",  "j", "",   "r",  ""},
    {"jnc",  "j", "",   "",   ""},
    {"jnz",  "j", "",  "",    ""},
    {"jz",   "j", "",  "",    ""},
    {"lcall","j", "",   "",   ""},
    {"ljmp", "j", "",   "",   ""},
    {"mov",  "",  "",   "w",  "r"},
    {"movc", "",  "",   "w",  "r"},
    {"movx", "",  "",   "w",  "r"},
    {"mul",  "",  "w",  "rw", ""},
    {"nop",  "",  "",   "",   ""},
    {"orl",  "",  "",   "rw", "r"},
    {"pop",  "",  "",   "w",  ""},
    {"push", "",  "",   "r",  ""},
    {"ret",  "j", "",   "",   ""},
    {"reti", "j", "",   "",   ""},
    {"rl",   "",  "",   "rw", ""},
    {"rlc",  "",  "rw", "rw", ""},
    {"rr",   "",  "",   "rw", ""},
    {"rrc",  "",  "rw", "rw", ""},
    {"setb", "",  "",   "w",  ""},
    {"sjmp", "j", "",   "",   ""},
    {"subb", "",  "rw", "rw", "r"},
    {"swap", "",  "",   "rw", ""},
    {"xch",  "",  "",   "rw", "rw"},
    {"xchd", "",  "",   "rw", "rw"},
    {"xrl",  "",  "",   "rw", "r"},
  };

static int
ds390opcodeCompare (const void *key, const void *member)
{
  return strcmp((const char *)key, ((ds390opcodedata *)member)->name);
}

static asmLineNode *
asmLineNodeFromLineNode (lineNode *ln, int currentDPS)
{
  asmLineNode *aln = ds390newAsmLineNode(currentDPS);
  char *op, op1[256], op2[256];
  int opsize;
  const char *p;
  char inst[8];
  ds390opcodedata *opdat;

  aln->initialized = 1;

  p = ln->line;

  while (*p && isspace(*p)) p++;
  for (op = inst, opsize=1; *p; p++)
    {
      if (isspace(*p) || *p == ';' || *p == ':' || *p == '=')
        break;
      else
        if (opsize < sizeof(inst))
          *op++ = tolower(*p), opsize++;
    }
  *op = '\0';

  if (*p == ';' || *p == ':' || *p == '=')
    return aln;

  while (*p && isspace(*p)) p++;
  if (*p == '=')
    return aln;

  for (op = op1, opsize=1; *p && *p != ','; p++)
    {
      if (!isspace(*p) && opsize < sizeof(op1))
        *op++ = tolower(*p), opsize++;
    }
  *op = '\0';

  if (*p == ',') p++;
  for (op = op2, opsize=1; *p && *p != ','; p++)
    {
      if (!isspace(*p) && opsize < sizeof(op2))
        *op++ = tolower(*p), opsize++;
    }
  *op = '\0';

  aln->size = instructionSize(inst, op1, op2);

  aln->regsRead = newBitVect (END_IDX);
  aln->regsWritten = newBitVect (END_IDX);

  opdat = bsearch (inst, ds390opcodeDataTable,
                   sizeof(ds390opcodeDataTable)/sizeof(ds390opcodedata),
                   sizeof(ds390opcodedata), ds390opcodeCompare);

  if (opdat)
    {
      updateOpRW (aln, op1, opdat->op1type, currentDPS);
      updateOpRW (aln, op2, opdat->op2type, currentDPS);
      if (strchr(opdat->pswtype,'r'))
        aln->regsRead = bitVectSetBit (aln->regsRead, CND_IDX);
      if (strchr(opdat->pswtype,'w'))
        aln->regsWritten = bitVectSetBit (aln->regsWritten, CND_IDX);
    }

  return aln;
}

static void
initializeAsmLineNode (lineNode *line)
{
  if (!line->aln)
    line->aln = (asmLineNodeBase *) asmLineNodeFromLineNode (line, 0);
  else if (line->aln && !((asmLineNode *)line->aln)->initialized)
    {
      int currentDPS = ((asmLineNode *)line->aln)->currentDPS;
      free(line->aln);
      line->aln = (asmLineNodeBase *) asmLineNodeFromLineNode (line, currentDPS);
    }
}

static int
getInstructionSize (lineNode *line)
{
  initializeAsmLineNode (line);
  return line->aln->size;
}

static bitVect *
getRegsRead (lineNode *line)
{
  initializeAsmLineNode (line);
  return line->aln->regsRead;
}

static bitVect *
getRegsWritten (lineNode *line)
{
  initializeAsmLineNode (line);
  return line->aln->regsWritten;
}

static const char *
get_model (void)
{
  switch (options.model)
    {
    case MODEL_SMALL:
      if (options.stackAuto)
        return "small-stack-auto";
      else
        return "small";

    case MODEL_LARGE:
      if (options.stackAuto)
        return "large-stack-auto";
      else
        return "large";

    case MODEL_FLAT24:
        return port->target;

    default:
      werror (W_UNKNOWN_MODEL, __FILE__, __LINE__);
      return "unknown";
    }
}

/** $1 is always the basename.
    $2 is always the output file.
    $3 varies
    $l is the list of extra options that should be there somewhere...
    MUST be terminated with a NULL.
*/
static const char *_linkCmd[] =
{
  "sdld", "-nf", "$1", NULL
};

/* $3 is replaced by assembler.debug_opts resp. port->assembler.plain_opts */
static const char *_asmCmd[] =
{
  "sdas390", "$l", "$3", "$2", "$1.asm", NULL
};

static const char * const _libs_ds390[] = { STD_DS390_LIB, NULL, };

/* Globals */
PORT ds390_port =
{
  TARGET_ID_DS390,
  "ds390",
  "DS80C390",                   /* Target name */
  NULL,
  {
    glue,
    TRUE,                       /* Emit glue around main */
    MODEL_SMALL | MODEL_LARGE | MODEL_FLAT24,
    MODEL_SMALL,
    get_model,
  },
  {
    _asmCmd,
    NULL,
    "-plosgffwy",              /* Options with debug */
    "-plosgffw",                /* Options without debug */
    0,
    ".asm",
    NULL                        /* no do_assemble function */
  },
  {                             /* Linker */
    _linkCmd,
    NULL,
    NULL,
    ".rel",
    1,
    NULL,                       /* crt */
    _libs_ds390,                /* libs */
  },
  {
    _defaultRules,
    getInstructionSize,
    getRegsRead,
    getRegsWritten
  },
  {
        /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
    1, 2, 2, 4, 8, 1, 2, 3, 1, 4, 4
  },

  /* tags for generic pointers */
  { 0x00, 0x40, 0x60, 0x80 },           /* far, near, xstack, code */

  {
    "XSEG    (XDATA)",
    "STACK   (DATA)",
    "CSEG    (CODE)",
    "DSEG    (DATA)",
    "ISEG    (DATA)",
    "PSEG    (PAG,XDATA)",
    "XSEG    (XDATA)",
    "BSEG    (BIT)",
    "RSEG    (DATA)",
    "GSINIT  (CODE)",
    "OSEG    (OVR,DATA)",
    "GSFINAL (CODE)",
    "HOME    (CODE)",
    "XISEG   (XDATA)",          // initialized xdata
    "XINIT   (CODE)",           // a code copy of xiseg
    "CONST   (CODE)",           // const_name - const data (code or not)
    "CABS    (ABS,CODE)",       // cabs_name - const absolute data (code or not)
    "XABS    (ABS,XDATA)",      // xabs_name - absolute xdata/pdata
    "IABS    (ABS,DATA)",       // iabs_name - absolute idata/data
    NULL,                       // name of segment for initialized variables
    NULL,                       // name of segment for copies of initialized variables in code space
    NULL,
    NULL,
    1,
    1                           // No fancy alignments supported.
  },
  { NULL, NULL },
  {
    +1, 1, 4, 1, 1, 0
  },
    /* ds390 has an 16 bit mul & div */
  {
    2, -1
  },
  {
    ds390_emitDebuggerSymbol
  },
  {
    255/4,      /* maxCount */
    4,          /* sizeofElement */
    {8,12,20},  /* sizeofMatchJump[] */
    {10,14,22}, /* sizeofRangeCompare[] */
    4,          /* sizeofSubtract */
    7,          /* sizeofDispatch */
  },
  "_",
  _ds390_init,
  _ds390_parseOptions,
  _ds390_options,
  NULL,
  _ds390_finaliseOptions,
  _ds390_setDefaultOptions,
  ds390_assignRegisters,
  _ds390_getRegName,
  _ds390_keywords,
  _ds390_genAssemblerPreamble,
  NULL,                         /* no genAssemblerEnd */
  _ds390_genIVT,
  _ds390_genXINIT,
  _ds390_genInitStartup,
  _ds390_reset_regparm,
  _ds390_regparm,
  NULL,
  NULL,
  _ds390_nativeMulCheck,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  FALSE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* No array initializer support. */
  cseCostEstimation,
  __ds390_builtins,             /* table of builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local static allowed */
  0,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

/*---------------------------------------------------------------------------------*/
/*                               TININative specific                               */
/*---------------------------------------------------------------------------------*/

#define OPTION_TINI_LIBID "--tini-libid"

static OPTION _tininative_options[] =
  {
    { 0, OPTION_FLAT24_MODEL,   NULL, "use the flat24 model for the ds390 (default)" },
    { 0, OPTION_STACK_8BIT,     NULL, "use the 8bit stack for the ds390 (not supported yet)" },
    { 0, OPTION_STACK_SIZE,     &options.stack_size, "Tells the linker to allocate this space for stack", CLAT_INTEGER },
    { 0, "--pack-iram",         NULL, "Tells the linker to pack variables in internal ram (default)"},
    { 0, "--no-pack-iram",      &options.no_pack_iram, "Deprecated: Tells the linker not to pack variables in internal ram"},
    { 0, "--stack-10bit",       &options.stack10bit, "use the 10bit stack for ds390 (default)" },
    { 0, "--use-accelerator",   &options.useAccelerator, "generate code for ds390 arithmetic accelerator"},
    { 0, "--protect-sp-update", &options.protect_sp_update, "will disable interrupts during ESP:SP updates"},
    { 0, "--parms-in-bank1",    &options.parms_in_bank1, "use Bank1 for parameter passing"},
    { 0, OPTION_TINI_LIBID,     &options.tini_libid, "<nnnn> LibraryID used in -mTININative", CLAT_INTEGER },
    { 0, NULL }
  };

static void _tininative_init (void)
{
    asm_addTree (&asm_a390_mapping);
}

static void _tininative_setDefaultOptions (void)
{
    options.model=MODEL_FLAT24;
    options.stack10bit=1;
    options.stackAuto = 1;
}

static void _tininative_finaliseOptions (void)
{
    /* Hack-o-matic: if we are using the flat24 model,
     * adjust pointer sizes.
     */
    if (options.model != MODEL_FLAT24)  {
        options.model = MODEL_FLAT24 ;
        fprintf(stderr,"TININative supports only MODEL FLAT24\n");
    }
    port->s.fptr_size = 3;
    port->s.gptr_size = 4;

    port->stack.isr_overhead += 2;      /* Will save dpx on ISR entry. */

    port->stack.call_overhead += 2;     /* This acounts for the extra byte
                                         * of return addres on the stack.
                                         * but is ugly. There must be a
                                         * better way.
                                         */

    port->mem.default_local_map = xdata;
    port->mem.default_globl_map = xdata;

    if (!options.stack10bit) {
        options.stack10bit = 1;
        fprintf(stderr,"TININative supports only stack10bit \n");
    }

    if (!options.stack_loc) options.stack_loc = 0x400008;

    /* generate native code 16*16 mul/div */
    if (options.useAccelerator)
        port->support.muldiv=2;
    else
        port->support.muldiv=1;

    /* Fixup the memory map for the stack; it is now in
     * far space and requires a FPOINTER to access it.
     */
    istack->fmap = 1;
    istack->ptrType = FPOINTER;
    options.cc_only =1;
}

static int _tininative_genIVT (struct dbuf_s * oBuf, symbol ** interrupts, int maxInterrupts)
{
    return TRUE;
}

static void _tininative_genAssemblerPreamble (FILE * of)
{
    fputs("$include(tini.inc)\n", of);
    fputs("$include(ds80c390.inc)\n", of);
    fputs("$include(tinimacro.inc)\n", of);
    fputs("$include(apiequ.inc)\n", of);
    fputs("_bpx EQU 01Eh \t\t; _bpx (frame pointer) mapped to R8_B3:R7_B3\n", of);
    fputs("_ap  EQU 01Dh \t\t; _ap mapped to R6_B3\n", of);
    /* Must be first and return 0 */
    fputs("Lib_Native_Init:\n",of);
    fputs("\tclr\ta\n",of);
    fputs("\tret\n",of);
    fputs("LibraryID:\n",of);
    fputs("\tdb \"DS\"\n",of);
    if (options.tini_libid) {
        fprintf(of,"\tdb 0,0,0%02xh,0%02xh,0%02xh,0%02xh\n",
                (options.tini_libid>>24 & 0xff),
                (options.tini_libid>>16 & 0xff),
                (options.tini_libid>>8 & 0xff),
                (options.tini_libid  & 0xff));
    } else {
        fprintf(of,"\tdb 0,0,0,0,0,1\n");
    }

}
static void _tininative_genAssemblerEnd (FILE * of)
{
    fputs("\tend\n",of);
}
/* tininative assembler , calls "macro", if it succeeds calls "a390" */
static void _tininative_do_assemble (set *asmOptions)
{
    char *buf;
    static const char *macroCmd[] = {
        "macro","$1.a51",NULL
    };
    static const char *a390Cmd[] = {
        "a390","$1.mpp",NULL
    };

    buf = buildCmdLine(macroCmd, dstFileName, NULL, NULL, NULL);
    if (sdcc_system(buf)) {
        Safe_free (buf);
        exit(1);
    }
    Safe_free (buf);
    buf = buildCmdLine(a390Cmd, dstFileName, NULL, NULL, asmOptions);
    if (sdcc_system(buf)) {
        Safe_free (buf);
        exit(1);
    }
    Safe_free (buf);
}

/* list of key words used by TININative */
static char *_tininative_keywords[] =
{
  "at",
  "bit",
  "code",
  "critical",
  "data",
  "far",
  "idata",
  "interrupt",
  "near",
  "pdata",
  "reentrant",
  "sfr",
  "sbit",
  "using",
  "xdata",
  "_data",
  "_code",
  "_generic",
  "_near",
  "_xdata",
  "_pdata",
  "_idata",
  "_naked",
  "_JavaNative",
  NULL
};

static builtins __tininative_builtins[] = {
    { "__builtin_memcpy_x2x","v",3,{"cx*","cx*","i"}}, /* void __builtin_memcpy_x2x (xdata char *,xdata char *,int) */
    { "__builtin_memcpy_c2x","v",3,{"cx*","cp*","i"}}, /* void __builtin_memcpy_c2x (xdata char *,code  char *,int) */
    { "__builtin_memset_x","v",3,{"cx*","c","i"}},     /* void __builtin_memset     (xdata char *,char,int)         */
    /* TINI NatLib */
    { "NatLib_LoadByte","c",1,{"c"}},                  /* char  Natlib_LoadByte  (0 based parameter number)         */
    { "NatLib_LoadShort","s",1,{"c"}},                 /* short Natlib_LoadShort (0 based parameter number)         */
    { "NatLib_LoadInt","l",1,{"c"}},                   /* long  Natlib_LoadLong  (0 based parameter number)         */
    { "NatLib_LoadPointer","cx*",1,{"c"}},             /* long  Natlib_LoadPointer  (0 based parameter number)      */
    /* TINI StateBlock related */
    { "NatLib_InstallImmutableStateBlock","c",2,{"vx*","us"}},/* char NatLib_InstallImmutableStateBlock(state block *,int handle) */
    { "NatLib_InstallEphemeralStateBlock","c",2,{"vx*","us"}},/* char NatLib_InstallEphemeralStateBlock(state block *,int handle) */
    { "NatLib_RemoveImmutableStateBlock","v",0,{NULL}},/* void NatLib_RemoveImmutableStateBlock() */
    { "NatLib_RemoveEphemeralStateBlock","v",0,{NULL}},/* void NatLib_RemoveEphemeralStateBlock() */
    { "NatLib_GetImmutableStateBlock","i",0,{NULL}},   /* int  NatLib_GetImmutableStateBlock () */
    { "NatLib_GetEphemeralStateBlock","i",0,{NULL}},   /* int  NatLib_GetEphemeralStateBlock () */
    /* Memory manager */
    { "MM_XMalloc","i",1,{"l"}},                       /* int  MM_XMalloc (long)                */
    { "MM_Malloc","i",1,{"i"}},                        /* int  MM_Malloc  (int)                 */
    { "MM_ApplicationMalloc","i",1,{"i"}},             /* int  MM_ApplicationMalloc  (int)      */
    { "MM_Free","i",1,{"i"}},                          /* int  MM_Free  (int)                   */
    { "MM_Deref","cx*",1,{"i"}},                       /* char *MM_Free  (int)                  */
    { "MM_UnrestrictedPersist","c",1,{"i"}},           /* char  MM_UnrestrictedPersist  (int)   */
    /* System functions */
    { "System_ExecJavaProcess","c",2,{"cx*","i"}},     /* char System_ExecJavaProcess (char *,int) */
    { "System_GetRTCRegisters","v",1,{"cx*"}},         /* void System_GetRTCRegisters (char *) */
    { "System_SetRTCRegisters","v",1,{"cx*"}},         /* void System_SetRTCRegisters (char *) */
    { "System_ThreadSleep","v",2,{"l","c"}},           /* void System_ThreadSleep (long,char)  */
    { "System_ThreadSleep_ExitCriticalSection","v",2,{"l","c"}},/* void System_ThreadSleep_ExitCriticalSection (long,char)  */
    { "System_ProcessSleep","v",2,{"l","c"}},           /* void System_ProcessSleep (long,char)  */
    { "System_ProcessSleep_ExitCriticalSection","v",2,{"l","c"}},/* void System_ProcessSleep_ExitCriticalSection (long,char)  */
    { "System_ThreadResume","c",2,{"c","c"}},          /* char System_ThreadResume(char,char)  */
    { "System_SaveJavaThreadState","v",0,{NULL}},      /* void System_SaveJavaThreadState()    */
    { "System_RestoreJavaThreadState","v",0,{NULL}},   /* void System_RestoreJavaThreadState() */
    { "System_ProcessYield","v",0,{NULL}},             /* void System_ProcessYield() */
    { "System_ProcessSuspend","v",0,{NULL}},           /* void System_ProcessSuspend() */
    { "System_ProcessResume","v",1,{"c"}},             /* void System_ProcessResume(char) */
    { "System_RegisterPoll","c",1,{"vF*"}},            /* char System_RegisterPoll ((void *func pointer)()) */
    { "System_RemovePoll","c",1,{"vF*"}},              /* char System_RemovePoll ((void *func pointer)()) */
    { "System_GetCurrentProcessId","c",0,{NULL}},      /* char System_GetCurrentProcessId() */
    { "System_GetCurrentThreadId","c",0,{NULL}},       /* char System_GetCurrentThreadId() */
    { NULL , NULL,0, {NULL}}                       /* mark end of table */
};

static const char *_a390Cmd[] =
{
  "macro", "$l", "$3", "$1.a51", NULL
};

PORT tininative_port =
{
  TARGET_ID_DS390,
  "TININative",
  "DS80C390",                   /* Target name */
  NULL,                         /* processor */
  {
    glue,
    FALSE,                      /* Emit glue around main */
    MODEL_FLAT24,
    MODEL_FLAT24,
    get_model,
  },
  {
    _a390Cmd,
    NULL,
    "-l",               /* Options with debug */
    "-l",               /* Options without debug */
    0,
    ".a51",
    _tininative_do_assemble
  },
  {                             /* Linker */
    NULL,
    NULL,
    NULL,
    ".tlib",
    1,
    NULL,                       /* crt */
    _libs_ds390,                /* libs */
  },
  {
    _defaultRules,
    getInstructionSize,
    getRegsRead,
    getRegsWritten
  },
  {
        /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
    1, 2, 2, 4, 8, 1, 3, 3, 1, 4, 4
  },
  /* tags for generic pointers */
  { 0x00, 0x40, 0x60, 0x80 },           /* far, near, xstack, code */

  {
    "XSEG    (XDATA)",
    "STACK   (DATA)",
    "CSEG    (CODE)",
    "DSEG    (DATA)",
    "ISEG    (DATA)",
    "PSEG    (PAG,XDATA)",
    "XSEG    (XDATA)",
    "BSEG    (BIT)",
    "RSEG    (DATA)",
    "GSINIT  (CODE)",
    "OSEG    (OVR,DATA)",
    "GSFINAL (CODE)",
    "HOME    (CODE)",
    NULL,
    NULL,
    "CONST   (CODE)",           // const_name - const data (code or not)
    "CABS    (ABS,CODE)",       // cabs_name - const absolute data (code or not)
    "XABS    (ABS,XDATA)",      // xabs_name - absolute xdata/pdata
    "IABS    (ABS,DATA)",       // iabs_name - absolute idata/data
    NULL,                       // name of segment for initialized variables
    NULL,                       // name of segment for copies of initialized variables in code space
    NULL,
    NULL,
    1,
    1                           // No fancy alignments supported.
  },
  { NULL, NULL },
  {
    +1, 1, 4, 1, 1, 0
  },
    /* ds390 has an 16 bit mul & div */
  {
    2, -1
  },
  {
    ds390_emitDebuggerSymbol
  },
  {
    255/4,      /* maxCount */
    4,          /* sizeofElement */
    {8,12,20},  /* sizeofMatchJump[] */
    {10,14,22}, /* sizeofRangeCompare[] */
    4,          /* sizeofSubtract */
    7,          /* sizeofDispatch */
  },
  "",
  _tininative_init,
  _ds390_parseOptions,
  _tininative_options,
  NULL,
  _tininative_finaliseOptions,
  _tininative_setDefaultOptions,
  ds390_assignRegisters,
  _ds390_getRegName,
  _tininative_keywords,
  _tininative_genAssemblerPreamble,
  _tininative_genAssemblerEnd,
  _tininative_genIVT,
  NULL,
  _ds390_genInitStartup,
  _ds390_reset_regparm,
  _ds390_regparm,
  NULL,
  NULL,
  NULL,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  FALSE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* No array initializer support. */
  cseCostEstimation,
  __tininative_builtins,        /* table of builtin functions */
  FPOINTER,                     /* treat unqualified pointers as far pointers */
  0,                            /* DONOT reset labelKey */
  0,                            /* globals & local static NOT allowed */
  0,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};

static int
_ds400_genIVT (struct dbuf_s * oBuf, symbol ** interrupts, int maxInterrupts)
{
  /* We can't generate a static IVT, since the boot rom creates one
   * for us in rom_init.
   *
   * we must patch it as part of the C startup.
   */
  dbuf_printf (oBuf, ";\tDS80C400 IVT must be generated at runtime.\n");
  dbuf_printf (oBuf, "\tsjmp\t__sdcc_400boot\n");
  dbuf_printf (oBuf, "\t.ascii\t'TINI'\t; required signature for 400 boot loader.\n");
  dbuf_printf (oBuf, "\t.db\t0\t; selected bank: zero *should* work...\n");
  dbuf_printf (oBuf, "\t__sdcc_400boot:\tljmp\t__sdcc_gsinit_startup\n");

  return TRUE;
}


/*---------------------------------------------------------------------------------*/
/*                               _ds400 specific                                   */
/*---------------------------------------------------------------------------------*/

static OPTION _ds400_options[] =
  {
    { 0, OPTION_FLAT24_MODEL,   NULL, "use the flat24 model for the ds400 (default)" },
    { 0, OPTION_STACK_8BIT,     NULL, "use the 8bit stack for the ds400 (not supported yet)" },
    { 0, OPTION_STACK_SIZE,     &options.stack_size, "Tells the linker to allocate this space for stack", CLAT_INTEGER },
    { 0, "--pack-iram",         NULL, "Tells the linker to pack variables in internal ram (default)"},
    { 0, "--no-pack-iram",      &options.no_pack_iram, "Deprecated: Tells the linker not to pack variables in internal ram"},
    { 0, "--stack-10bit",       &options.stack10bit, "use the 10bit stack for ds400 (default)" },
    { 0, "--use-accelerator",   &options.useAccelerator, "generate code for ds400 arithmetic accelerator"},
    { 0, "--protect-sp-update", &options.protect_sp_update, "will disable interrupts during ESP:SP updates"},
    { 0, "--parms-in-bank1",    &options.parms_in_bank1, "use Bank1 for parameter passing"},
    { 0, NULL }
  };

static void
_ds400_finaliseOptions (void)
{
  if (options.noXinitOpt)
    {
      port->genXINIT=0;
    }

  // hackhack: we're a superset of the 390.
  addSet(&preArgvSet, Safe_strdup("-DSDCC_ds390"));
  addSet(&preArgvSet, Safe_strdup("-D__ds390"));

  /* Hack-o-matic: if we are using the flat24 model,
   * adjust pointer sizes.
   */
  if (options.model != MODEL_FLAT24)
    {
      fprintf (stderr,
               "*** warning: ds400 port small and large model experimental.\n");
      if (options.model == MODEL_LARGE)
        {
          port->mem.default_local_map = xdata;
          port->mem.default_globl_map = xdata;
        }
      else
        {
          port->mem.default_local_map = data;
          port->mem.default_globl_map = data;
        }
    }
  else
    {
      port->s.fptr_size = 3;
      port->s.gptr_size = 4;

      port->stack.isr_overhead += 2;      /* Will save dpx on ISR entry. */

      port->stack.call_overhead += 2;     /* This acounts for the extra byte
                                           * of return addres on the stack.
                                           * but is ugly. There must be a
                                           * better way.
                                           */

      port->mem.default_local_map = xdata;
      port->mem.default_globl_map = xdata;

      if (!options.stack10bit)
        {
          fprintf (stderr,
                   "*** error: ds400 port only supports the 10 bit stack mode.\n");
        }
      else
        {
          if (!options.stack_loc)
            options.stack_loc = 0xffdc00;
          // assumes IDM1:0 = 1:0, CMA = 1.
        }

      /* generate native code 16*16 mul/div */
      if (options.useAccelerator)
        port->support.muldiv=2;
      else
        port->support.muldiv=1;

      /* Fixup the memory map for the stack; it is now in
       * far space and requires a FPOINTER to access it.
       */
      istack->fmap = 1;
      istack->ptrType = FPOINTER;

      // the DS400 rom calling interface uses register bank 3.
      RegBankUsed[3] = 1;

    }  /* MODEL_FLAT24 */
}

static void _ds400_generateRomDataArea(FILE *fp, bool isMain)
{
    /* Only do this for the file containing main() */
    if (isMain)
    {
        fprintf(fp, "%s", iComments2);
        fprintf(fp, "; the direct data area used by the DS80c400 ROM code.\n");
        fprintf(fp, "%s", iComments2);
        fprintf(fp, ".area ROMSEG (ABS,CON,DATA)\n\n");
        fprintf(fp, ".ds 24 ; 24 bytes of directs used starting at 0x68\n\n");
    }
}

static void _ds400_linkRomDataArea(FILE *fp)
{
    fprintf(fp, "-b ROMSEG = 0x0068\n");
}

static const char * const _libs_ds400[] = { STD_DS400_LIB, NULL, };

PORT ds400_port =
{
  TARGET_ID_DS400,
  "ds400",
  "DS80C400",                   /* Target name */
  NULL,
  {
    glue,
    TRUE,                       /* Emit glue around main */
    MODEL_SMALL | MODEL_LARGE | MODEL_FLAT24,
    MODEL_SMALL,
    get_model,
  },
  {
    _asmCmd,
    NULL,
    "-plosgffwy",               /* Options with debug */
    "-plosgffw",                /* Options without debug */
    0,
    ".asm",
    NULL                        /* no do_assemble function */
  },
  {                             /* Linker */
    _linkCmd,
    NULL,
    NULL,
    ".rel",
    1,
    NULL,                       /* crt */
    _libs_ds400,                /* libs */
  },
  {                             /* Peephole optimizer */
    _defaultRules,
    getInstructionSize,
    getRegsRead,
    getRegsWritten,
    0,
    0,
    0,
  },
  {
        /* Sizes: char, short, int, long, long long, ptr, fptr, gptr, bit, float, max */
    1, 2, 2, 4, 8, 1, 2, 3, 1, 4, 4
  },

  /* tags for generic pointers */
  { 0x00, 0x40, 0x60, 0x80 },           /* far, near, xstack, code */

  {
    "XSEG    (XDATA)",
    "STACK   (DATA)",
    "CSEG    (CODE)",
    "DSEG    (DATA)",
    "ISEG    (DATA)",
    "PSEG    (PAG,XDATA)",
    "XSEG    (XDATA)",
    "BSEG    (BIT)",
    "RSEG    (DATA)",
    "GSINIT  (CODE)",
    "OSEG    (OVR,DATA)",
    "GSFINAL (CODE)",
    "HOME    (CODE)",
    "XISEG   (XDATA)", // initialized xdata
    "XINIT   (CODE)", // a code copy of xiseg
    "CONST   (CODE)",           // const_name - const data (code or not)
    "CABS    (ABS,CODE)",       // cabs_name - const absolute data (code or not)
    "XABS    (ABS,XDATA)",      // xabs_name - absolute xdata/pdata
    "IABS    (ABS,DATA)",       // iabs_name - absolute idata/data
    NULL,                       // name of segment for initialized variables
    NULL,                       // name of segment for copies of initialized variables in code space
    NULL,
    NULL,
    1
  },
  { _ds400_generateRomDataArea, _ds400_linkRomDataArea },
  {
    +1, 1, 4, 1, 1, 0
  },
    /* ds390 has an 16 bit mul & div */
  {
    2, -1
  },
  {
    ds390_emitDebuggerSymbol
  },
  {
    255/4,      /* maxCount */
    4,          /* sizeofElement */
    {8,12,20},  /* sizeofMatchJump[] */
    {10,14,22}, /* sizeofRangeCompare[] */
    4,          /* sizeofSubtract */
    7,          /* sizeofDispatch */
  },
  "_",
  _ds390_init,
  _ds390_parseOptions,
  _ds400_options,
  NULL,
  _ds400_finaliseOptions,
  _ds390_setDefaultOptions,
  ds390_assignRegisters,
  _ds390_getRegName,
  _ds390_keywords,
  _ds390_genAssemblerPreamble,
  NULL,                         /* no genAssemblerEnd */
  _ds400_genIVT,
  _ds390_genXINIT,
  _ds390_genInitStartup,
  _ds390_reset_regparm,
  _ds390_regparm,
  NULL,
  NULL,
  _ds390_nativeMulCheck,
  hasExtBitOp,                  /* hasExtBitOp */
  oclsExpense,                  /* oclsExpense */
  FALSE,
  TRUE,                         /* little endian */
  0,                            /* leave lt */
  0,                            /* leave gt */
  1,                            /* transform <= to ! > */
  1,                            /* transform >= to ! < */
  1,                            /* transform != to !(a == b) */
  0,                            /* leave == */
  FALSE,                        /* No array initializer support. */
  cseCostEstimation,
  __ds390_builtins,             /* table of builtin functions */
  GPOINTER,                     /* treat unqualified pointers as "generic" pointers */
  1,                            /* reset labelKey to 1 */
  1,                            /* globals & local static allowed */
  0,                            /* Number of registers handled in the tree-decomposition-based register allocator in SDCCralloc.hpp */
  PORT_MAGIC
};
